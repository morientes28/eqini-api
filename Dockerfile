FROM python:3-onbuild
MAINTAINER Michal Kalman "michalkalman@gmail.com"
ADD ./ /app
WORKDIR /app
VOLUME /app
CMD python manage.py runserver 0.0.0.0:8000 --settings config.settings_docker