""" Setting na vyvoj s dockerom """

try:
    from config.settings import *
except ImportError as e:
    pass

DEBUG = True
DISABLED_AUTHORIZATION = False
DOMAIN_BYPASS = True
CORS_ORIGIN_ALLOW_ALL = True
FRONT_END_DOMAIN = 'trh'

APPEND_SLASH = False


DATABASES = {
    'default': {
        'ENGINE': 'tenant_schemas.postgresql_backend',
        'NAME': 'docker',
        'USER': 'docker',
        'PASSWORD': 'docker',
        'HOST': '192.168.1.14',
        'PORT': '5432',
    },
    'analytics': { # Analytics and Meta data DB
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'docker_tenant',
        'USER': 'docker',
        'PASSWORD': 'docker',
        'HOST': '192.168.1.14',
        'PORT': '5432',
    }
}

#'HOST': '192.168.1.14',