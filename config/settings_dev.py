""" Setting na vyvoj bez dockeru """
try:
    from config.settings import *
except ImportError:
    pass

DEBUG = True
DOMAIN_BYPASS = True
DISABLED_AUTHORIZATION = True
CORS_ORIGIN_ALLOW_ALL = True

DATABASES = {
    'default': {
        'ENGINE': 'tenant_schemas.postgresql_backend',
        'NAME': 'docker',
        'USER': 'docker',
        'PASSWORD': 'docker',
        'HOST': '192.168.1.14',
        'PORT': '5432',
    },
    'analytics': { # Analytics and Meta data DB
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'docker_tenant',
        'USER': 'docker',
        'PASSWORD': 'docker',
        'HOST': '192.168.1.14',
        'PORT': '5432',
    }
}