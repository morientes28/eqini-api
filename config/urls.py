from django.conf.urls import url, include
from django.conf import settings
from django.contrib import admin
from django.conf.urls.static import static
from rest_framework.documentation import include_docs_urls
from config.permissions.token_views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token

API_TITLE = 'Eqini API'
API_DESCRIPTION = 'Public API interactive documentation'

urlpatterns = [
    url(r'^master/admin/', admin.site.urls),

	url(r'^api/shared/', include('modules.shared.api.urls')),
	url(r'^api/lists/', include('modules.lists.api.urls')),
	url(r'^api/account/', include('modules.account.api.urls')),
	url(r'^api/directory/', include('modules.directory.api.urls')),
	url(r'^api/warehouse/', include('modules.warehouse.api.urls')),
	



	url(r'^api/auth/token/get/', obtain_jwt_token),
	url(r'^api/auth/token/refresh/', refresh_jwt_token),
	url(r'^api/auth/token/verify/', verify_jwt_token),

    url(r'^api/fe/auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/fe/docs/', include_docs_urls(title=API_TITLE, description=API_DESCRIPTION))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
