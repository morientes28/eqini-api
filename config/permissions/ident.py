"""
This module is a collection of helpers for extracting data from valid JWT (retrieving identity)
"""
import jwt
from modules.account.models import Company, User
from django.conf import settings
from django.http import Http404

def get_token_payload(request):
    """extract payload from a valid JWT"""
    if 'HTTP_AUTHORIZATION' in request.META:
        token = request.META['HTTP_AUTHORIZATION'].lstrip("Bearer").strip()
        return jwt.decode(token, verify=False)


def get_orgid(request):
    """extract ORGID from a valid JWT"""
    try:
        payload = get_token_payload(request)
        return payload['orgid']
    except:
        raise Http404()


def get_org_obj(request):
    """extract ORGID from a valid JWT and pass it as a company object"""
    try:
        payload = get_token_payload(request)
        try:
            return Company.objects.get(id=payload['orgid'])
        except:
            raise Http404()
    except:
        raise Http404()


def get_user_id(request):
    """extract user ID from a valid JWT"""
    try:
        payload = get_token_payload(request)
        return payload['user_id']
    except:
        raise Http404()


def get_tenant(request):
    """extract tenant name from a valid JWT"""
    if 1 == 1:
        return 'localhost:4200'
    if 'tenant' in request.COOKIES:
        return request.COOKIES.get('tenant')
    else:
        try:
            payload = get_token_payload(request)
            return payload['tenant']
        except:
            raise Http404()










def GetOrgID(request):
    if settings.DISABLED_AUTHORIZATION:
        print("Vyberam GetOrgID mockovacie s id 1")
        print("A nastavujem do requestu usera s ID=ea67eb88-22a8-b34b-5ff7-d472f2b25cbc")
        #request.user = User.objects.get(id='ea67eb88-22a8-b34b-5ff7-d472f2b25cbc')
        request.user = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')
        return 1

    token = request.META['HTTP_AUTHORIZATION'] #.lstrip("Bearer").strip()
    print(token)
    payload = jwt.decode(token, verify=False)
    print(payload)
    return payload['orgid']


def GetOrg(request):
    if settings.DISABLED_AUTHORIZATION:
        print("Vyberam GetOrg mockovacie s id 1")
        return Company.objects.get(id=1)

    token = request.META['HTTP_AUTHORIZATION'] #.lstrip("Bearer").strip()
    payload = jwt.decode(token, verify=False)
    try:
        return Company.objects.get(id=payload['orgid'])
    except:
        # fixme: osetrit vynimku
        pass


def GetUserID(request):
    if settings.DISABLED_AUTHORIZATION:
        print("Vyberam GetUserID mockovacie s id ea67eb88-22a8-b34b-5ff7-d472f2b25cbc")
        #return User.objects.get(id='ea67eb88-22a8-b34b-5ff7-d472f2b25cbc')
        return User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')
    token = request.META['HTTP_AUTHORIZATION'] #.lstrip("Bearer").strip()
    payload = jwt.decode(token, verify=False)
    return payload['user_id']
