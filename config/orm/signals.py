from __future__ import unicode_literals
import uuid
from datetime import datetime
import random

from django.utils import dateformat
from django.utils.text import slugify
from django.db.models.signals import pre_save, post_save, post_delete

from django.db import models

# ex.: "37-some-title-203458"
def create_instance_slug(instance):
	prefix = str(random.randint(10,99))
	timestamp = str(datetime.now().strftime("%H")) + str(datetime.now().strftime("%M")) + str(datetime.now().strftime("%S"))
	slug = prefix + '-' + slugify(instance.title) + timestamp
	return slug

def create_instance_slug_from_name(instance):
	prefix = str(random.randint(10,99))
	timestamp = str(datetime.now().strftime("%H")) + str(datetime.now().strftime("%M")) + str(datetime.now().strftime("%S"))
	slug = prefix + '-' + slugify(instance.full_name) + timestamp
	return slug

def instance_slug_receiver(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = create_instance_slug(instance)