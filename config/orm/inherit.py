import uuid
from django.db import models

class IdOnly(models.Model):
	id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, help_text="Instance's unique ID")

	class Meta:
		abstract = True

class BaseTitleOrgID(models.Model):
	id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, help_text="Instance's unique ID")
	title = models.CharField(max_length=100, null=False, blank=True, help_text="Instance's title")
	orgid = models.ForeignKey('account.Company', related_name='%(app_label)s_%(class)s_company', null=True, help_text="Instance owner (organization)")

	class Meta:
		abstract = True

class BaseTitleSlugOrgID(models.Model):
	id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, help_text="Instance's unique ID")
	title = models.CharField(max_length=100, null=False, blank=True, help_text="Instance's title")
	slug = models.SlugField(max_length=125, unique=True, null=False, blank=True, help_text="Slug of the instance")
	orgid = models.ForeignKey('account.Company', related_name='%(app_label)s_%(class)s_company', null=True, help_text="Instance owner (organization)")

	class Meta:
		abstract = True

class BaseTitleSlug(models.Model):
	id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, help_text="Instance's unique ID")
	title = models.CharField(max_length=100, null=False, blank=True, help_text="Instance's title")
	slug = models.SlugField(max_length=125, unique=True, null=False, blank=True, help_text="Slug of the instance")

	class Meta:
		abstract = True

class BaseOrgID(models.Model):
	id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, help_text="Instance's unique ID")
	orgid = models.ForeignKey('account.Company', related_name='%(app_label)s_%(class)s_company', null=True, help_text="Instance owner (organization)")

	class Meta:
		abstract = True

class Branch(models.Model):
	branchid = models.ForeignKey('account.CompanyBranch', related_name='%(app_label)s_%(class)s_branchid', null=True, help_text="Instance owner (branch)")

	class Meta:
		abstract = True

class CRUDInfo(models.Model):
	created_at = models.DateTimeField(auto_now=False, auto_now_add=True, null=True, help_text="Instance creation timestamp")
	updated_at = models.DateTimeField(auto_now=True, auto_now_add=False, null=True, help_text="Instance update timestamp")
	deleted_at = models.DateTimeField(auto_now=False, auto_now_add=False, null=True, help_text="Instance deletion timestamp")
	restored_at	= models.DateTimeField(auto_now=False, auto_now_add=False, null=True, help_text="Instance restoration timestamp")
	deleted = models.BooleanField(null=False, default=False, help_text="Is instance deleted?")
	created_by = models.ForeignKey('account.User', related_name='%(app_label)s_%(class)s_created_by', null=True)
	updated_by = models.ForeignKey('account.User', related_name='%(app_label)s_%(class)s_updated_by', null=True)
	deleted_by = models.ForeignKey('account.User', related_name='%(app_label)s_%(class)s_deleted_by', null=True)
	restored_by = models.ForeignKey('account.User', related_name='%(app_label)s_%(class)s_restored_by', null=True)

	class Meta:
		abstract = True

class BaseLocation(models.Model):
	base_street = models.CharField(max_length=200, null=False, blank=True, help_text="Base street with number")
	base_city = models.CharField(max_length=150, null=False, blank=True, help_text="Base city")
	base_zip = models.CharField(max_length=20, null=False, blank=True, help_text="Base zip code")
	base_state = models.ForeignKey('shared.State', related_name='%(app_label)s_%(class)s_state', null=True, help_text="Base state")
	base_country = models.ForeignKey('shared.Country', related_name='%(app_label)s_%(class)s_country', null=True, help_text="Base country")
	base_gps_coordinates = models.CharField(max_length=100, null=False, blank=True, help_text="Location gps coordinates")

	class Meta:
		abstract = True

class PostLocation(models.Model):
	post_street = models.CharField(max_length=200, null=False, blank=True, help_text="Post street with number")
	post_city = models.CharField(max_length=150, null=False, blank=True, help_text="Post city")
	post_zip = models.CharField(max_length=20, null=False, blank=True, help_text="Post zip code")
	post_state = models.ForeignKey('shared.State', related_name='%(class)s_state', null=True, help_text="Post state")
	post_country = models.ForeignKey('shared.Country', related_name='%(class)s_country', null=True, help_text="Post country")

	class Meta:
		abstract = True

class ContactInfo(models.Model):
	phone_1 = models.CharField(max_length=30, null=False, blank=True, help_text="Phone number 1")
	phone_1_type = models.ForeignKey('shared.PhoneType', related_name='%(app_label)s_%(class)s_phone_type_1', null=True, help_text="Phone number type 1")
	phone_2 = models.CharField(max_length=30, null=False, blank=True, help_text="Phone number 2")
	phone_2_type = models.ForeignKey('shared.PhoneType', related_name='%(app_label)s_%(class)s_phone_type_2', null=True, help_text="Phone number type 2")
	phone_3 = models.CharField(max_length=30, null=False, blank=True, help_text="Phone number 3")
	phone_3_type = models.ForeignKey('shared.PhoneType', related_name='%(app_label)s_%(class)s_phone_type_3', null=True, help_text="Phone number type 3")
	email_1 = models.EmailField(max_length=150, null=False, blank=True, help_text="Email address 1")
	email_2 = models.EmailField(max_length=150, null=False, blank=True, help_text="Email address 2")
	contact_person = models.ForeignKey('account.User', related_name='%(app_label)s_%(class)s_contact_person', null=True, help_text="Contact person")
	web = models.CharField(max_length=100, null=False, blank=True, help_text="Web address")

	class Meta:
		abstract = True

class SocialLinks(models.Model):
	facebook = models.CharField(max_length=150, null=False, blank=True, help_text="Facebook Page")
	twitter = models.CharField(max_length=150, null=False, blank=True, help_text="Twitter handle @")
	google_plus = models.CharField(max_length=150, null=False, blank=True, help_text="Google Plus page")
	instagram = models.CharField(max_length=150, null=False, blank=True, help_text="Instagram nickname")
	linkedin = models.CharField(max_length=150, null=False, blank=True, help_text="LinkedIn link")
	youtube = models.CharField(max_length=150, null=False, blank=True, help_text="Youtube accoutn link")
	skype = models.CharField(max_length=150, null=False, blank=True, help_text="skype accoutn link")

	class Meta:
		abstract = True

class FinInfo(models.Model):
	ico = models.CharField(max_length=20, null=False, blank=True, help_text="ICO")
	dic = models.CharField(max_length=20, null=False, blank=True, help_text="DIC")
	icdph = models.CharField(max_length=20, null=False, blank=True, help_text="IC DPH")
	is_dph = models.BooleanField(default=False, help_text="Is DPH payer?")
	#legal_form = models.ForeignKey('shared.LegalForm', related_name='%(app_label)s_%(class)s_legal_form', null=True, help_text="Legal form of the subject")

	class Meta:
		abstract = True