""" Django 10.0.6 """

import os
import datetime
SITE_ROOT = os.path.realpath(os.path.dirname(__file__))
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECRET_KEY = 'ad1s%)op=z(z7z%g3lh=6s@r6^#l2%ax+w-v!#$!9ctp9#+fco'
FRONT_END_DOMAIN = '.eqini.sk'
CORP_NAME = 'TPSOFT, s.r.o.'

ROOT_URLCONF = 'config.urls'
WSGI_APPLICATION = 'config.wsgi.application'
SITE_ID = 1
AUTH_USER_MODEL = 'account.User'

DEBUG = False
DISABLED_AUTHORIZATION = False

ALLOWED_HOSTS = ['*']

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'Europe/Bratislava'
USE_I18N = True
USE_L10N = True
USE_TZ = True

EMAIL_HOST = 'smtp.gmail.com'
EMIAL_PORT = '587'
EMAIL_HOST_USER = 'eqinicloud@gmail.com'
EMAIL_HOST_PASSWORD = 'tpsoft2016'
EMAIL_USE_TLS = True
EMAIL_SUBJECT_PREFIX = 'Eqini Cloud | '

DATA_UPLOAD_MAX_MEMORY_SIZE = 3621440
FILE_UPLOAD_MAX_MEMORY_SIZE = 10621440

STATIC_URL          = '/static/assets/'
STATIC_ROOT         = os.path.join(BASE_DIR, "static", "assets")
STATICFILES_DIRS    = [os.path.join(BASE_DIR, "static", "temp"),]
MEDIA_URL           = '/media/'
MEDIA_ROOT          = os.path.join(BASE_DIR, "media")
TEMPLATE_URL        = os.path.join(BASE_DIR, "templates")
DEFAULT_FILE_STORAGE = 'tenant_schemas.storage.TenantFileSystemStorage'

CSRF_COOKIE_DOMAIN = FRONT_END_DOMAIN
CSRF_COOKIE_SECURE = False # Set To True When SSL Available
CSRF_TRUSTED_ORIGINS = [FRONT_END_DOMAIN]

CORS_ORIGIN_ALLOW_ALL = False # Change to False in production
#CORS_ORIGIN_WHITELIST       = ('eqini.sk',)
#CORS_ORIGIN_WHITELIST       = ('localhost','localhost:4200')
#CORS_ORIGIN_REGEX_WHITELIST = (r'^(http?://)?(\w+\.)?eqini\.sk$', )
#CORS_URLS_REGEX = r'^/api/.*$'
CORS_ALLOW_METHODS = ('GET', 'POST', 'PUT', 'DELETE', 'OPTIONS')
CORS_ALLOW_CREDENTIALS = True
#CORS_EXPOSE_HEADERS = ('origin',)
#CORS_EXPOSE_HEADERS = ([])
CORS_ALLOW_HEADERS = (                                             # - approved headers
    'x-requested-with',
    'content-type',
    'accept',
    'origin',
    'authorization',
    'x-csrftoken',
    'user-agent',
    'accept-encoding',
    'cookie',
	'access-control-allow-origin',
)
DISALLOWED_USER_AGENTS = []

SESSION_EXPIRE_AT_BROWSER_CLOSE = True
SESSION_COOKIE_DOMAIN = FRONT_END_DOMAIN

DATABASES = {
    'default': {
        'ENGINE': 'tenant_schemas.postgresql_backend',
        'NAME': 'tp-eqini-app',
        'USER': 'tpsoft-worker',
        'PASSWORD': '#EPpthCN2$BgGtt&fePwGxqA',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    },
    'analytics': { # Analytics and Meta data DB
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'tp-eqini-analytics',
        'USER': 'tpsoft-worker',
        'PASSWORD': '#EPpthCN2$BgGtt&fePwGxqA',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}
DATABASE_ROUTERS = ['config.orm.router.AnalyticsDBRouter', 'tenant_schemas.routers.TenantSyncRouter']

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

MIDDLEWARE = [
    'config.orm.middleware.DefaultTenantMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

SHARED_APPS = [
    'tenant_schemas',
    'modules.account',
    'modules.shared',

    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'rest_framework',
    'corsheaders'
]
TENANT_APPS = [
    #'modules.access',
    'modules.directory',
    'modules.lists',
    'modules.warehouse',
    
    'corsheaders',
    'django.contrib.contenttypes',
]
TENANT_MODEL = "account.Company" 
INSTALLED_APPS = [
    'tenant_schemas',
    'modules.account',
    #'modules.access',
    'modules.shared',
    'modules.directory',
    'modules.lists',
    'modules.warehouse',

    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',

    'rest_framework',
    'corsheaders',
]
#PG_EXTRA_SEARCH_PATHS = ['extensions']

PASSWORD_HASHERS = [
    #'django.contrib.auth.hashers.Argon2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
]

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.AllowAny',
        #'rest_framework.permissions.IsAuthenticated'
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.BasicAuthentication'
    ),
}

JWT_AUTH = {
    'JWT_ENCODE_HANDLER':
    'config.permissions.token.jwt_encode_handler',

    'JWT_DECODE_HANDLER':
    'config.permissions.token.jwt_decode_handler',

    'JWT_PAYLOAD_HANDLER':
    'config.permissions.token.jwt_payload_handler',

    'JWT_PAYLOAD_GET_USER_ID_HANDLER':
    'config.permissions.token.jwt_get_user_id_from_payload_handler',

    'JWT_RESPONSE_PAYLOAD_HANDLER':
    'config.permissions.token.jwt_response_payload_handler',

    'JWT_SECRET_KEY': SECRET_KEY,
    'JWT_GET_USER_SECRET_KEY': None,
    'JWT_PUBLIC_KEY': None,
    'JWT_PRIVATE_KEY': None,
    'JWT_ALGORITHM': 'HS256',
    'JWT_VERIFY': True,
    'JWT_VERIFY_EXPIRATION': True,
    'JWT_LEEWAY': 10,
    'JWT_EXPIRATION_DELTA': datetime.timedelta(hours=100),
    'JWT_AUDIENCE': None,
    'JWT_ISSUER': CORP_NAME,

    'JWT_ALLOW_REFRESH': True,
    'JWT_REFRESH_EXPIRATION_DELTA': datetime.timedelta(days=7),

    'JWT_AUTH_HEADER_PREFIX': 'Bearer',
    'JWT_AUTH_COOKIE': None,
}
