#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 -h 0.0.0.0 --username "docker" <<-EOSQL
    INSERT INTO public.account_company(id,created_at,updated_at,deleted_at,restored_at,deleted,base_street, base_city, base_zip, base_gps_coordinates, post_street, post_city, post_zip, phone_1, phone_2, phone_3, email_1, email_2, web, facebook, twitter, google_plus, instagram, linkedin, youtube, skype, ico, dic, icdph,is_dph,title,slug,domain_url, schema_name,long_title, domain,is_trial,is_blocked,base_country_id,base_state_id,contact_person_id,created_by_id,deleted_by_id, phone_1_type_id,phone_2_type_id,phone_3_type_id,post_country_id,post_state_id,restored_by_id,updated_by_id) VALUES (1,'2017-01-01 00:00:00','2017-01-01 00:00:00', '2017-01-01 00:00:00','2017-01-01 00:00:00', false, false, false, false, false, false, false, false, false, false, '', '', '', '', '', '', '', '', '', '', '', '', '', '', false, 'test','test', 'test','public','test title','test',false,false, null, null, null, null, null, null, null, null, null, null, null, null);
    ﻿INSERT INTO public.account_user(
	password, last_login, is_superuser, created_at, updated_at, deleted_at, restored_at, deleted,  email,
    full_name, is_staff, is_admin, is_active, activation_token, created_by_id, deleted_by_id, restored_by_id, updated_by_id,
    company, avatar,id)
	VALUES ('test','2017-01-01 00:00:00', true, '2017-01-01 00:00:00', '2017-01-01 00:00:00', null, null, false, 'test@test.sk',
    'testovac', true, true, true, '', null, null, null, null,
     1, 1,  'ea67eb88-22a8-b34b-5ff7-d472f2b25cbc');

     ﻿INSERT INTO public.account_company_users(id, company_id, user_id)	VALUES (1, 1, 'ea67eb88-22a8-b34b-5ff7-d472f2b25cbc');
EOSQL
