# -*- coding: utf-8 -*-
import os
import subprocess
from fabric.api import local, run, env, put, sudo, cd, get

# -------------------------------  profiles, settings and hosts ---
env.roledefs = {
    'dev': ['localhost'],
    'docker': ['localhost']
}

env.hosts = ['localhost']

base_path = os.path.dirname(os.path.realpath(__file__))

# ------------------------------- private functions ----------------
def _setting():
	if env.roles:
		if env.roles[0]=="dev":
			return " --settings config.settings_dev"
		if env.roles[0]=="docker":
			return " --settings config.settings_docker"
	return ""

# ------------------------------- CLI -------------------------------
def migrate():
	""" migration tenant schemas """
	local("python manage.py migrate_schemas" + _setting())

def init():
	""" init project """
	local("virtualenv env")
	local("source /env/bin/activate")
	local("pip install -r requirements.txt")
	migrate()

def test(module="warehouse"):
    """ run unit tests """
    local(base_path + "/manage.py test " + module)

def run():
	""" run develop server """
	local("python manage.py runserver" + _setting())