#!/usr/bin/python
# -*- coding: utf-8

from django.core.mail import EmailMultiAlternatives
from django.conf import settings

def sendUserRegMail(emailAddress, fullName, companyTitle, companyDomain):
	emailSubject = settings.CORP_NAME + ' - nová registrácia firmy'
	emailTo = emailAddress
	emailFrom = settings.EMAIL_HOST_USER

	text_content = fullName + u', vitajte v aplikácii Eqini spoločnosti ' + settings.CORP_NAME + u'. Účet vašej firmy ' + companyTitle + u' je pripravený. Pristupovať k nemu môžete z adresy http://' + companyDomain

	html_content = '<p><strong>' + fullName + ',</strong></p>' + u'<p>vitajte v aplikácii Eqini spoločnosti ' + settings.CORP_NAME + u'. Účet vašej firmy ' + companyTitle + u' je pripravený.</p>' + u'<p>Pristupovať k nemu môžete z adresy <strong>http://' + companyDomain + '</strong></p>'

	subject, from_email, to = emailSubject, emailFrom, emailTo

	msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
	msg.attach_alternative(html_content, "text/html")
	msg.send()
	
