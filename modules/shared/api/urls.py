from django.conf.urls import url, include
from modules.shared.api.view.localization import *
from modules.shared.api.view.directory import *
from modules.shared.api.view.warehouse import *
from modules.shared.api.view.finance import *

urlpatterns = [
    url(r'^localization/states/$', StatesList.as_view(), name='states-all'),
    url(r'^localization/states/(?P<id>[^/]+)/$', StatesDetail.as_view(), name='states-detail'),
    url(r'^localization/cities/$', CitiesList.as_view(), name='cities-all'),
    url(r'^localization/countries/$', CountriesList.as_view()),
    url(r'^localization/languages/$', LanguagesList.as_view(), name='languages-all'),
    url(r'^localization/currencies/$', CurrenciesList.as_view(), name='currencies-all'),

    url(r'^directory/phone-types/$', PhoneTypesList.as_view(), name='phonetypes-all'),
    url(r'^directory/priorities/$', PrioritiesList.as_view(), name='priorities-all'),
    url(r'^directory/outgo-types/$', OutgoTypesList.as_view(), name='outgotypes-all'),
    url(r'^directory/pay-types/$', PayTypesList.as_view(), name='paytypes-all'),
    url(r'^directory/alc-reg-types/$', AlcRegTypesList.as_view(), name='alcregtypes-all'),
    url(r'^directory/turnovers/$', TurnoversList.as_view(), name='turnovers-all'),
    url(r'^directory/employee-counts/$', EmployeeCountsList.as_view(), name='employeecounts-all'),
    url(r'^directory/account-statuses/$', AccountStatusesList.as_view(), name='accountstatuses-all'),
    url(r'^directory/family-statuses/$', FamilyStatusesList.as_view(), name='familystatuses-all'),
    url(r'^directory/genders/$', GendersList.as_view(), name='genders-all'),
    url(r'^directory/sources/$', SourcesList.as_view(), name='sources-all'),

    url(r'^warehouse/measure-units/$', MeasureUnitsList.as_view(), name='measureunits-all'),

    url(r'^finance/banks/$', BanksList.as_view(), name='banks-all'),
]