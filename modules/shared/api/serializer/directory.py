from rest_framework.validators import UniqueTogetherValidator
from rest_framework import serializers

from modules.shared.models import PhoneType, Priority, OutgoType, PayType, AlcRegType, Turnover, EmployeeCount, AccountStatus, FamilyStatus, Gender, Source

class PhoneTypesSerializer(serializers.ModelSerializer):

	class Meta:
		model = PhoneType
		fields = ('__all__')

class PrioritiesSerializer(serializers.ModelSerializer):

	class Meta:
		model = Priority
		fields = ('__all__')

class OutgoTypesSerializer(serializers.ModelSerializer):

	class Meta:
		model = OutgoType
		fields = ('__all__')

class PayTypesSerializer(serializers.ModelSerializer):

	class Meta:
		model = PayType
		fields = ('__all__')		

class AlcRegTypesSerializer(serializers.ModelSerializer):

	class Meta:
		model = AlcRegType
		fields = ('__all__')

class TurnoversSerializer(serializers.ModelSerializer):

	class Meta:
		model = Turnover
		fields = ('__all__')

class EmployeeCountsSerializer(serializers.ModelSerializer):

	class Meta:
		model = EmployeeCount
		fields = ('__all__')

class AccountStatusesSerializer(serializers.ModelSerializer):

	class Meta:
		model = AccountStatus
		fields = ('__all__')

class FamilyStatusesSerializer(serializers.ModelSerializer):

	class Meta:
		model = FamilyStatus
		fields = ('__all__')

class GendersSerializer(serializers.ModelSerializer):

	class Meta:
		model = Gender
		fields = ('__all__')		

class SourcesSerializer(serializers.ModelSerializer):

	class Meta:
		model = Source
		fields = ('__all__')
