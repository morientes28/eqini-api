from rest_framework.validators import UniqueTogetherValidator
from rest_framework import serializers

from modules.shared.models import State, Country, Language, Currency, City


class StatesSerializer(serializers.ModelSerializer):
    class Meta:
        model = State
        fields = ('__all__')


class CountriesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = ('__all__')


class LanguagesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Language
        fields = ('__all__')


class CurrenciesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Currency
        fields = ('__all__')


class CitiesSerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ('__all__')

