from rest_framework.validators import UniqueTogetherValidator
from rest_framework import serializers

from modules.shared.models import Bank

class BanksSerializer(serializers.ModelSerializer):

	class Meta:
		model = Bank
		fields = ('__all__')