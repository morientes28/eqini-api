from rest_framework.validators import UniqueTogetherValidator
from rest_framework import serializers

from modules.shared.models import MeasureUnit

class MeasureUnitsSerializer(serializers.ModelSerializer):

	class Meta:
		model = MeasureUnit
		fields = ('__all__')