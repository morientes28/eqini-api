from django.http import Http404

from rest_framework import status
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from rest_framework.response import Response
from rest_framework.permissions import AllowAny

from rest_framework.renderers import JSONRenderer

from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.authentication import BasicAuthentication

from modules.shared.models import PhoneType, Priority, OutgoType, PayType, AlcRegType, Turnover, EmployeeCount, AccountStatus, FamilyStatus, Gender, Source
from modules.shared.api.serializer.directory import *

class PhoneTypesList(generics.ListAPIView):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
	ordering = ('title')

	def get_serializer_class(self):
		return PhoneTypesSerializer

	def get_queryset(self):
		return PhoneType.objects.all()

class PrioritiesList(generics.ListAPIView):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
	ordering = ('title')

	def get_serializer_class(self):
		return PrioritiesSerializer

	def get_queryset(self):
		return Priority.objects.all()

class OutgoTypesList(generics.ListAPIView):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
	ordering = ('title')

	def get_serializer_class(self):
		return OutgoTypesSerializer

	def get_queryset(self):
		return OutgoType.objects.all()

class PayTypesList(generics.ListAPIView):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
	ordering = ('title')

	def get_serializer_class(self):
		return PayTypesSerializer

	def get_queryset(self):
		return PayType.objects.all()

class AlcRegTypesList(generics.ListAPIView):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
	ordering = ('title')

	def get_serializer_class(self):
		return AlcRegTypesSerializer

	def get_queryset(self):
		return AlcRegType.objects.all()

class TurnoversList(generics.ListAPIView):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
	ordering = ('title')

	def get_serializer_class(self):
		return TurnoversSerializer

	def get_queryset(self):
		return Turnover.objects.all()

class EmployeeCountsList(generics.ListAPIView):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
	ordering = ('title')

	def get_serializer_class(self):
		return EmployeeCountsSerializer

	def get_queryset(self):
		return EmployeeCount.objects.all()

class AccountStatusesList(generics.ListAPIView):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
	ordering = ('title')

	def get_serializer_class(self):
		return AccountStatusesSerializer

	def get_queryset(self):
		return AccountStatus.objects.all()

class FamilyStatusesList(generics.ListAPIView):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
	ordering = ('title')

	def get_serializer_class(self):
		return FamilyStatusesSerializer

	def get_queryset(self):
		return FamilyStatus.objects.all()

class GendersList(generics.ListAPIView):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
	ordering = ('title')

	def get_serializer_class(self):
		return GendersSerializer

	def get_queryset(self):
		return Gender.objects.all()

class SourcesList(generics.ListAPIView):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
	ordering = ('title')

	def get_serializer_class(self):
		return SourcesSerializer

	def get_queryset(self):
		return Source.objects.all()