from django.http import Http404

from rest_framework import status
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from rest_framework.response import Response
from rest_framework.permissions import AllowAny

from rest_framework.renderers import JSONRenderer

from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.authentication import BasicAuthentication

from modules.shared.models import State, Country, Language, Currency, City
from modules.shared.api.serializer.localization import *

class StatesList(generics.ListAPIView):
	permission_classes 		= (AllowAny,)
	authentication_classes 	= (BasicAuthentication, JSONWebTokenAuthentication)
	ordering 		= ('title')

	def get_serializer_class(self):
		return StatesSerializer

	def get_queryset(self):
		return State.objects.all()


class StatesDetail(generics.RetrieveUpdateDestroyAPIView):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
	lookup_field = ('id') #slug

	# Check the request type and select the corrent serializer
	def get_serializer_class(self):
		return StatesSerializer

	# Allow partial updates
	def get_serializer(self, *args, **kwargs):
		kwargs['partial'] = True
		return super(StatesDetail, self).get_serializer(*args, **kwargs)

	# Get the correct queryset
	def get_queryset(self):
		return State.objects.all()  # filter(


class CountriesList(generics.ListAPIView):
	permission_classes 		= (AllowAny,)
	authentication_classes 	= (BasicAuthentication, JSONWebTokenAuthentication)
	ordering 		= ('title')

	def get_serializer_class(self):
		return CountriesSerializer

	def get_queryset(self):
		return Country.objects.all()

class LanguagesList(generics.ListAPIView):
	permission_classes 		= (AllowAny,)
	authentication_classes 	= (BasicAuthentication, JSONWebTokenAuthentication)
	ordering 		= ('title')

	def get_serializer_class(self):
		return LanguagesSerializer

	def get_queryset(self):
		return Language.objects.all()

class CurrenciesList(generics.ListAPIView):
	permission_classes 		= (AllowAny,)
	authentication_classes 	= (BasicAuthentication, JSONWebTokenAuthentication)
	ordering 		= ('title')

	def get_serializer_class(self):
		return CurrenciesSerializer

	def get_queryset(self):
		return Currency.objects.all()

class CitiesList(generics.ListAPIView):
	permission_classes 		= (AllowAny,)
	authentication_classes 	= (BasicAuthentication, JSONWebTokenAuthentication)
	ordering 		= ('title')

	def get_serializer_class(self):
		return CitiesSerializer

	def get_queryset(self):
		return City.objects.all()