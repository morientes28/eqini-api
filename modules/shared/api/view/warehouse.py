from django.http import Http404

from rest_framework import status
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from rest_framework.response import Response
from rest_framework.permissions import AllowAny

from rest_framework.renderers import JSONRenderer

from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.authentication import BasicAuthentication

from modules.shared.models import MeasureUnit
from modules.shared.api.serializer.warehouse import *

class MeasureUnitsList(generics.ListAPIView):
	permission_classes 		= (AllowAny,)
	authentication_classes 	= (BasicAuthentication, JSONWebTokenAuthentication)
	ordering 		= ('title')

	def get_serializer_class(self):
		return MeasureUnitsSerializer

	def get_queryset(self):
		return MeasureUnit.objects.all()