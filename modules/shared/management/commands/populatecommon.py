from django.core.management.base import BaseCommand, CommandError
from modules.shared.management.data.data import *

class Command(BaseCommand):
	help = 'Write initial data to DB ("Common Tables")'

	# def add_arguments(self, parser):
	# 	parser.add_arguments(
	# 		'--states',
	# 		action='store_true',
	# 		dest='states',
	# 		default=False,
	# 		help='Add states'
	# 	)

	def handle(self, *args, **options):
		LanguageData(self)
		self.stdout.write(self.style.SUCCESS('Languages have been added'))

		CountryData(self)
		self.stdout.write(self.style.SUCCESS('Countries have been added'))

		CurrencyData(self)
		self.stdout.write(self.style.SUCCESS('Currencies have been added'))

		PhoneTypeData(self)
		self.stdout.write(self.style.SUCCESS('Phone Types have been added'))
		
		StateData(self)
		self.stdout.write(self.style.SUCCESS('States have been added'))

		CityData(self)
		self.stdout.write(self.style.SUCCESS('Cities have been added'))

		AccountStatusData(self)
		self.stdout.write(self.style.SUCCESS('Account statuses have been added'))

		PriorityData(self)
		self.stdout.write(self.style.SUCCESS('Prorities have been added'))

		TurnoverData(self)
		self.stdout.write(self.style.SUCCESS('Turnovers have been added'))	

		EmployeeCountData(self)
		self.stdout.write(self.style.SUCCESS('Employee counts have been added'))

		FamilyStatusData(self)
		self.stdout.write(self.style.SUCCESS('Family statuses have been added'))

		GenderData(self)
		self.stdout.write(self.style.SUCCESS('Genders have been added'))

		BankData(self)
		self.stdout.write(self.style.SUCCESS('Banks have been added'))