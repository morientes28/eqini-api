from __future__ import unicode_literals
import uuid
from django.db import models
from django.contrib.postgres.fields import JSONField
from config.orm.inherit import IdOnly


class Permission(IdOnly):
	title = models.CharField(max_length=100, null=False, blank=True, help_text="Permission's title")
	description = models.TextField(max_length=500, null=False, blank=True, help_text="Permission's description")
	module = models.ForeignKey('shared.Module', related_name='permissions_modules', null=True, help_text="Module Id")

	class Meta:
		verbose_name_plural = 'Permissions'
		index_together = ['id'],['title']

	def __str__(self):
		return self.title


class Module(IdOnly):
	title = models.CharField(max_length=100, null=False, blank=True, help_text="Module's title")
	description = models.TextField(max_length=500, null=False, blank=True, help_text="Module's description")

	class Meta:
		verbose_name_plural = 'Modules'
		index_together = ['id'],['title']

	def __str__(self):
		return self.title


class State(IdOnly):
	title = models.CharField(max_length=100, null=False, blank=True, help_text="State's title")
	abrv = models.CharField(max_length=5, null=False, blank=True, help_text="State's abreviation")
	currency = models.ForeignKey('shared.Currency', related_name='state_currencies', null=True, help_text="Currency Id")

	class Meta:
		verbose_name_plural = 'States'
		index_together = ['id'],['title']

	def __str__(self):
		return self.title


class Country(IdOnly):
	code = models.CharField(max_length=25, null=False, blank=True)
	title = models.CharField(max_length=150, null=False, blank=True)
	iso = models.CharField(max_length=5, null=False, blank=True)
	iso3 = models.CharField(max_length=5, null=False, blank=True)
	phonecode = models.IntegerField(null=True)

	class Meta:
		verbose_name_plural = 'Countries'
		index_together = ['id'],['title']

	def __str__(self):
		return self.title


class PhoneType(IdOnly):
	title = models.CharField(max_length=100, null=False, blank=True, help_text="PhoneType's title")

	class Meta:
		verbose_name_plural = 'PhoneTypes'
		index_together = ['id'],['title']

	def __str__(self):
		return self.title


class Language(IdOnly):
	title = models.CharField(max_length=150, null=False, blank=True)

	class Meta:
		verbose_name_plural = 'Languages'
		index_together = ['id'],['title']

	def __str__(self):
		return self.title


class Currency(IdOnly):
	title = models.CharField(max_length=150, null=False, blank=True)
	abrv = models.CharField(max_length=5, null=False, blank=True)
	sign = models.CharField(max_length=10, null=False, blank=True)
	market = models.DecimalField(decimal_places=2, max_digits=5, null=True, blank=True)

	class Meta:
		verbose_name_plural = 'Currencies'

	def __str__(self):
		return self.title


class Priority(IdOnly):
	title = models.CharField(max_length=150, null=False, blank=True)

	class Meta:
		verbose_name_plural = 'Priorities'

	def __str__(self):
		return self.title


class OutgoType(IdOnly):
	title = models.CharField(max_length=150, null=False, blank=True)

	class Meta:
		verbose_name_plural = 'OutgoTypes'

	def __str__(self):
		return self.title


class PayType(IdOnly):
	title = models.CharField(max_length=150, null=False, blank=True)

	class Meta:
		verbose_name_plural = 'PayTypes'

	def __str__(self):
		return self.title


class AlcRegType(IdOnly):
	title = models.CharField(max_length=150, null=False, blank=True)

	class Meta:
		verbose_name_plural = 'AlcRegTypes'

	def __str__(self):
		return self.title


class Turnover(IdOnly):
	title = models.CharField(max_length=150, null=False, blank=True)

	class Meta:
		verbose_name_plural = 'Turnovers'

	def __str__(self):
		return self.title


class EmployeeCount(IdOnly):
	title = models.CharField(max_length=150, null=False, blank=True)

	class Meta:
		verbose_name_plural = 'EmployeeCounts'

	def __str__(self):
		return self.title


class AccountStatus(IdOnly):
	title = models.CharField(max_length=150, null=False, blank=True)

	class Meta:
		verbose_name_plural = 'AccountStatuses'

	def __str__(self):
		return self.title


class FamilyStatus(IdOnly):
	title = models.CharField(max_length=150, null=False, blank=True)

	class Meta:
		verbose_name_plural = 'FamilyStatuses'

	def __str__(self):
		return self.title


class Gender(IdOnly):
	title = models.CharField(max_length=150, null=False, blank=True)

	class Meta:
		verbose_name_plural = 'Genders'

	def __str__(self):
		return self.title


class City(IdOnly):
	title = models.CharField(max_length=150, null=False, blank=True)
	psc = JSONField(null=True)
	okres = models.CharField(max_length=150, null=False, blank=True)
	kraj = models.CharField(max_length=10, null=False, blank=True)

	class Meta:
		verbose_name_plural = 'Cities'

	def __str__(self):
		return self.title


class MeasureUnit(IdOnly):
	title = models.CharField(max_length=150, null=False, blank=True)
	abrv = models.CharField(max_length=10, null=False, blank=True, help_text="Unit's areviation")

	class Meta:
		verbose_name_plural = 'MeasureUnits'
		index_together = ['id'],['title']

	def __str__(self):
		return self.title


class Bank(IdOnly):
	title = models.CharField(max_length=150, null=False, blank=True)
	code = models.CharField(max_length=10, null=False, blank=True, help_text="Bank's code")
	swift = models.CharField(max_length=50, null=False, blank=True, help_text="Bank's swift code")
	mark = models.CharField(max_length=50, null=False, blank=True, help_text="Bank's mark")

	class Meta:
		verbose_name_plural = 'Banks'
		index_together = ['id'],['title']

	def __str__(self):
		return self.title


class Source(IdOnly):
	title = models.CharField(max_length=150, null=False, blank=True)

	class Meta:
		verbose_name_plural = 'Sources'

	def __str__(self):
		return self.title