# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-04-20 10:06
from __future__ import unicode_literals

import django.contrib.postgres.fields.jsonb
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AccountStatus',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, help_text="Instance's unique ID", primary_key=True, serialize=False)),
                ('title', models.CharField(blank=True, max_length=150)),
            ],
            options={
                'verbose_name_plural': 'AccountStatuses',
            },
        ),
        migrations.CreateModel(
            name='AlcRegType',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, help_text="Instance's unique ID", primary_key=True, serialize=False)),
                ('title', models.CharField(blank=True, max_length=150)),
            ],
            options={
                'verbose_name_plural': 'AlcRegTypes',
            },
        ),
        migrations.CreateModel(
            name='Bank',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, help_text="Instance's unique ID", primary_key=True, serialize=False)),
                ('title', models.CharField(blank=True, max_length=150)),
                ('code', models.CharField(blank=True, help_text="Bank's code", max_length=10)),
                ('swift', models.CharField(blank=True, help_text="Bank's swift code", max_length=50)),
                ('mark', models.CharField(blank=True, help_text="Bank's mark", max_length=50)),
            ],
            options={
                'verbose_name_plural': 'Banks',
            },
        ),
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, help_text="Instance's unique ID", primary_key=True, serialize=False)),
                ('title', models.CharField(blank=True, max_length=150)),
                ('psc', django.contrib.postgres.fields.jsonb.JSONField(null=True)),
                ('okres', models.CharField(blank=True, max_length=150)),
                ('kraj', models.CharField(blank=True, max_length=10)),
            ],
            options={
                'verbose_name_plural': 'Cities',
            },
        ),
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, help_text="Instance's unique ID", primary_key=True, serialize=False)),
                ('code', models.CharField(blank=True, max_length=25)),
                ('title', models.CharField(blank=True, max_length=150)),
                ('iso', models.CharField(blank=True, max_length=5)),
                ('iso3', models.CharField(blank=True, max_length=5)),
                ('phonecode', models.IntegerField(null=True)),
            ],
            options={
                'verbose_name_plural': 'Countries',
            },
        ),
        migrations.CreateModel(
            name='Currency',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, help_text="Instance's unique ID", primary_key=True, serialize=False)),
                ('title', models.CharField(blank=True, max_length=150)),
                ('abrv', models.CharField(blank=True, max_length=5)),
                ('sign', models.CharField(blank=True, max_length=10)),
            ],
            options={
                'verbose_name_plural': 'Currencies',
            },
        ),
        migrations.CreateModel(
            name='EmployeeCount',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, help_text="Instance's unique ID", primary_key=True, serialize=False)),
                ('title', models.CharField(blank=True, max_length=150)),
            ],
            options={
                'verbose_name_plural': 'EmployeeCounts',
            },
        ),
        migrations.CreateModel(
            name='FamilyStatus',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, help_text="Instance's unique ID", primary_key=True, serialize=False)),
                ('title', models.CharField(blank=True, max_length=150)),
            ],
            options={
                'verbose_name_plural': 'FamilyStatuses',
            },
        ),
        migrations.CreateModel(
            name='Gender',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, help_text="Instance's unique ID", primary_key=True, serialize=False)),
                ('title', models.CharField(blank=True, max_length=150)),
            ],
            options={
                'verbose_name_plural': 'Genders',
            },
        ),
        migrations.CreateModel(
            name='Language',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, help_text="Instance's unique ID", primary_key=True, serialize=False)),
                ('title', models.CharField(blank=True, max_length=150)),
            ],
            options={
                'verbose_name_plural': 'Languages',
            },
        ),
        migrations.CreateModel(
            name='MeasureUnit',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, help_text="Instance's unique ID", primary_key=True, serialize=False)),
                ('title', models.CharField(blank=True, max_length=150)),
                ('abrv', models.CharField(blank=True, help_text="Unit's areviation", max_length=10)),
            ],
            options={
                'verbose_name_plural': 'MeasureUnits',
            },
        ),
        migrations.CreateModel(
            name='Module',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, help_text="Instance's unique ID", primary_key=True, serialize=False)),
                ('title', models.CharField(blank=True, help_text="Module's title", max_length=100)),
                ('description', models.TextField(blank=True, help_text="Module's description", max_length=500)),
            ],
            options={
                'verbose_name_plural': 'Modules',
            },
        ),
        migrations.CreateModel(
            name='OutgoType',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, help_text="Instance's unique ID", primary_key=True, serialize=False)),
                ('title', models.CharField(blank=True, max_length=150)),
            ],
            options={
                'verbose_name_plural': 'OutgoTypes',
            },
        ),
        migrations.CreateModel(
            name='PayType',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, help_text="Instance's unique ID", primary_key=True, serialize=False)),
                ('title', models.CharField(blank=True, max_length=150)),
            ],
            options={
                'verbose_name_plural': 'PayTypes',
            },
        ),
        migrations.CreateModel(
            name='Permission',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, help_text="Instance's unique ID", primary_key=True, serialize=False)),
                ('title', models.CharField(blank=True, help_text="Permission's title", max_length=100)),
                ('description', models.TextField(blank=True, help_text="Permission's description", max_length=500)),
                ('module', models.ForeignKey(help_text='Module Id', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='permissions_modules', to='shared.Module')),
            ],
            options={
                'verbose_name_plural': 'Permissions',
            },
        ),
        migrations.CreateModel(
            name='PhoneType',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, help_text="Instance's unique ID", primary_key=True, serialize=False)),
                ('title', models.CharField(blank=True, help_text="PhoneType's title", max_length=100)),
            ],
            options={
                'verbose_name_plural': 'PhoneTypes',
            },
        ),
        migrations.CreateModel(
            name='Priority',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, help_text="Instance's unique ID", primary_key=True, serialize=False)),
                ('title', models.CharField(blank=True, max_length=150)),
            ],
            options={
                'verbose_name_plural': 'Priorities',
            },
        ),
        migrations.CreateModel(
            name='Source',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, help_text="Instance's unique ID", primary_key=True, serialize=False)),
                ('title', models.CharField(blank=True, max_length=150)),
            ],
            options={
                'verbose_name_plural': 'Sources',
            },
        ),
        migrations.CreateModel(
            name='State',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, help_text="Instance's unique ID", primary_key=True, serialize=False)),
                ('title', models.CharField(blank=True, help_text="State's title", max_length=100)),
                ('abrv', models.CharField(blank=True, help_text="State's abreviation", max_length=5)),
            ],
            options={
                'verbose_name_plural': 'States',
            },
        ),
        migrations.CreateModel(
            name='Turnover',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, help_text="Instance's unique ID", primary_key=True, serialize=False)),
                ('title', models.CharField(blank=True, max_length=150)),
            ],
            options={
                'verbose_name_plural': 'Turnovers',
            },
        ),
        migrations.AlterIndexTogether(
            name='state',
            index_together=set([('title',), ('id',)]),
        ),
        migrations.AlterIndexTogether(
            name='phonetype',
            index_together=set([('title',), ('id',)]),
        ),
        migrations.AlterIndexTogether(
            name='module',
            index_together=set([('title',), ('id',)]),
        ),
        migrations.AlterIndexTogether(
            name='measureunit',
            index_together=set([('title',), ('id',)]),
        ),
        migrations.AlterIndexTogether(
            name='language',
            index_together=set([('title',), ('id',)]),
        ),
        migrations.AlterIndexTogether(
            name='country',
            index_together=set([('title',), ('id',)]),
        ),
        migrations.AlterIndexTogether(
            name='bank',
            index_together=set([('title',), ('id',)]),
        ),
        migrations.AlterIndexTogether(
            name='permission',
            index_together=set([('title',), ('id',)]),
        ),
    ]
