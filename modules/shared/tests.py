from django.test import TestCase, RequestFactory
from django.test import Client

class TestLocalization(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.client = Client()
        self.get_string = '/api/shared/localization/'

    def test_states(self):
        response = self.client.get(self.get_string + 'states/')
        self.assertEqual(response.status_code, 200)

    def test_cities(self):
        response = self.client.get(self.get_string + 'cities/')
        self.assertEqual(response.status_code, 200)

    def test_countries(self):
        response = self.client.get(self.get_string + 'countries/')
        self.assertEqual(response.status_code, 200)

    def test_languages(self):
        response = self.client.get(self.get_string + 'languages/')
        self.assertEqual(response.status_code, 200)

    def test_currencies(self):
        response = self.client.get(self.get_string + 'currencies/')
        self.assertEqual(response.status_code, 200)

class TestDirectory(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.client = Client()
        self.get_string = '/api/shared/directory/'

    def test_phone_types(self):
        response = self.client.get(self.get_string + 'phone-types/')
        self.assertEqual(response.status_code, 200)

    def test_priorities(self):
        response = self.client.get(self.get_string + 'priorities/')
        self.assertEqual(response.status_code, 200)

    def test_outgo_types(self):
        response = self.client.get(self.get_string + 'outgo-types/')
        self.assertEqual(response.status_code, 200)

    def test_pay_types(self):
        response = self.client.get(self.get_string + 'pay-types/')
        self.assertEqual(response.status_code, 200)

    def test_alc_reg_types(self):
        response = self.client.get(self.get_string + 'alc-reg-types/')
        self.assertEqual(response.status_code, 200)

    def test_turnovers(self):
        response = self.client.get(self.get_string + 'turnovers/')
        self.assertEqual(response.status_code, 200)

    def test_employee_counts(self):
        response = self.client.get(self.get_string + 'employee-counts/')
        self.assertEqual(response.status_code, 200)

    def test_account_statuses(self):
        response = self.client.get(self.get_string + 'account-statuses/')
        self.assertEqual(response.status_code, 200)

    def test_family_statuses(self):
        response = self.client.get(self.get_string + 'family-statuses/')
        self.assertEqual(response.status_code, 200)

    def test_genders(self):
        response = self.client.get(self.get_string + 'genders/')
        self.assertEqual(response.status_code, 200)

    def test_sources(self):
        response = self.client.get(self.get_string + 'sources/')
        self.assertEqual(response.status_code, 200)

class TestWarehouse(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.client = Client()
        self.get_string = '/api/shared/warehouse/'

    def test_measure_units(self):
        response = self.client.get(self.get_string + 'measure-units/')
        self.assertEqual(response.status_code, 200)

class TestFinance(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.client = Client()
        self.get_string = '/api/shared/finance/'

    def test_banks(self):
        response = self.client.get(self.get_string + 'banks/')
        self.assertEqual(response.status_code, 200)