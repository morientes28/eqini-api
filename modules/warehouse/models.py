from __future__ import unicode_literals
from django.db import models
from django.contrib.postgres.fields import JSONField
from config.orm.inherit import CRUDInfo, BaseTitleSlugOrgID, BaseOrgID
from django.db.models.signals import pre_save
from config.orm.signals import create_instance_slug

class Item(BaseTitleSlugOrgID, CRUDInfo):
	# Base Info
	code = models.CharField(max_length=50, null=False, blank=True)
	serial_number = models.CharField(max_length=50, null=False, blank=True)
	plu_number = models.CharField(max_length=50, null=False, blank=True)
	atest_code = models.CharField(max_length=50, null=False, blank=True)
	manufacture_dosage = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)
	origin_country = models.ForeignKey('lists.Country', related_name='item_originCountry', null=True)
	manufacture_year = models.IntegerField(null=True, blank=True)

	# Typology
	is_goods = models.BooleanField(default=False)
	is_packaging = models.BooleanField(default=False)
	is_service = models.BooleanField(default=False)
	is_product = models.BooleanField(default=False)

	# Chain
	suppliers = models.ManyToManyField('directory.Client', related_name='item_suppliers')
	manufacturer = models.ManyToManyField('directory.Client', related_name='item_producers')

	# Physical
	width = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)
	width_unit = models.ForeignKey('lists.MeasureUnit', related_name='item_widthUnit', null=True)
	height = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)
	height_unit = models.ForeignKey('lists.MeasureUnit', related_name='item_heightUnit', null=True)
	lenght = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)
	lenght_unit = models.ForeignKey('lists.MeasureUnit', related_name='item_lenghtUnit', null=True)
	weight = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)
	weight_unit = models.ForeignKey('lists.MeasureUnit', related_name='item_weightUnit', null=True)
	density = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)
	density_unit = models.ForeignKey('lists.MeasureUnit', related_name='item_densityUnit', null=True)
	volume = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)
	volume_unit = models.ForeignKey('lists.MeasureUnit', related_name='item_volumeUnit', null=True)

	# Packaging
	amount_unit = models.ForeignKey('lists.MeasureUnit', related_name='item_amountUnit', null=True)
	items_in_unit = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)
	package = models.ForeignKey('lists.PackageType', related_name='item_package', null=True)
	units_in_package = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)

	# Warranty
	warrranty_company = models.IntegerField(null=True, blank=True)
	warranty_idividual = models.IntegerField(null=True, blank=True)
	warranty_users = models.IntegerField(null=True, blank=True)

	# Pricing
	currency = models.ForeignKey('lists.Currency', related_name='item_currency', null=True)
	is_quantity_discounts = models.BooleanField(default=False)
	is_quantity_discounts_sale = models.BooleanField(default=False)
	buying_price = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)
	vat_level = models.ForeignKey('lists.VatType', related_name='item_vatType', null=True)
	consumer_tax = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)
	clo = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)
	transport_margin = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)
	item_account = models.ForeignKey('lists.FinAccount', related_name='item_itemAccount', null=True)
	sale = models.ForeignKey('lists.SalesList', related_name='item_sale', null=True)
	discounted_price = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)
	is_enabled_discount_cash_register = models.BooleanField(default=False)
	onsale = models.BooleanField(default=False)

	# Stocking
	min_stock = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)
	max_stock = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)
	units_in_box = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)
	units_on_palette = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)
	last_stock_check = models.DateTimeField(auto_now=False, auto_now_add=False, null=True)
	is_new = models.BooleanField(default=False)
	is_bestseller = models.BooleanField(default=False)
	new_start = models.DateTimeField(auto_now=False, auto_now_add=False, null=True)
	new_end = models.DateTimeField(auto_now=False, auto_now_add=False, null=True)

	# Exports
	is_exported_branches = models.BooleanField(default=False)
	is_exported_cash_register = models.BooleanField(default=False)
	is_exported_eshop = models.BooleanField(default=False)
	is_consignation = models.BooleanField(default=False)
	is_unit_serial_numbers = models.BooleanField(default=False)

	# Other
	alcohol_amount = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)
	clo_list_number = models.CharField(max_length=50, null=False, blank=True)
	is_in_eshop = models.BooleanField(default=False)
	external_eshop_id = models.CharField(max_length=50, null=False, blank=True)
	is_in_restaurant = models.BooleanField(default=False)
	offering_list_page = models.IntegerField(null=True, blank=True)
	item_relation_item = models.ForeignKey('warehouse.Item', related_name='item_itemRelationItem', null=True)

	class Meta:
		verbose_name_plural = 'Items'
		index_together = ['id'],['title'],['serial_number']

	def __str__(self):
		return self.title

def pre_save_Item_receiver(sender, instance, *args, **kwargs):
# create tenant data
	if not instance.slug:
		instance.slug = create_instance_slug(instance)
pre_save.connect(pre_save_Item_receiver, sender=Item)

class ItemTitle(BaseOrgID):
	item = models.ForeignKey('warehouse.Item', related_name='itemTitle_title', null=True)
	language = models.ForeignKey('lists.Language', related_name='itemTitle_language', null=True)
	name = models.CharField(max_length=100, null=False, blank=True)
	description = models.TextField(null=True, blank=True)

	class Meta:
		verbose_name_plural = 'ItemTitles'
		index_together = ['id'],['item']

	def __str__(self):
		return self.name

class EanCode(BaseOrgID):
	item = models.ForeignKey('warehouse.Item', related_name='eanCode_title', null=True)
	code_type = models.ForeignKey('lists.EanType', related_name='eanCode_type', null=True)
	ean = models.CharField(max_length=100, null=False, blank=True)
	amount = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)

	class Meta:
		verbose_name_plural = 'EanCodes'
		index_together = ['id'],['item']

	def __str__(self):
		return self.ean

class ItemPrice(BaseOrgID):
	item = models.ForeignKey('warehouse.Item', related_name='itemPrice_title', null=True)
	price_level = models.ForeignKey('lists.PriceLevel', related_name='eanCode_priceLevel', null=True)
	currency = models.ForeignKey('lists.currency', related_name='itemPrice_currency', null=True)
	price = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)

	class Meta:
		verbose_name_plural = 'ItemPrices'
		index_together = ['id'],['item']

	def __str__(self):
		return self.id

class ItemIndividualPrice(BaseOrgID):
	item = models.ForeignKey('warehouse.Item', related_name='itemIndividualPrice_title', null=True)
	client = models.ForeignKey('directory.Client', related_name='itemIndividualPrice_client', null=True)
	currency = models.ForeignKey('lists.currency', related_name='itemIndividualPrice_currency', null=True)
	price = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)
	is_w_vat = models.BooleanField(default=False)
	is_fixed = models.BooleanField(default=False)

	class Meta:
		verbose_name_plural = 'ItemIndividualPrices'
		index_together = ['id'],['item']

	def __str__(self):
		return self.id

class QuantityDiscount(BaseOrgID):
	item = models.ForeignKey('warehouse.Item', related_name='quantityDiscount_title', null=True)
	price = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)
	top = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)
	bottom = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)
	discount = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)
	start_date = models.DateTimeField(auto_now=False, auto_now_add=False, null=True)
	end_date = models.DateTimeField(auto_now=False, auto_now_add=False, null=True)
	discounted_price = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)
	is_w_vat = models.BooleanField(default=False)

	class Meta:
		verbose_name_plural = 'QuantityDiscounts'
		index_together = ['id'],['item']

	def __str__(self):
		return self.id

class ItemReplacement(BaseOrgID):
	item = models.ForeignKey('warehouse.Item', related_name='itemReplacement_item', null=True)
	replacement = models.ForeignKey('warehouse.Item', related_name='itemReplacement_replacement', null=True)
	memo = models.TextField(null=True, blank=True)

	class Meta:
		verbose_name_plural = 'ItemReplacements'
		index_together = ['id'],['item']

	def __str__(self):
		return self.id

class RelatedItem(BaseOrgID):
	item = models.ForeignKey('warehouse.Item', related_name='relatedItem_item', null=True)
	related = models.ForeignKey('warehouse.Item', related_name='relatedItem_related', null=True)
	memo = models.TextField(null=True, blank=True)

	class Meta:
		verbose_name_plural = 'RelatedItems'
		index_together = ['id'],['item']

	def __str__(self):
		return self.id

class CompetitionPrice(BaseOrgID):
	item = models.ForeignKey('warehouse.Item', related_name='competitionPrice_item', null=True)
	memo = models.TextField(null=True, blank=True)
	competitor = models.ForeignKey('directory.Client', related_name='competitionPrice_competitor', null=True)
	start_date = models.DateTimeField(auto_now=False, auto_now_add=False, null=True)
	end_date = models.DateTimeField(auto_now=False, auto_now_add=False, null=True)
	discounted_price = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)
	regular_price = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)

	class Meta:
		verbose_name_plural = 'CompetitionPrice'
		index_together = ['id'],['item']

	def __str__(self):
		return self.id

class ItemComponent(BaseOrgID):
	item = models.ForeignKey('warehouse.Item', related_name='itemComponent_item', null=True)
	component = models.ForeignKey('warehouse.Item', related_name='itemComponent_component', null=True)
	memo = models.TextField(null=True, blank=True)

	class Meta:
		verbose_name_plural = 'ItemComponents'
		index_together = ['id'],['item']

	def __str__(self):
		return self.id

class ItemPackage(BaseOrgID):
	item = models.ForeignKey('warehouse.Item', related_name='itemPackage_item', null=True)
	package = models.ForeignKey('lists.PackageType', related_name='itemPackage_package', null=True)
	weight = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)
	is_summed = models.BooleanField(default=False)

	class Meta:
		verbose_name_plural = 'ItemPackages'
		index_together = ['id'],['item'],['package']

	def __str__(self):
		return self.id

class ItemOtherPrice(BaseOrgID):
	item = models.ForeignKey('warehouse.Item', related_name='itemOtherPrice_item', null=True)
	currency = models.ForeignKey('lists.Currency', related_name='itemOtherPrice_currency', null=True)
	price = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)

	class Meta:
		verbose_name_plural = 'ItemOtherPrice'
		index_together = ['id'],['item'],['currency']

	def __str__(self):
		return self.id

class UnitSerialNumber(BaseOrgID):
	serial_number = models.CharField(max_length=100, null=False, blank=True)

	class Meta:
		verbose_name_plural = 'UnitSerialNumber'
		index_together = ['id']

	def __str__(self):
		return self.id

# File directory
def picture_directory_path(instance, filename):
	# file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
	return 'org_{0}/{1}/{2}'.format(instance.orgid, instance.item, filename)

class ItemPicture(BaseOrgID):
	item = models.ForeignKey('warehouse.Item', related_name='itemPicture_item', null=True)
	is_primary = models.BooleanField(default=False)
	picture = models.ImageField(upload_to=picture_directory_path, max_length=100, null=True, blank=True)

	class Meta:
		verbose_name_plural = 'ItemPictures'
		index_together = ['id'],['item']

	def __str__(self):
		return self.id

class WarehouseStatus(BaseOrgID):
	warehouse = models.ForeignKey('account.CompanyBranch', related_name='warehouseStatus_branch', null=True)
	item = models.ForeignKey('warehouse.Item', related_name='warehouseStatus_item', null=True)
	serial_number = models.ManyToManyField('warehouse.UnitSerialNumber', related_name='warehouseStatus_serialNumber')
	current_amount = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)
	enroute_amount = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)
	reserved_amount = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)
	is_enabled = models.BooleanField(default=False)
	max_stock = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)
	min_stock = models.DecimalField(max_digits=15, decimal_places=4, null=True, blank=True)

	class Meta:
		verbose_name_plural = 'WarehouseStatuses'
		index_together = ['id'],['warehouse'],['item'],['warehouse','item'],

	def __str__(self):
		return self.id

