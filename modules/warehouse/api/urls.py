from django.conf.urls import url, include
from modules.warehouse.api.view.items import *

urlpatterns = [
    url(r'^list/items/$', ItemsMinList.as_view(), name='items-list'),
]