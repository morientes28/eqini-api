from rest_framework.validators import UniqueTogetherValidator
from rest_framework import serializers

from modules.warehouse.models import Item

class ItemsMinSerializer(serializers.ModelSerializer):

	class Meta:
		model = Item
		fields = ('id', 'title')

class ItemsSerializer(serializers.ModelSerializer):
	created_by = serializers.StringRelatedField()
	updated_by = serializers.StringRelatedField()
	deleted_by = serializers.StringRelatedField()
	restored_by = serializers.StringRelatedField()

	class Meta:
		model = Item
		fields = (

		)