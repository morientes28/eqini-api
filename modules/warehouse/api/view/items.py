from django.http import Http404

from rest_framework import status
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from rest_framework.response import Response
from rest_framework.permissions import AllowAny

from rest_framework.renderers import JSONRenderer

from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.authentication import BasicAuthentication

from modules.warehouse.models import Item
from modules.warehouse.api.serializer.items import *

class ItemsMinList(generics.ListAPIView):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

	# Check the request type and select the corrent serializer
	def get_serializer_class(self):
		return ItemsMinSerializer

	# Get the correct queryset
	def get_queryset(self):
		return Item.objects.all()#filter(
		# 	Q(orgid = GetOrgID(self.request))
		# )