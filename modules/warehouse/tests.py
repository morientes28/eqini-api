from django.test import TestCase, RequestFactory
from django.test import Client

class TestList(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.client = Client()
        self.get_string = 'api/warehouse/list/'

    def test_items(self):
        response = self.client.get(self.get_string + 'items/')
        self.assertEqual(response.status_code, 200)
