from django.test import TestCase, RequestFactory
from django.test import Client

class TestWarehouse(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.client = Client()
        self.get_string = '/api/lists/warehouse/'

    def test_list_item_groups(self):
        response = self.client.get(self.get_string + 'list/item-groups/')
        self.assertEqual(response.status_code, 200)

    def test_item_groups(self):
        response = self.client.get(self.get_string + 'item-groups/')
        self.assertEqual(response.status_code, 200)

    #TODO item-groups/X

    def test_list_package_types(self):
        response = self.client.get(self.get_string + 'list/package-types/')
        self.assertEqual(response.status_code, 200)

    def test_package_types(self):
        response = self.client.get(self.get_string + 'package-types/')
        self.assertEqual(response.status_code, 200)

    #TODO package-types/X

    def test_list_order_types(self):
        response = self.client.get(self.get_string + 'list/order-types/')
        self.assertEqual(response.status_code, 200)

    def test_order_types(self):
        response = self.client.get(self.get_string + 'order-types/')
        self.assertEqual(response.status_code, 200)

    # TODO order-types/X

    def test_list_measure_units(self):
        response = self.client.get(self.get_string + 'list/measure-units/')
        self.assertEqual(response.status_code, 200)

    def test_measure_units(self):
        response = self.client.get(self.get_string + 'measure-units/')
        self.assertEqual(response.status_code, 200)

    # TODO measure-units/X

    def test_list_outgo_types(self):
        response = self.client.get(self.get_string + 'list/outgo-types/')
        self.assertEqual(response.status_code, 200)

    def test_outgo_types(self):
        response = self.client.get(self.get_string + 'outgo-types/')
        self.assertEqual(response.status_code, 200)

    # TODO outgo-types/X

    def test_list_pay_types(self):
        response = self.client.get(self.get_string + 'list/pay-types/')
        self.assertEqual(response.status_code, 200)

    def test_pay_types(self):
        response = self.client.get(self.get_string + 'pay-types/')
        self.assertEqual(response.status_code, 200)

    # TODO pay-types/X

    def test_list_movements(self):
        response = self.client.get(self.get_string + 'list/movements/')
        self.assertEqual(response.status_code, 200)

    def test_movements(self):
        response = self.client.get(self.get_string + 'movements/')
        self.assertEqual(response.status_code, 200)

    # TODO movements/X

    def test_list_ean_types(self):
        response = self.client.get(self.get_string + 'list/ean-types/')
        self.assertEqual(response.status_code, 200)

    def test_ean_types(self):
        response = self.client.get(self.get_string + 'ean-types/')
        self.assertEqual(response.status_code, 200)

    # TODO ean-types/X

class TestLocalization(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.client = Client()
        self.get_string = '/api/lists/localization/'

    def test_list_states(self):
        response = self.client.get(self.get_string + 'list/states/')
        self.assertEqual(response.status_code, 200)

    def test_states(self):
        response = self.client.get(self.get_string + 'states/')
        self.assertEqual(response.status_code, 200)

    # TODO states/X

    def test_list_cities(self):
        response = self.client.get(self.get_string + 'list/cities/')
        self.assertEqual(response.status_code, 200)

    def test_cities(self):
        response = self.client.get(self.get_string + 'cities/')
        self.assertEqual(response.status_code, 200)

    # TODO cities/X

    def test_list_countries(self):
        response = self.client.get(self.get_string + 'list/countries/')
        self.assertEqual(response.status_code, 200)

    def test_countries(self):
        response = self.client.get(self.get_string + 'countries/')
        self.assertEqual(response.status_code, 200)

    # TODO countries/X

    def test_list_languages(self):
        response = self.client.get(self.get_string + 'list/languages/')
        self.assertEqual(response.status_code, 200)

    def test_languages(self):
        response = self.client.get(self.get_string + 'languages/')
        self.assertEqual(response.status_code, 200)

    # TODO languages/X

    def test_list_currencies(self):
        response = self.client.get(self.get_string + 'list/currencies/')
        self.assertEqual(response.status_code, 200)

    def test_currencies(self):
        response = self.client.get(self.get_string + 'currencies/')
        self.assertEqual(response.status_code, 200)

    # TODO currencies/X

class TestDirectory(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.client = Client()
        self.get_string = '/api/lists/directory/'

    def test_list_phone_types(self):
        response = self.client.get(self.get_string + 'list/phone-types/')
        self.assertEqual(response.status_code, 200)

    def test_phone_types(self):
        response = self.client.get(self.get_string + 'phone-types/')
        self.assertEqual(response.status_code, 200)

    # TODO phone-types/X

    def test_list_priorities(self):
        response = self.client.get(self.get_string + 'list/priorities/')
        self.assertEqual(response.status_code, 200)

    def test_priorities(self):
        response = self.client.get(self.get_string + 'priorities/')
        self.assertEqual(response.status_code, 200)

    # TODO priorities/X

    def test_list_account_statuses(self):
        response = self.client.get(self.get_string + 'list/account-statuses/')
        self.assertEqual(response.status_code, 200)

    def test_account_statuses(self):
        response = self.client.get(self.get_string + 'account-statuses/')
        self.assertEqual(response.status_code, 200)

    # TODO account-statuses/X

    def test_list_territories(self):
        response = self.client.get(self.get_string + 'list/territories/')
        self.assertEqual(response.status_code, 200)

    def test_territories(self):
        response = self.client.get(self.get_string + 'territories/')
        self.assertEqual(response.status_code, 200)

    # TODO territories/X

class TestFinance(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.client = Client()
        self.get_string = '/api/lists/finance/'

    def test_list_price_levels(self):
        response = self.client.get(self.get_string + 'list/price-levels/')
        self.assertEqual(response.status_code, 200)

    def test_price_levels(self):
        response = self.client.get(self.get_string + 'price-levels/')
        self.assertEqual(response.status_code, 200)

    # TODO price-levels/X

    def test_list_vat_types(self):
        response = self.client.get(self.get_string + 'list/vat-types/')
        self.assertEqual(response.status_code, 200)

    def test_vat_types(self):
        response = self.client.get(self.get_string + 'vat-types/')
        self.assertEqual(response.status_code, 200)

    # TODO vat-types/X

    def test_list_fin_accounts(self):
        response = self.client.get(self.get_string + 'list/fin-accounts/')
        self.assertEqual(response.status_code, 200)

    def test_fin_accounts(self):
        response = self.client.get(self.get_string + 'fin-accounts/')
        self.assertEqual(response.status_code, 200)

    # TODO fin-accounts/X

    def test_list_banks(self):
        response = self.client.get(self.get_string + 'list/banks/')
        self.assertEqual(response.status_code, 200)

    def test_banks(self):
        response = self.client.get(self.get_string + 'banks/')
        self.assertEqual(response.status_code, 200)

    # TODO banks/X

    def test_list_sales_lists(self):
        response = self.client.get(self.get_string + 'list/sales-lists/')
        self.assertEqual(response.status_code, 200)

    def test_sales_lists(self):
        response = self.client.get(self.get_string + 'sales-lists/')
        self.assertEqual(response.status_code, 200)

    # TODO sales-lists/X

    def test_list_discount_types(self):
        response = self.client.get(self.get_string + 'list/discount-types/')
        self.assertEqual(response.status_code, 200)

    def test_discount_types(self):
        response = self.client.get(self.get_string + 'discount-types/')
        self.assertEqual(response.status_code, 200)

    # TODO discount-types/X
