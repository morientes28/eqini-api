from django.conf.urls import url, include
from modules.lists.api.view.warehouse import *
from modules.lists.api.view.localization import *
from modules.lists.api.view.directory import *
from modules.lists.api.view.finance import *

urlpatterns = [
    url(r'^warehouse/list/item-groups/$', ItemGroupsMinList.as_view(), name='itemgroups-list'),
    url(r'^warehouse/item-groups/$', ItemGroupsList.as_view(), name='itemgroups-all'),
    url(r'^warehouse/item-groups/(?P<id>[^/]+)/$', ItemGroupsDetail.as_view(), name='itemgroups-detail'),

    url(r'^warehouse/list/package-types/$', PackageTypesMinList.as_view(), name='packagetypes-list'),
    url(r'^warehouse/package-types/$', PackageTypesList.as_view(), name='packagetypes-all'),
    url(r'^warehouse/package-types/(?P<id>[^/]+)/$', PackageTypesDetail.as_view(), name='packagetypes-detail'),

    url(r'^warehouse/list/order-types/$', OrderTypesMinList.as_view(), name='ordertypes-list'),
    url(r'^warehouse/order-types/$', OrderTypesList.as_view(), name='ordertypes-all'),
    url(r'^warehouse/order-types/(?P<id>[^/]+)/$', OrderTypesDetail.as_view(), name='ordertypes-detail'),

    url(r'^warehouse/list/measure-units/$', MeasureUnitsMinList.as_view(), name='measureunits-list'),
    url(r'^warehouse/measure-units/$', MeasureUnitsList.as_view(), name='measureunits-all'),
    url(r'^warehouse/measure-units/(?P<id>[^/]+)/$', MeasureUnitsDetail.as_view(), name='measureunits-detail'),

    url(r'^warehouse/list/outgo-types/$', OutgoTypesMinList.as_view(), name='outgotypes-list'),
    url(r'^warehouse/outgo-types/$', OutgoTypesList.as_view(), name='outgotypes-all'),
    url(r'^warehouse/outgo-types/(?P<id>[^/]+)/$', OutgoTypesDetail.as_view(), name='outgotypes-detail'),

    url(r'^warehouse/list/pay-types/$', PayTypesMinList.as_view(), name='paytype-list'),
    url(r'^warehouse/pay-types/$', PayTypesList.as_view(), name='paytype-all'),
    url(r'^warehouse/pay-types/(?P<id>[^/]+)/$', PayTypesDetail.as_view(), name='paytype-detail'),

    url(r'^warehouse/list/movements/$', MovementsMinList.as_view(), name='movements-list'),
    url(r'^warehouse/movements/$', MovementsList.as_view(), name='movements-all'),
    url(r'^warehouse/movements/(?P<id>[^/]+)/$', MovementsDetail.as_view(), name='movements-detail'),

    url(r'^warehouse/list/ean-types/$', EanTypesMinList.as_view(), name='eantypes-list'),
    url(r'^warehouse/ean-types/$', EanTypesList.as_view(), name='eantypes-all'),
    url(r'^warehouse/ean-types/(?P<id>[^/]+)/$', EanTypesDetail.as_view(), name='eantypes-detail'),

    url(r'^finance/list/price-levels/$', PriceLevelsMinList.as_view(), name='pricelevels-list'),
    url(r'^finance/price-levels/$', PriceLevelsList.as_view(), name='pricelevels-all'),
    url(r'^finance/price-levels/(?P<id>[^/]+)/$', PriceLevelsDetail.as_view(), name='pricelevels-detail'),

    url(r'^localization/list/states/$', StatesMinList.as_view(), name='states-list'),
    url(r'^localization/states/$', StatesList.as_view(), name='states-all'),
    url(r'^localization/states/(?P<id>[^/]+)/$', StatesDetail.as_view(), name='states-detail'),
    url(r'^localization/states/add/$', StatesAdditing.as_view(), name='states-add'),

    url(r'^localization/list/cities/$', CitiesMinList.as_view(), name='cities-list'),
    url(r'^localization/cities/$', CitiesList.as_view(), name='cities-all'),
    url(r'^localization/cities/(?P<id>[^/]+)/$', CitiesDetail.as_view(), name='cities-detail'),

    url(r'^localization/list/countries/$', CountriesMinList.as_view(), name='countries-list'),
    url(r'^localization/countries/$', CountriesList.as_view(), name='countries-all'),
    url(r'^localization/countries/(?P<id>[^/]+)/$', CountriesDetail.as_view(), name='countries-detail'),

    url(r'^localization/list/languages/$', LanguagesMinList.as_view(), name='languages-list'),
    url(r'^localization/languages/$', LanguagesList.as_view(), name='languages-all'),
    url(r'^localization/languages/delete/$', LanguagesDelete.as_view(), name='languages-clear'),
    url(r'^localization/languages/add/$', LanguageAdditing.as_view(), name='languages-add'),
    url(r'^localization/languages/(?P<id>[^/]+)/$', LanguagesDetail.as_view(), name='languages-detail'),

    url(r'^localization/list/currencies/$', CurrenciesMinList.as_view(), name='currencies-list'),
    url(r'^localization/currencies/$', CurrenciesList.as_view(), name='currencies-all'),
    url(r'^localization/currencies/(?P<id>[^/]+)/$', CurrenciesDetail.as_view(), name='currencies-detail'),

    url(r'^directory/list/phone-types/$', PhoneTypesMinList.as_view(), name='phonetypes-list'),
    url(r'^directory/phone-types/$', PhoneTypesList.as_view(), name='phonetypes-all'),
    url(r'^directory/phone-types/(?P<id>[^/]+)/$', PhoneTypesDetail.as_view(), name='phonetypes-detail'),

    url(r'^directory/list/priorities/$', PrioritiesMinList.as_view(), name='priorities-list'),
    url(r'^directory/priorities/$', PrioritiesList.as_view(), name='priorities-all'),
    url(r'^directory/priorities/(?P<id>[^/]+)/$', PrioritiesDetail.as_view(), name='priorities-detail'),

    url(r'^directory/list/account-statuses/$', AccountStatusesMinList.as_view(), name='accountstatuses-list'),
    url(r'^directory/account-statuses/$', AccountStatusesList.as_view(), name='accountstatuses-all'),
    url(r'^directory/account-statuses/(?P<id>[^/]+)/$', AccountStatusesDetail.as_view(), name='accountstatuses-detail'),

    url(r'^directory/list/territories/$', TerritoriesMinList.as_view(), name='territories-list'),
    url(r'^directory/territories/$', TerritoriesList.as_view(), name='territories-all'),
    url(r'^directory/territories/(?P<id>[^/]+)/$', TerritoriesDetail.as_view(), name='territories-detail'),

    url(r'^finance/list/vat-types/$', VatTypesMinList.as_view(), name='vattypes-list'),
    url(r'^finance/vat-types/$', VatTypesList.as_view(), name='vattypes-all'),
    url(r'^finance/vat-types/(?P<id>[^/]+)/$', VatTypesDetail.as_view(), name='vattypes-detail'),

    url(r'^finance/list/fin-accounts/$', FinAccountsMinList.as_view(), name='finaccounts-list'),
    url(r'^finance/fin-accounts/$', FinAccountsList.as_view(), name='finaccounts-all'),
    url(r'^finance/fin-accounts/(?P<id>[^/]+)/$', FinAccountsDetail.as_view(), name='finaccounts-detail'),

    url(r'^finance/list/banks/$', BanksMinList.as_view(), name='banks-list'),
    url(r'^finance/banks/$', BanksList.as_view(), name='banks-all'),
    url(r'^finance/banks/(?P<id>[^/]+)/$', BanksDetail.as_view(), name='banks-detail'),

    url(r'^finance/list/sales-lists/$', SalesListsMinList.as_view(), name='saleslists-list'),
    url(r'^finance/sales-lists/$', SalesListsList.as_view(), name='saleslists-all'),
    url(r'^finance/sales-lists/(?P<id>[^/]+)/$', SalesListsDetail.as_view(), name='saleslists-detail'),

    url(r'^finance/list/discount-types/$', DiscountTypesMinList.as_view(), name='discounttypes-list'),
    url(r'^finance/discount-types/$', DiscountTypesList.as_view(), name='discounttypes-all'),
    url(r'^finance/discount-types/(?P<id>[^/]+)/$', DiscountTypesDetail.as_view(), name='discounttypes-detail'),
]