from rest_framework.validators import UniqueTogetherValidator
from rest_framework import serializers

from modules.lists.models import State, Country, Language, Currency, City
from modules.account.models import User, Company
from modules.shared.models import Currency as CurrencyShared


class CurrenciesSerializer(serializers.ModelSerializer):

	class Meta:
		model = Currency
		fields = ('id', 'abrv', 'market')


class StatesMinSerializer(serializers.ModelSerializer):

	class Meta:
		model = State
		fields = ('id', 'title')

class StatesSerializer(serializers.ModelSerializer):
	created_by = serializers.StringRelatedField()
	updated_by = serializers.StringRelatedField()
	deleted_by = serializers.StringRelatedField()
	restored_by = serializers.StringRelatedField()
	currency = CurrenciesSerializer(many=False, read_only=False) #= serializers.PrimaryKeyRelatedField(queryset=Currency.objects.all(), required=False, allow_null=True)

	class Meta:
		model = State
		fields = ('id', 'title', 'slug', 'created_by', 'updated_by', 'deleted_by', 'restored_by', 'created_at', 'updated_at', 'deleted_at', 'restored_at', 'abrv', 'currency')

class StatesManageSerializer(serializers.ModelSerializer):
	title = serializers.CharField(max_length=100, allow_blank=True, required=False)
	orgid = serializers.PrimaryKeyRelatedField(queryset=Company.objects.all(), required=False, allow_null=True)
	created_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	updated_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	restored_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted = serializers.BooleanField(required=False)
	deleted_at = serializers.DateTimeField(required=False, allow_null=True)
	currency =  serializers.PrimaryKeyRelatedField(queryset=CurrencyShared.objects.all(), required=False, allow_null=True)
	#CurrenciesSerializer(many=False, read_only=False)
	class Meta:
		model = State
		fields = ('__all__')

	def create(self, validated_data):
		# fixme: zamockovat
		validated_data['created_by'] = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')
		state = State.objects.create(**validated_data)
		state.save()
		return state

	def update(self, instance, validated_data):
		instance.title = validated_data.get('title', instance.title)
		instance.abrv = validated_data.get('abrv', instance.abrv)
		instance.currency = validated_data.get('currency', instance.currency)

		#instance.updated_by = validated_data.get('updated_by', instance.updated_by)
		# fixme: zamockovat
		instance.updated_by = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')
		instance.created_by = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')

		instance.deleted_by = validated_data.get('deleted_by', instance.deleted_by)
		instance.restored_by = validated_data.get('restored_by', instance.restored_by)
		instance.deleted = validated_data.get('deleted', instance.deleted)
		instance.deleted_at = validated_data.get('deleted_at', instance.deleted_at)

		instance.save()
		return instance

class CitiesMinSerializer(serializers.ModelSerializer):

	class Meta:
		model = City
		fields = ('id', 'title', 'psc')

class CitiesSerializer(serializers.ModelSerializer):
	created_by = serializers.StringRelatedField()
	updated_by = serializers.StringRelatedField()
	deleted_by = serializers.StringRelatedField()
	restored_by = serializers.StringRelatedField()

	class Meta:
		model = City
		fields = ('id', 'title', 'slug', 'psc', 'okres', 'kraj', 'created_by', 'updated_by', 'deleted_by', 'restored_by', 'created_at', 'updated_at', 'deleted_at', 'restored_at')

class CitiesManageSerializer(serializers.ModelSerializer):
	title = serializers.CharField(max_length=100, allow_blank=True, required=False)
	okres = serializers.CharField(max_length=100, allow_blank=True, required=False)
	kraj = serializers.CharField(max_length=10, allow_blank=True, required=False)
	psc = serializers.JSONField(required=False)
	orgid = serializers.PrimaryKeyRelatedField(queryset=Company.objects.all(), required=False, allow_null=True)
	created_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	updated_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	restored_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted = serializers.BooleanField(required=False)
	deleted_at 	= serializers.DateTimeField(required=False, allow_null=True)
	
	class Meta:
		model = City
		fields = ('__all__')

	def create(self, validated_data):
		# fixme: zamockovat
		validated_data['created_by'] = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')

		city = City.objects.create(**validated_data)
		city.save()
		return city

	def update(self, instance, validated_data):
		instance.title = validated_data.get('title', instance.title)
		instance.psc = validated_data.get('psc', instance.psc)
		instance.okres = validated_data.get('okres', instance.okres)
		instance.kraj = validated_data.get('kraj', instance.kraj)
		#instance.updated_by = validated_data.get('updated_by', instance.updated_by)
		# fixme: zamockovat
		instance.updated_by = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')
		instance.created_by = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')

		instance.deleted_by = validated_data.get('deleted_by', instance.deleted_by)
		instance.restored_by = validated_data.get('restored_by', instance.restored_by)
		instance.deleted = validated_data.get('deleted', instance.deleted)
		instance.deleted_at = validated_data.get('deleted_at', instance.deleted_at)

		instance.save()
		return instance









class CountriesMinSerializer(serializers.ModelSerializer):
	class Meta:
		model = Country
		fields = ('id', 'title')

class CountriesSerializer(serializers.ModelSerializer):
	created_by = serializers.StringRelatedField()
	updated_by = serializers.StringRelatedField()
	deleted_by = serializers.StringRelatedField()
	restored_by = serializers.StringRelatedField()

	class Meta:
		model = Country
		fields = ('id', 'title', 'slug', 'code', 'iso', 'iso3', 'phonecode', 'created_by', 'updated_by', 'deleted_by', 'restored_by', 'created_at', 'updated_at', 'deleted_at', 'restored_at')

class CountriesManageSerializer(serializers.ModelSerializer):
	title = serializers.CharField(max_length=100, allow_blank=True, required=False)
	code = serializers.CharField(max_length=25, allow_blank=True, required=False)
	iso = serializers.CharField(max_length=5, allow_blank=True, required=False)
	iso3 = serializers.CharField(max_length=5, allow_blank=True, required=False)
	phonecode = serializers.IntegerField(required=False)
	orgid = serializers.PrimaryKeyRelatedField(queryset=Company.objects.all(), required=False, allow_null=True)
	created_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	updated_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	restored_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted = serializers.BooleanField(required=False)
	deleted_at 	= serializers.DateTimeField(required=False, allow_null=True)
	
	class Meta:
		model = Country
		fields = ('__all__')

	def create(self, validated_data):
		# fixme: zamockovat
		validated_data['created_by'] = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')

		country = Country.objects.create(**validated_data)
		country.save()
		return country

		# try:
		# 	Country.objects.get(title=validated_data['title'])
		# 	return country
		# except:
		# 	country = Country.objects.create(**validated_data)
		# 	country.save()
		# 	return country

	def update(self, instance, validated_data):
		instance.title = validated_data.get('title', instance.title)
		instance.code = validated_data.get('code', instance.code)
		instance.iso = validated_data.get('iso', instance.iso)
		instance.iso3 = validated_data.get('iso3', instance.iso3)
		instance.phonecode = validated_data.get('phonecode', instance.phonecode)
		#instance.updated_by = validated_data.get('updated_by', instance.updated_by)
		# fixme: zamockovat
		instance.updated_by = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')
		instance.created_by = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')

		instance.deleted_by = validated_data.get('deleted_by', instance.deleted_by)
		instance.restored_by = validated_data.get('restored_by', instance.restored_by)
		instance.deleted = validated_data.get('deleted', instance.deleted)
		instance.deleted_at = validated_data.get('deleted_at', instance.deleted_at)

		instance.save()
		return instance

class LanguagesMinSerializer(serializers.ModelSerializer):

	class Meta:
		model = Language
		fields = ('id', 'title')

class LanguagesSerializer(serializers.ModelSerializer):
	created_by = serializers.StringRelatedField()
	updated_by = serializers.StringRelatedField()
	deleted_by = serializers.StringRelatedField()
	restored_by = serializers.StringRelatedField()

	class Meta:
		model = Language
		fields = ('id', 'title', 'slug', 'created_by', 'updated_by', 'deleted_by', 'restored_by', 'created_at', 'updated_at', 'deleted_at', 'restored_at')

class LanguagesManageSerializer(serializers.ModelSerializer):
	title = serializers.CharField(max_length=100, allow_blank=True, required=False)
	orgid = serializers.PrimaryKeyRelatedField(queryset=Company.objects.all(), required=False, allow_null=True)
	created_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	updated_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	restored_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted = serializers.BooleanField(required=False)
	deleted_at 	= serializers.DateTimeField(required=False, allow_null=True)
	
	class Meta:
		model = Language
		fields = ('__all__')

	def create(self, validated_data):
		# fixme: zamockovat
		validated_data['created_by'] = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')

		language = Language.objects.create(**validated_data)
		language.save()
		return language

	def update(self, instance, validated_data):
		print(instance)
		instance.title = validated_data.get('title', instance.title)
		#instance.updated_by = validated_data.get('updated_by', instance.updated_by)
		# fixme: zamockovat
		instance.updated_by = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')
		instance.created_by = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')

		instance.deleted_by = validated_data.get('deleted_by', instance.deleted_by)
		instance.restored_by = validated_data.get('restored_by', instance.restored_by)
		instance.deleted = validated_data.get('deleted', instance.deleted)
		instance.deleted_at = validated_data.get('deleted_at', instance.deleted_at)

		instance.save()
		return instance

class CurrenciesMinSerializer(serializers.ModelSerializer):

	class Meta:
		model = Currency
		fields = ('id', 'title')

class CurrenciesSerializer(serializers.ModelSerializer):
	created_by = serializers.StringRelatedField()
	updated_by = serializers.StringRelatedField()
	deleted_by = serializers.StringRelatedField()
	restored_by = serializers.StringRelatedField()

	class Meta:
		model = Currency
		fields = ('id', 'title', 'slug', 'created_by', 'updated_by', 'deleted_by', 'restored_by', 'created_at', 'updated_at', 'deleted_at', 'restored_at')

class CurrenciesManageSerializer(serializers.ModelSerializer):
	title = serializers.CharField(max_length=100, allow_blank=True, required=False)
	orgid = serializers.PrimaryKeyRelatedField(queryset=Company.objects.all(), required=False, allow_null=True)
	created_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	updated_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	restored_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted = serializers.BooleanField(required=False)
	deleted_at 	= serializers.DateTimeField(required=False, allow_null=True)
	
	class Meta:
		model = Currency
		fields = ('__all__')

	def create(self, validated_data):
		# fixme: zamockovat
		validated_data['created_by'] = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')

		language = Currency.objects.create(**validated_data)
		language.save()
		return language

	def update(self, instance, validated_data):
		instance.title = validated_data.get('title', instance.title)
		#instance.updated_by = validated_data.get('updated_by', instance.updated_by)
		# fixme: zamockovat
		instance.updated_by = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')
		instance.created_by = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')

		instance.deleted_by = validated_data.get('deleted_by', instance.deleted_by)
		instance.restored_by = validated_data.get('restored_by', instance.restored_by)
		instance.deleted = validated_data.get('deleted', instance.deleted)
		instance.deleted_at = validated_data.get('deleted_at', instance.deleted_at)

		instance.save()
		return instance