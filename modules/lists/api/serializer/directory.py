from rest_framework.validators import UniqueTogetherValidator
from rest_framework import serializers

from modules.lists.models import PhoneType, Priority, AccountStatus, Territory
from modules.account.models import User, Company

class PhoneTypesMinSerializer(serializers.ModelSerializer):

	class Meta:
		model = PhoneType
		fields = ('id', 'title')

class PhoneTypesSerializer(serializers.ModelSerializer):
	created_by = serializers.StringRelatedField()
	updated_by = serializers.StringRelatedField()
	deleted_by = serializers.StringRelatedField()
	restored_by = serializers.StringRelatedField()

	class Meta:
		model = PhoneType
		fields = ('id', 'title', 'slug', 'created_by', 'updated_by', 'deleted_by', 'restored_by', 'created_at', 'updated_at', 'deleted_at', 'restored_at')

class PhoneTypesManageSerializer(serializers.ModelSerializer):
	title = serializers.CharField(max_length=100, allow_blank=True, required=False)
	orgid = serializers.PrimaryKeyRelatedField(queryset=Company.objects.all(), required=False, allow_null=True)
	created_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	updated_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	restored_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted = serializers.BooleanField(required=False)
	deleted_at 	= serializers.DateTimeField(required=False, allow_null=True)
	
	class Meta:
		model = PhoneType
		fields = ('__all__')

	def create(self, validated_data):
		# fixme: zamockovat
		validated_data['created_by'] = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')

		phonetype = PhoneType.objects.create(**validated_data)
		phonetype.save()
		return phonetype

	def update(self, instance, validated_data):
		instance.title = validated_data.get('title', instance.title)
		#instance.updated_by = validated_data.get('updated_by', instance.updated_by)
		# fixme: zamockovat
		instance.updated_by = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')
		instance.created_by = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')

		instance.deleted_by = validated_data.get('deleted_by', instance.deleted_by)
		instance.restored_by = validated_data.get('restored_by', instance.restored_by)
		instance.deleted = validated_data.get('deleted', instance.deleted)
		instance.deleted_at = validated_data.get('deleted_at', instance.deleted_at)

		instance.save()
		return instance

class PrioritiesMinSerializer(serializers.ModelSerializer):
	class Meta:
		model = Priority
		fields = ('id', 'title')

class PrioritiesSerializer(serializers.ModelSerializer):
	created_by = serializers.StringRelatedField()
	updated_by = serializers.StringRelatedField()
	deleted_by = serializers.StringRelatedField()
	restored_by = serializers.StringRelatedField()

	class Meta:
		model = Priority
		fields = ('id', 'title', 'slug', 'created_by', 'updated_by', 'deleted_by', 'restored_by', 'created_at', 'updated_at', 'deleted_at', 'restored_at')

class PrioritiesManageSerializer(serializers.ModelSerializer):
	title = serializers.CharField(max_length=100, allow_blank=True, required=False)
	orgid = serializers.PrimaryKeyRelatedField(queryset=Company.objects.all(), required=False, allow_null=True)
	created_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	updated_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	restored_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted = serializers.BooleanField(required=False)
	deleted_at 	= serializers.DateTimeField(required=False, allow_null=True)
	
	class Meta:
		model = Priority
		fields = ('__all__')

	def create(self, validated_data):
		# fixme: zamockovat
		validated_data['created_by'] = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')

		priority = Priority.objects.create(**validated_data)
		priority.save()
		return priority

	def update(self, instance, validated_data):
		instance.title = validated_data.get('title', instance.title)
		#instance.updated_by = validated_data.get('updated_by', instance.updated_by)
		# fixme: zamockovat
		instance.updated_by = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')
		instance.created_by = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')

		instance.deleted_by = validated_data.get('deleted_by', instance.deleted_by)
		instance.restored_by = validated_data.get('restored_by', instance.restored_by)
		instance.deleted = validated_data.get('deleted', instance.deleted)
		instance.deleted_at = validated_data.get('deleted_at', instance.deleted_at)

		instance.save()
		return instance

class AccountStatusesMinSerializer(serializers.ModelSerializer):

	class Meta:
		model = AccountStatus
		fields = ('id', 'title')

class AccountStatusesSerializer(serializers.ModelSerializer):
	created_by = serializers.StringRelatedField()
	updated_by = serializers.StringRelatedField()
	deleted_by = serializers.StringRelatedField()
	restored_by = serializers.StringRelatedField()

	class Meta:
		model = AccountStatus
		fields = ('id', 'title', 'slug', 'created_by', 'updated_by', 'deleted_by', 'restored_by', 'created_at', 'updated_at', 'deleted_at', 'restored_at')

class AccountStatusesManageSerializer(serializers.ModelSerializer):
	title = serializers.CharField(max_length=100, allow_blank=True, required=False)
	orgid = serializers.PrimaryKeyRelatedField(queryset=Company.objects.all(), required=False, allow_null=True)
	created_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	updated_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	restored_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted = serializers.BooleanField(required=False)
	deleted_at 	= serializers.DateTimeField(required=False, allow_null=True)
	
	class Meta:
		model = AccountStatus
		fields = ('__all__')

	def create(self, validated_data):
		# fixme: zamockovat
		validated_data['created_by'] = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')

		status = AccountStatus.objects.create(**validated_data)
		status.save()
		return status

	def update(self, instance, validated_data):
		instance.title = validated_data.get('title', instance.title)
		#instance.updated_by = validated_data.get('updated_by', instance.updated_by)
		# fixme: zamockovat
		instance.updated_by = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')
		instance.created_by = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')

		instance.deleted_by = validated_data.get('deleted_by', instance.deleted_by)
		instance.restored_by = validated_data.get('restored_by', instance.restored_by)
		instance.deleted = validated_data.get('deleted', instance.deleted)
		instance.deleted_at = validated_data.get('deleted_at', instance.deleted_at)

		instance.save()
		return instance







class TerritoriesMinSerializer(serializers.ModelSerializer):

	class Meta:
		model = Territory
		fields = ('id', 'title')

class TerritoriesSerializer(serializers.ModelSerializer):
	created_by = serializers.StringRelatedField()
	updated_by = serializers.StringRelatedField()
	deleted_by = serializers.StringRelatedField()
	restored_by = serializers.StringRelatedField()

	class Meta:
		model = Territory
		fields = ('id', 'title', 'slug', 'created_by', 'updated_by', 'deleted_by', 'restored_by', 'created_at', 'updated_at', 'deleted_at', 'restored_at')

class TerritoriesManageSerializer(serializers.ModelSerializer):
	title = serializers.CharField(max_length=100, allow_blank=True, required=False)
	orgid = serializers.PrimaryKeyRelatedField(queryset=Company.objects.all(), required=False, allow_null=True)
	created_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	updated_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	restored_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted = serializers.BooleanField(required=False)
	deleted_at 	= serializers.DateTimeField(required=False, allow_null=True)
	
	class Meta:
		model = Territory
		fields = ('__all__')

	def create(self, validated_data):
		# fixme: zamockovat
		validated_data['created_by'] = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')

		status = Territory.objects.create(**validated_data)
		status.save()
		return status

	def update(self, instance, validated_data):
		instance.title = validated_data.get('title', instance.title)
		#instance.updated_by = validated_data.get('updated_by', instance.updated_by)
		# fixme: zamockovat
		instance.updated_by = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')
		instance.created_by = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')

		instance.deleted_by = validated_data.get('deleted_by', instance.deleted_by)
		instance.restored_by = validated_data.get('restored_by', instance.restored_by)
		instance.deleted = validated_data.get('deleted', instance.deleted)
		instance.deleted_at = validated_data.get('deleted_at', instance.deleted_at)

		instance.save()
		return instance