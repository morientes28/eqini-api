from rest_framework.validators import UniqueTogetherValidator
from rest_framework import serializers

from modules.lists.models import MeasureUnit, OutgoType, PayType, Movement, OrderType, PackageType, EanType, PriceLevel, ItemGroup
from modules.account.models import User, Company

class MeasureUnitsMinSerializer(serializers.ModelSerializer):

	class Meta:
		model = MeasureUnit
		fields = ('id', 'title', 'abrv')

class MeasureUnitsSerializer(serializers.ModelSerializer):
	created_by = serializers.StringRelatedField()
	updated_by = serializers.StringRelatedField()
	deleted_by = serializers.StringRelatedField()
	restored_by = serializers.StringRelatedField()

	class Meta:
		model = MeasureUnit
		fields = ('id', 'title', 'abrv', 'slug', 'created_by', 'updated_by', 'deleted_by', 'restored_by', 'created_at', 'updated_at', 'deleted_at', 'restored_at')

class MeasureUnitsManageSerializer(serializers.ModelSerializer):
	title = serializers.CharField(max_length=100, allow_blank=True, required=False)
	abrv = serializers.CharField(max_length=10, allow_blank=True, required=False)
	orgid = serializers.PrimaryKeyRelatedField(queryset=Company.objects.all(), required=False, allow_null=True)
	created_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	updated_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	restored_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted = serializers.BooleanField(required=False)
	deleted_at 	= serializers.DateTimeField(required=False, allow_null=True)
	
	class Meta:
		model = MeasureUnit
		fields = ('__all__')

	def create(self, validated_data):
		# fixme: zamockovat
		validated_data['created_by'] = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')

		unit = MeasureUnit.objects.create(**validated_data)
		unit.save()
		return unit

	def update(self, instance, validated_data):
		instance.title = validated_data.get('title', instance.title)
		instance.abrv = validated_data.get('abrv', instance.abrv)
		#instance.updated_by = validated_data.get('updated_by', instance.updated_by)
		# fixme: zamockovat
		instance.updated_by = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')
		instance.created_by = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')

		instance.deleted_by = validated_data.get('deleted_by', instance.deleted_by)
		instance.restored_by = validated_data.get('restored_by', instance.restored_by)
		instance.deleted = validated_data.get('deleted', instance.deleted)
		instance.deleted_at = validated_data.get('deleted_at', instance.deleted_at)

		instance.save()
		return instance

class OutgoTypesMinSerializer(serializers.ModelSerializer):
	class Meta:
		model = OutgoType
		fields = ('id', 'title')

class OutgoTypesSerializer(serializers.ModelSerializer):
	created_by = serializers.StringRelatedField()
	updated_by = serializers.StringRelatedField()
	deleted_by = serializers.StringRelatedField()
	restored_by = serializers.StringRelatedField()

	class Meta:
		model = OutgoType
		fields = ('id', 'title', 'slug', 'created_by', 'updated_by', 'deleted_by', 'restored_by', 'created_at', 'updated_at', 'deleted_at', 'restored_at')

class OutgoTypesManageSerializer(serializers.ModelSerializer):
	title = serializers.CharField(max_length=100, allow_blank=True, required=False)
	orgid = serializers.PrimaryKeyRelatedField(queryset=Company.objects.all(), required=False, allow_null=True)
	created_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	updated_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	restored_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted = serializers.BooleanField(required=False)
	deleted_at 	= serializers.DateTimeField(required=False, allow_null=True)
	
	class Meta:
		model = OutgoType
		fields = ('__all__')

	def create(self, validated_data):
		outgo = OutgoType.objects.create(**validated_data)
		outgo.save()
		return outgo

	def update(self, instance, validated_data):
		instance.title = validated_data.get('title', instance.title)
		instance.updated_by = validated_data.get('updated_by', instance.updated_by)
		instance.deleted_by = validated_data.get('deleted_by', instance.deleted_by)
		instance.restored_by = validated_data.get('restored_by', instance.restored_by)
		instance.deleted = validated_data.get('deleted', instance.deleted)
		instance.deleted_at = validated_data.get('deleted_at', instance.deleted_at)

		instance.save()
		return instance

class PayTypesMinSerializer(serializers.ModelSerializer):

	class Meta:
		model = PayType
		fields = ('id', 'title')

class PayTypesSerializer(serializers.ModelSerializer):
	created_by = serializers.StringRelatedField()
	updated_by = serializers.StringRelatedField()
	deleted_by = serializers.StringRelatedField()
	restored_by = serializers.StringRelatedField()

	class Meta:
		model = PayType
		fields = ('id', 'title', 'slug', 'created_by', 'updated_by', 'deleted_by', 'restored_by', 'created_at', 'updated_at', 'deleted_at', 'restored_at')

class PayTypesManageSerializer(serializers.ModelSerializer):
	title = serializers.CharField(max_length=100, allow_blank=True, required=False)
	orgid = serializers.PrimaryKeyRelatedField(queryset=Company.objects.all(), required=False, allow_null=True)
	created_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	updated_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	restored_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted = serializers.BooleanField(required=False)
	deleted_at 	= serializers.DateTimeField(required=False, allow_null=True)
	
	class Meta:
		model = PayType
		fields = ('__all__')

	def create(self, validated_data):
		# fixme: zamockovat
		validated_data['created_by'] = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')

		paytype = PayType.objects.create(**validated_data)
		paytype.save()
		return paytype

	def update(self, instance, validated_data):
		instance.title = validated_data.get('title', instance.title)
		#instance.updated_by = validated_data.get('updated_by', instance.updated_by)
		# fixme: zamockovat
		instance.updated_by = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')
		instance.created_by = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')

		instance.deleted_by = validated_data.get('deleted_by', instance.deleted_by)
		instance.restored_by = validated_data.get('restored_by', instance.restored_by)
		instance.deleted = validated_data.get('deleted', instance.deleted)
		instance.deleted_at = validated_data.get('deleted_at', instance.deleted_at)

		instance.save()
		return instance

class MovementsMinSerializer(serializers.ModelSerializer):

	class Meta:
		model = Movement
		fields = ('id', 'title')

class MovementsSerializer(serializers.ModelSerializer):
	created_by = serializers.StringRelatedField()
	updated_by = serializers.StringRelatedField()
	deleted_by = serializers.StringRelatedField()
	restored_by = serializers.StringRelatedField()

	class Meta:
		model = Movement
		fields = ('id', 'title', 'slug', 'created_by', 'updated_by', 'deleted_by', 'restored_by', 'created_at', 'updated_at', 'deleted_at', 'restored_at')

class MovementsManageSerializer(serializers.ModelSerializer):
	title = serializers.CharField(max_length=100, allow_blank=True, required=False)
	orgid = serializers.PrimaryKeyRelatedField(queryset=Company.objects.all(), required=False, allow_null=True)
	created_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	updated_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	restored_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted = serializers.BooleanField(required=False)
	deleted_at 	= serializers.DateTimeField(required=False, allow_null=True)
	
	class Meta:
		model = Movement
		fields = ('__all__')

	def create(self, validated_data):
		# fixme: zamockovat
		validated_data['created_by'] = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')

		movement = Movement.objects.create(**validated_data)
		movement.save()
		return movement

	def update(self, instance, validated_data):
		instance.title = validated_data.get('title', instance.title)
		#instance.updated_by = validated_data.get('updated_by', instance.updated_by)
		# fixme: zamockovat
		instance.updated_by = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')
		instance.created_by = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')

		instance.deleted_by = validated_data.get('deleted_by', instance.deleted_by)
		instance.restored_by = validated_data.get('restored_by', instance.restored_by)
		instance.deleted = validated_data.get('deleted', instance.deleted)
		instance.deleted_at = validated_data.get('deleted_at', instance.deleted_at)

		instance.save()
		return instance

class OrderTypesMinSerializer(serializers.ModelSerializer):

	class Meta:
		model = OrderType
		fields = ('id', 'title')

class OrderTypesSerializer(serializers.ModelSerializer):
	created_by = serializers.StringRelatedField()
	updated_by = serializers.StringRelatedField()
	deleted_by = serializers.StringRelatedField()
	restored_by = serializers.StringRelatedField()

	class Meta:
		model = OrderType
		fields = ('id', 'title', 'slug', 'created_by', 'updated_by', 'deleted_by', 'restored_by', 'created_at', 'updated_at', 'deleted_at', 'restored_at')

class OrderTypesManageSerializer(serializers.ModelSerializer):
	title = serializers.CharField(max_length=100, allow_blank=True, required=False)
	orgid = serializers.PrimaryKeyRelatedField(queryset=Company.objects.all(), required=False, allow_null=True)
	created_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	updated_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	restored_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted = serializers.BooleanField(required=False)
	deleted_at 	= serializers.DateTimeField(required=False, allow_null=True)
	
	class Meta:
		model = OrderType
		fields = ('__all__')

	def create(self, validated_data):
		orderType = OrderType.objects.create(**validated_data)
		orderType.save()
		return orderType

	def update(self, instance, validated_data):
		instance.title = validated_data.get('title', instance.title)
		instance.updated_by = validated_data.get('updated_by', instance.updated_by)
		instance.deleted_by = validated_data.get('deleted_by', instance.deleted_by)
		instance.restored_by = validated_data.get('restored_by', instance.restored_by)
		instance.deleted = validated_data.get('deleted', instance.deleted)
		instance.deleted_at = validated_data.get('deleted_at', instance.deleted_at)

		instance.save()
		return instance

class PackageTypesMinSerializer(serializers.ModelSerializer):

	class Meta:
		model = PackageType
		fields = ('id', 'title')

class PackageTypesSerializer(serializers.ModelSerializer):
	created_by = serializers.StringRelatedField()
	updated_by = serializers.StringRelatedField()
	deleted_by = serializers.StringRelatedField()
	restored_by = serializers.StringRelatedField()

	class Meta:
		model = PackageType
		fields = ('id', 'title', 'slug', 'created_by', 'updated_by', 'deleted_by', 'restored_by', 'created_at', 'updated_at', 'deleted_at', 'restored_at')

class PackageTypesManageSerializer(serializers.ModelSerializer):
	title = serializers.CharField(max_length=100, allow_blank=True, required=False)
	orgid = serializers.PrimaryKeyRelatedField(queryset=Company.objects.all(), required=False, allow_null=True)
	created_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	updated_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	restored_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted = serializers.BooleanField(required=False)
	deleted_at 	= serializers.DateTimeField(required=False, allow_null=True)
	
	class Meta:
		model = PackageType
		fields = ('__all__')

	def create(self, validated_data):
		package = PackageType.objects.create(**validated_data)
		package.save()
		return package

	def update(self, instance, validated_data):
		instance.title = validated_data.get('title', instance.title)
		instance.updated_by = validated_data.get('updated_by', instance.updated_by)
		instance.deleted_by = validated_data.get('deleted_by', instance.deleted_by)
		instance.restored_by = validated_data.get('restored_by', instance.restored_by)
		instance.deleted = validated_data.get('deleted', instance.deleted)
		instance.deleted_at = validated_data.get('deleted_at', instance.deleted_at)

		instance.save()
		return instance

class EanTypesMinSerializer(serializers.ModelSerializer):

	class Meta:
		model = EanType
		fields = ('id', 'title')

class EanTypesSerializer(serializers.ModelSerializer):
	created_by = serializers.StringRelatedField()
	updated_by = serializers.StringRelatedField()
	deleted_by = serializers.StringRelatedField()
	restored_by = serializers.StringRelatedField()

	class Meta:
		model = EanType
		fields = ('id', 'title', 'slug', 'created_by', 'updated_by', 'deleted_by', 'restored_by', 'created_at', 'updated_at', 'deleted_at', 'restored_at')

class EanTypesManageSerializer(serializers.ModelSerializer):
	title = serializers.CharField(max_length=100, allow_blank=True, required=False)
	orgid = serializers.PrimaryKeyRelatedField(queryset=Company.objects.all(), required=False, allow_null=True)
	created_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	updated_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	restored_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted = serializers.BooleanField(required=False)
	deleted_at 	= serializers.DateTimeField(required=False, allow_null=True)
	
	class Meta:
		model = EanType
		fields = ('__all__')

	def create(self, validated_data):
		package = EanType.objects.create(**validated_data)
		package.save()
		return package

	def update(self, instance, validated_data):
		instance.title = validated_data.get('title', instance.title)
		instance.updated_by = validated_data.get('updated_by', instance.updated_by)
		instance.deleted_by = validated_data.get('deleted_by', instance.deleted_by)
		instance.restored_by = validated_data.get('restored_by', instance.restored_by)
		instance.deleted = validated_data.get('deleted', instance.deleted)
		instance.deleted_at = validated_data.get('deleted_at', instance.deleted_at)

		instance.save()
		return instance

class PriceLevelsMinSerializer(serializers.HyperlinkedModelSerializer):
	url = serializers.HyperlinkedIdentityField(view_name='pricelevels-detail', lookup_field='slug', lookup_url_kwarg='slug')

	class Meta:
		model = PriceLevel
		fields = ('url', 'id', 'title', 'slug')

class PriceLevelsSerializer(serializers.HyperlinkedModelSerializer):
	url = serializers.HyperlinkedIdentityField(view_name='pricelevels-detail', lookup_field='slug', lookup_url_kwarg='slug')
	created_by = serializers.StringRelatedField()
	updated_by = serializers.StringRelatedField()
	deleted_by = serializers.StringRelatedField()
	restored_by = serializers.StringRelatedField()

	class Meta:
		model = PriceLevel
		fields = ('id', 'url', 'title', 'slug', 'created_by', 'updated_by', 'deleted_by', 'restored_by', 'created_at', 'updated_at', 'deleted_at', 'restored_at')

class PriceLevelsManageSerializer(serializers.HyperlinkedModelSerializer):
	url = serializers.HyperlinkedIdentityField(view_name='pricelevels-detail', lookup_field='slug', lookup_url_kwarg='slug')
	title = serializers.CharField(max_length=100, allow_blank=True, required=False)
	orgid = serializers.PrimaryKeyRelatedField(queryset=Company.objects.all(), required=False, allow_null=True)
	created_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	updated_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	restored_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted = serializers.BooleanField(required=False)
	deleted_at 	= serializers.DateTimeField(required=False, allow_null=True)
	
	class Meta:
		model = PriceLevel
		fields = ('__all__')

	def create(self, validated_data):
		package = PriceLevel.objects.create(**validated_data)
		package.save()
		return package

	def update(self, instance, validated_data):
		instance.title = validated_data.get('title', instance.title)
		instance.updated_by = validated_data.get('updated_by', instance.updated_by)
		instance.deleted_by = validated_data.get('deleted_by', instance.deleted_by)
		instance.restored_by = validated_data.get('restored_by', instance.restored_by)
		instance.deleted = validated_data.get('deleted', instance.deleted)
		instance.deleted_at = validated_data.get('deleted_at', instance.deleted_at)

		instance.save()
		return instance

class ItemGroupsMinSerializer(serializers.ModelSerializer):

	class Meta:
		model = ItemGroup
		fields = ('id', 'title')

class ItemGroupsSerializer(serializers.ModelSerializer):
	created_by = serializers.StringRelatedField()
	updated_by = serializers.StringRelatedField()
	deleted_by = serializers.StringRelatedField()
	restored_by = serializers.StringRelatedField()

	class Meta:
		model = ItemGroup
		fields = ('id', 'title', 'slug', 'created_by', 'updated_by', 'deleted_by', 'restored_by', 'created_at', 'updated_at', 'deleted_at', 'restored_at')

class ItemGroupsManageSerializer(serializers.ModelSerializer):
	title = serializers.CharField(max_length=100, allow_blank=True, required=False)
	orgid = serializers.PrimaryKeyRelatedField(queryset=Company.objects.all(), required=False, allow_null=True)
	created_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	updated_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	restored_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted = serializers.BooleanField(required=False)
	deleted_at 	= serializers.DateTimeField(required=False, allow_null=True)
	
	class Meta:
		model = ItemGroup
		fields = ('__all__')

	def create(self, validated_data):
		package = ItemGroup.objects.create(**validated_data)
		package.save()
		return package

	def update(self, instance, validated_data):
		instance.title = validated_data.get('title', instance.title)
		instance.updated_by = validated_data.get('updated_by', instance.updated_by)
		instance.deleted_by = validated_data.get('deleted_by', instance.deleted_by)
		instance.restored_by = validated_data.get('restored_by', instance.restored_by)
		instance.deleted = validated_data.get('deleted', instance.deleted)
		instance.deleted_at = validated_data.get('deleted_at', instance.deleted_at)

		instance.save()
		return instance