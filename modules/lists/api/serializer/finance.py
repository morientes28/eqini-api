from rest_framework.validators import UniqueTogetherValidator
from rest_framework import serializers

from modules.lists.models import VatType, FinAccount, Bank, SalesList, DiscountType
from modules.account.models import User, Company

class VatTypesMinSerializer(serializers.ModelSerializer):

	class Meta:
		model = VatType
		fields = ('id', 'title')

class VatTypesSerializer(serializers.ModelSerializer):
	created_by = serializers.StringRelatedField()
	updated_by = serializers.StringRelatedField()
	deleted_by = serializers.StringRelatedField()
	restored_by = serializers.StringRelatedField()

	class Meta:
		model = VatType
		fields = ('id', 'title', 'slug', 'created_by', 'updated_by', 'deleted_by', 'restored_by', 'created_at', 'updated_at', 'deleted_at', 'restored_at')

class VatTypesManageSerializer(serializers.ModelSerializer):
	title = serializers.CharField(max_length=100, allow_blank=True, required=False)
	orgid = serializers.PrimaryKeyRelatedField(queryset=Company.objects.all(), required=False, allow_null=True)
	created_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	updated_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	restored_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted = serializers.BooleanField(required=False)
	deleted_at 	= serializers.DateTimeField(required=False, allow_null=True)
	
	class Meta:
		model = VatType
		fields = ('__all__')

	def create(self, validated_data):
		package = VatType.objects.create(**validated_data)
		package.save()
		return package

	def update(self, instance, validated_data):
		instance.title = validated_data.get('title', instance.title)
		instance.updated_by = validated_data.get('updated_by', instance.updated_by)
		instance.deleted_by = validated_data.get('deleted_by', instance.deleted_by)
		instance.restored_by = validated_data.get('restored_by', instance.restored_by)
		instance.deleted = validated_data.get('deleted', instance.deleted)
		instance.deleted_at = validated_data.get('deleted_at', instance.deleted_at)

		instance.save()
		return instance

class FinAccountsMinSerializer(serializers.ModelSerializer):

	class Meta:
		model = FinAccount
		fields = ('id', 'title')

class FinAccountsSerializer(serializers.ModelSerializer):
	created_by = serializers.StringRelatedField()
	updated_by = serializers.StringRelatedField()
	deleted_by = serializers.StringRelatedField()
	restored_by = serializers.StringRelatedField()

	class Meta:
		model = FinAccount
		fields = ('id', 'title', 'slug', 'created_by', 'updated_by', 'deleted_by', 'restored_by', 'created_at', 'updated_at', 'deleted_at', 'restored_at')

class FinAccountsManageSerializer(serializers.ModelSerializer):
	title = serializers.CharField(max_length=100, allow_blank=True, required=False)
	orgid = serializers.PrimaryKeyRelatedField(queryset=Company.objects.all(), required=False, allow_null=True)
	created_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	updated_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	restored_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted = serializers.BooleanField(required=False)
	deleted_at 	= serializers.DateTimeField(required=False, allow_null=True)
	
	class Meta:
		model = FinAccount
		fields = ('__all__')

	def create(self, validated_data):
		package = FinAccount.objects.create(**validated_data)
		package.save()
		return package

	def update(self, instance, validated_data):
		instance.title = validated_data.get('title', instance.title)
		instance.updated_by = validated_data.get('updated_by', instance.updated_by)
		instance.deleted_by = validated_data.get('deleted_by', instance.deleted_by)
		instance.restored_by = validated_data.get('restored_by', instance.restored_by)
		instance.deleted = validated_data.get('deleted', instance.deleted)
		instance.deleted_at = validated_data.get('deleted_at', instance.deleted_at)

		instance.save()
		return instance

class BanksMinSerializer(serializers.ModelSerializer):

	class Meta:
		model = Bank
		fields = ('id', 'title')

class BanksSerializer(serializers.ModelSerializer):
	created_by = serializers.StringRelatedField()
	updated_by = serializers.StringRelatedField()
	deleted_by = serializers.StringRelatedField()
	restored_by = serializers.StringRelatedField()

	class Meta:
		model = Bank
		fields = ('id', 'title', 'slug', 'swift', 'mark', 'created_by', 'updated_by', 'deleted_by', 'restored_by', 'created_at', 'updated_at', 'deleted_at', 'restored_at')

class BanksManageSerializer(serializers.ModelSerializer):
	title = serializers.CharField(max_length=100, allow_blank=True, required=False)
	swift = serializers.CharField(max_length=20, allow_blank=True, required=False)
	mark = serializers.CharField(max_length=20, allow_blank=True, required=False)
	orgid = serializers.PrimaryKeyRelatedField(queryset=Company.objects.all(), required=False, allow_null=True)
	created_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	updated_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	restored_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted = serializers.BooleanField(required=False)
	deleted_at 	= serializers.DateTimeField(required=False, allow_null=True)
	
	class Meta:
		model = Bank
		fields = ('__all__')

	def create(self, validated_data):
		# fixme: zamockovat
		validated_data['created_by'] = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')

		package = Bank.objects.create(**validated_data)
		package.save()
		return package

	def update(self, instance, validated_data):
		instance.title = validated_data.get('title', instance.title)
		instance.swift = validated_data.get('swift', instance.swift)
		instance.mark = validated_data.get('mark', instance.mark)
		#instance.updated_by = validated_data.get('updated_by', instance.updated_by)
		# fixme: zamockovat
		instance.updated_by = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')
		instance.created_by = User.objects.get(id='26fff95a-c7bd-4449-b881-a3e7908ae5c3')

		instance.deleted_by = validated_data.get('deleted_by', instance.deleted_by)
		instance.restored_by = validated_data.get('restored_by', instance.restored_by)
		instance.deleted = validated_data.get('deleted', instance.deleted)
		instance.deleted_at = validated_data.get('deleted_at', instance.deleted_at)

		instance.save()
		return instance

class SalesListsMinSerializer(serializers.ModelSerializer):

	class Meta:
		model = SalesList
		fields = ('id', 'title')

class SalesListsSerializer(serializers.ModelSerializer):
	created_by = serializers.StringRelatedField()
	updated_by = serializers.StringRelatedField()
	deleted_by = serializers.StringRelatedField()
	restored_by = serializers.StringRelatedField()

	class Meta:
		model = SalesList
		fields = ('id', 'title', 'slug', 'created_by', 'updated_by', 'deleted_by', 'restored_by', 'created_at', 'updated_at', 'deleted_at', 'restored_at')

class SalesListsManageSerializer(serializers.ModelSerializer):
	title = serializers.CharField(max_length=100, allow_blank=True, required=False)
	orgid = serializers.PrimaryKeyRelatedField(queryset=Company.objects.all(), required=False, allow_null=True)
	created_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	updated_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	restored_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted = serializers.BooleanField(required=False)
	deleted_at 	= serializers.DateTimeField(required=False, allow_null=True)
	
	class Meta:
		model = SalesList
		fields = ('__all__')

	def create(self, validated_data):
		package = SalesList.objects.create(**validated_data)
		package.save()
		return package

	def update(self, instance, validated_data):
		instance.title = validated_data.get('title', instance.title)
		instance.updated_by = validated_data.get('updated_by', instance.updated_by)
		instance.deleted_by = validated_data.get('deleted_by', instance.deleted_by)
		instance.restored_by = validated_data.get('restored_by', instance.restored_by)
		instance.deleted = validated_data.get('deleted', instance.deleted)
		instance.deleted_at = validated_data.get('deleted_at', instance.deleted_at)

		instance.save()
		return instance

class DiscountTypesMinSerializer(serializers.ModelSerializer):

	class Meta:
		model = DiscountType
		fields = ('id', 'title')

class DiscountTypesSerializer(serializers.ModelSerializer):
	created_by = serializers.StringRelatedField()
	updated_by = serializers.StringRelatedField()
	deleted_by = serializers.StringRelatedField()
	restored_by = serializers.StringRelatedField()

	class Meta:
		model = DiscountType
		fields = ('id', 'title', 'slug', 'created_by', 'updated_by', 'deleted_by', 'restored_by', 'created_at', 'updated_at', 'deleted_at', 'restored_at')

class DiscountTypesManageSerializer(serializers.ModelSerializer):
	title = serializers.CharField(max_length=100, allow_blank=True, required=False)
	orgid = serializers.PrimaryKeyRelatedField(queryset=Company.objects.all(), required=False, allow_null=True)
	created_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	updated_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	restored_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted = serializers.BooleanField(required=False)
	deleted_at 	= serializers.DateTimeField(required=False, allow_null=True)
	
	class Meta:
		model = DiscountType
		fields = ('__all__')

	def create(self, validated_data):
		package = DiscountType.objects.create(**validated_data)
		package.save()
		return package

	def update(self, instance, validated_data):
		instance.title = validated_data.get('title', instance.title)
		instance.updated_by = validated_data.get('updated_by', instance.updated_by)
		instance.deleted_by = validated_data.get('deleted_by', instance.deleted_by)
		instance.restored_by = validated_data.get('restored_by', instance.restored_by)
		instance.deleted = validated_data.get('deleted', instance.deleted)
		instance.deleted_at = validated_data.get('deleted_at', instance.deleted_at)

		instance.save()
		return instance