from rest_framework import generics
from rest_framework.permissions import AllowAny

from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.authentication import BasicAuthentication

from config.permissions.ident import GetOrg

from modules.lists.api.serializer.finance import *
from modules.lists.api.view.inherit import ChosenUser


class VatTypesList(generics.ListCreateAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return VatTypesSerializer
        else:
            return VatTypesManageSerializer

    # Get the correct queryset
    def get_queryset(self):
        return VatType.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Post override
    def perform_create(self, serializer):
        serializer.save(
            created_by=self.get_request_user(),
            orgid=GetOrg(self.request)
        )


class VatTypesMinList(generics.ListAPIView):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        return VatTypesMinSerializer

    # Get the correct queryset
    def get_queryset(self):
        return VatType.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )


class VatTypesDetail(generics.RetrieveUpdateDestroyAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
    lookup_field = ('id')

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return VatTypesSerializer
        else:
            return VatTypesManageSerializer

    # Allow partial updates
    def get_serializer(self, *args, **kwargs):
        kwargs['partial'] = True
        return super(VatTypesDetail, self).get_serializer(*args, **kwargs)

    # Get the correct queryset
    def get_queryset(self):
        return VatType.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Put override
    def perform_update(self, serializer):
        serializer.save(updated_by=self.get_request_user())


class FinAccountsList(generics.ListCreateAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return FinAccountsSerializer
        else:
            return FinAccountsManageSerializer

    # Get the correct queryset
    def get_queryset(self):
        return FinAccount.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Post override
    def perform_create(self, serializer):
        serializer.save(
            created_by=self.get_request_user(),
            orgid=GetOrg(self.request)
        )


class FinAccountsMinList(generics.ListAPIView):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        return FinAccountsMinSerializer

    # Get the correct queryset
    def get_queryset(self):
        return FinAccount.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )


class FinAccountsDetail(generics.RetrieveUpdateDestroyAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
    lookup_field = ('id')

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return FinAccountsSerializer
        else:
            return FinAccountsManageSerializer

    # Allow partial updates
    def get_serializer(self, *args, **kwargs):
        kwargs['partial'] = True
        return super(FinAccountsDetail, self).get_serializer(*args, **kwargs)

    # Get the correct queryset
    def get_queryset(self):
        return FinAccount.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Put override
    def perform_update(self, serializer):
        serializer.save(updated_by=self.get_request_user())


class BanksList(generics.ListCreateAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return BanksSerializer
        else:
            return BanksManageSerializer

    # Get the correct queryset
    def get_queryset(self):
        return Bank.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Post override
    def perform_create(self, serializer):
        serializer.save(
            created_by=self.get_request_user(),
            orgid=GetOrg(self.request)
        )


class BanksMinList(generics.ListAPIView):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        return BanksMinSerializer

    # Get the correct queryset
    def get_queryset(self):
        return Bank.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )


class BanksDetail(generics.RetrieveUpdateDestroyAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
    lookup_field = ('id')

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return BanksSerializer
        else:
            return BanksManageSerializer

    # Allow partial updates
    def get_serializer(self, *args, **kwargs):
        kwargs['partial'] = True
        return super(BanksDetail, self).get_serializer(*args, **kwargs)

    # Get the correct queryset
    def get_queryset(self):
        return Bank.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Put override
    def perform_update(self, serializer):
        serializer.save(updated_by=self.get_request_user())


class SalesListsList(generics.ListCreateAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return SalesListsSerializer
        else:
            return SalesListsManageSerializer

    # Get the correct queryset
    def get_queryset(self):
        return SalesList.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Post override
    def perform_create(self, serializer):
        serializer.save(
            created_by=self.get_request_user(),
            orgid=GetOrg(self.request)
        )


class SalesListsMinList(generics.ListAPIView):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        return SalesListsMinSerializer

    # Get the correct queryset
    def get_queryset(self):
        return SalesList.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )


class SalesListsDetail(generics.RetrieveUpdateDestroyAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
    lookup_field = ('id')

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return SalesListsSerializer
        else:
            return SalesListsManageSerializer

    # Allow partial updates
    def get_serializer(self, *args, **kwargs):
        kwargs['partial'] = True
        return super(SalesListsDetail, self).get_serializer(*args, **kwargs)

    # Get the correct queryset
    def get_queryset(self):
        return SalesList.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Put override
    def perform_update(self, serializer):
        serializer.save(updated_by=self.get_request_user())


class DiscountTypesList(generics.ListCreateAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return DiscountTypesSerializer
        else:
            return DiscountTypesManageSerializer

    # Get the correct queryset
    def get_queryset(self):
        return DiscountType.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Post override
    def perform_create(self, serializer):
        serializer.save(
            created_by=self.get_request_user(),
            orgid=GetOrg(self.request)
        )


class DiscountTypesMinList(generics.ListAPIView):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        return DiscountTypesMinSerializer

    # Get the correct queryset
    def get_queryset(self):
        return DiscountType.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )


class DiscountTypesDetail(generics.RetrieveUpdateDestroyAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
    lookup_field = ('id')

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return DiscountTypesSerializer
        else:
            return DiscountTypesManageSerializer

    # Allow partial updates
    def get_serializer(self, *args, **kwargs):
        kwargs['partial'] = True
        return super(DiscountTypesDetail, self).get_serializer(*args, **kwargs)

    # Get the correct queryset
    def get_queryset(self):
        return DiscountType.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Put override
    def perform_update(self, serializer):
        serializer.save(updated_by=self.get_request_user())
