from rest_framework import generics
from rest_framework.permissions import AllowAny

from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.authentication import BasicAuthentication

from config.permissions.ident import GetOrg

from modules.lists.api.serializer.directory import *
from modules.lists.api.view.inherit import ChosenUser


class PhoneTypesList(generics.ListCreateAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return PhoneTypesSerializer
        else:
            return PhoneTypesManageSerializer

    # Get the correct queryset
    def get_queryset(self):
        return PhoneType.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Post override
    def perform_create(self, serializer):
        print(GetOrg(self.request))
        serializer.save(
            created_by=self.get_request_user(),
            orgid=GetOrg(self.request)
        )


class PhoneTypesMinList(generics.ListAPIView):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        return PhoneTypesMinSerializer

    # Get the correct queryset
    def get_queryset(self):
        return PhoneType.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )


class PhoneTypesDetail(generics.RetrieveUpdateDestroyAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
    lookup_field = ('id')

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return PhoneTypesSerializer
        else:
            return PhoneTypesManageSerializer

    # Allow partial updates
    def get_serializer(self, *args, **kwargs):
        kwargs['partial'] = True
        return super(PhoneTypesDetail, self).get_serializer(*args, **kwargs)

    # Get the correct queryset
    def get_queryset(self):
        return PhoneType.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Put override
    def perform_update(self, serializer):
        serializer.save(updated_by=self.get_request_user())


class PrioritiesList(generics.ListCreateAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return PrioritiesSerializer
        else:
            return PrioritiesManageSerializer

    # Get the correct queryset
    def get_queryset(self):
        return Priority.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Post override
    def perform_create(self, serializer):
        serializer.save(
            created_by=self.get_request_user(),
            orgid=GetOrg(self.request)
        )


class PrioritiesMinList(generics.ListAPIView):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        return PrioritiesMinSerializer

    # Get the correct queryset
    def get_queryset(self):
        return Priority.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )


class PrioritiesDetail(generics.RetrieveUpdateDestroyAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
    lookup_field = ('id')

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return PrioritiesSerializer
        else:
            return PrioritiesManageSerializer

    # Allow partial updates
    def get_serializer(self, *args, **kwargs):
        kwargs['partial'] = True
        return super(PrioritiesDetail, self).get_serializer(*args, **kwargs)

    # Get the correct queryset
    def get_queryset(self):
        return Priority.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Put override
    def perform_update(self, serializer):
        serializer.save(updated_by=self.get_request_user())


class AccountStatusesList(generics.ListCreateAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return AccountStatusesSerializer
        else:
            return AccountStatusesManageSerializer

    # Get the correct queryset
    def get_queryset(self):
        return AccountStatus.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Post override
    def perform_create(self, serializer):
        serializer.save(
            created_by=self.get_request_user(),
            orgid=GetOrg(self.request)
        )


class AccountStatusesMinList(generics.ListAPIView):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        return AccountStatusesMinSerializer

    # Get the correct queryset
    def get_queryset(self):
        return AccountStatus.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )


class AccountStatusesDetail(generics.RetrieveUpdateDestroyAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
    lookup_field = ('id')

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return AccountStatusesSerializer
        else:
            return AccountStatusesManageSerializer

    # Allow partial updates
    def get_serializer(self, *args, **kwargs):
        kwargs['partial'] = True
        return super(AccountStatusesDetail, self).get_serializer(*args, **kwargs)

    # Get the correct queryset
    def get_queryset(self):
        return AccountStatus.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Put override
    def perform_update(self, serializer):
        serializer.save(updated_by=self.get_request_user())


class TerritoriesList(generics.ListCreateAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return TerritoriesSerializer
        else:
            return TerritoriesManageSerializer

    # Get the correct queryset
    def get_queryset(self):
        return Territory.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Post override
    def perform_create(self, serializer):
        serializer.save(
            created_by=self.get_request_user(),
            orgid=GetOrg(self.request)
        )


class TerritoriesMinList(generics.ListAPIView):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        return TerritoriesMinSerializer

    # Get the correct queryset
    def get_queryset(self):
        return Territory.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )


class TerritoriesDetail(generics.RetrieveUpdateDestroyAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
    lookup_field = ('id')

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return TerritoriesSerializer
        else:
            return TerritoriesManageSerializer

    # Allow partial updates
    def get_serializer(self, *args, **kwargs):
        kwargs['partial'] = True
        return super(TerritoriesDetail, self).get_serializer(*args, **kwargs)

    # Get the correct queryset
    def get_queryset(self):
        return Territory.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Put override
    def perform_update(self, serializer):
        serializer.save(updated_by=self.get_request_user())
