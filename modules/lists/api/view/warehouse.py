from rest_framework import generics
from rest_framework.permissions import AllowAny

from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.authentication import BasicAuthentication

from config.permissions.ident import GetOrg

from modules.lists.api.serializer.warehouse import *
from modules.lists.api.view.inherit import ChosenUser


class MeasureUnitsList(generics.ListCreateAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return MeasureUnitsSerializer
        else:
            return MeasureUnitsManageSerializer

    # Get the correct queryset
    def get_queryset(self):
        return MeasureUnit.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Post override
    def perform_create(self, serializer):
        print(GetOrg(self.request))
        serializer.save(
            created_by=self.get_request_user(),
            orgid=GetOrg(self.request)
        )


class MeasureUnitsMinList(generics.ListAPIView):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        return MeasureUnitsMinSerializer

    # Get the correct queryset
    def get_queryset(self):
        return MeasureUnit.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )


class MeasureUnitsDetail(generics.RetrieveUpdateDestroyAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
    lookup_field = ('id')

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return MeasureUnitsSerializer
        else:
            return MeasureUnitsManageSerializer

    # Allow partial updates
    def get_serializer(self, *args, **kwargs):
        kwargs['partial'] = True
        return super(MeasureUnitsDetail, self).get_serializer(*args, **kwargs)

    # Get the correct queryset
    def get_queryset(self):
        return MeasureUnit.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Put override
    def perform_update(self, serializer):
        serializer.save(updated_by=self.get_request_user())


class OutgoTypesList(generics.ListCreateAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return OutgoTypesSerializer
        else:
            return OutgoTypesManageSerializer

    # Get the correct queryset
    def get_queryset(self):
        return OutgoType.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Post override
    def perform_create(self, serializer):
        serializer.save(
            created_by=self.get_request_user(),
            orgid=GetOrg(self.request)
        )


class OutgoTypesMinList(generics.ListAPIView):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        return OutgoTypesMinSerializer

    # Get the correct queryset
    def get_queryset(self):
        return OutgoType.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )


class OutgoTypesDetail(generics.RetrieveUpdateDestroyAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
    lookup_field = ('id')

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return OutgoTypesSerializer
        else:
            return OutgoTypesManageSerializer

    # Allow partial updates
    def get_serializer(self, *args, **kwargs):
        kwargs['partial'] = True
        return super(OutgoTypesDetail, self).get_serializer(*args, **kwargs)

    # Get the correct queryset
    def get_queryset(self):
        return OutgoType.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Put override
    def perform_update(self, serializer):
        serializer.save(updated_by=self.get_request_user())


class PayTypesList(generics.ListCreateAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return PayTypesSerializer
        else:
            return PayTypesManageSerializer

    # Get the correct queryset
    def get_queryset(self):
        return PayType.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Post override
    def perform_create(self, serializer):
        serializer.save(
            created_by=self.get_request_user(),
            orgid=GetOrg(self.request)
        )


class PayTypesMinList(generics.ListAPIView):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        return PayTypesMinSerializer

    # Get the correct queryset
    def get_queryset(self):
        return PayType.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )


class PayTypesDetail(generics.RetrieveUpdateDestroyAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
    lookup_field = ('id')

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return PayTypesSerializer
        else:
            return PayTypesManageSerializer

    # Allow partial updates
    def get_serializer(self, *args, **kwargs):
        kwargs['partial'] = True
        return super(PayTypesDetail, self).get_serializer(*args, **kwargs)

    # Get the correct queryset
    def get_queryset(self):
        return PayType.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Put override
    def perform_update(self, serializer):
        serializer.save(updated_by=self.get_request_user())


class MovementsList(generics.ListCreateAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return MovementsSerializer
        else:
            return MovementsManageSerializer

    # Get the correct queryset
    def get_queryset(self):
        return Movement.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Post override
    def perform_create(self, serializer):
        serializer.save(
            created_by=self.get_request_user(),
            orgid=GetOrg(self.request)
        )


class MovementsMinList(generics.ListAPIView):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        return MovementsMinSerializer

    # Get the correct queryset
    def get_queryset(self):
        return Movement.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )


class MovementsDetail(generics.RetrieveUpdateDestroyAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
    lookup_field = ('id')

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return MovementsSerializer
        else:
            return MovementsManageSerializer

    # Allow partial updates
    def get_serializer(self, *args, **kwargs):
        kwargs['partial'] = True
        return super(MovementsDetail, self).get_serializer(*args, **kwargs)

    # Get the correct queryset
    def get_queryset(self):
        return Movement.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Put override
    def perform_update(self, serializer):
        serializer.save(updated_by=self.get_request_user())


class OrderTypesList(generics.ListCreateAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return OrderTypesSerializer
        else:
            return OrderTypesManageSerializer

    # Get the correct queryset
    def get_queryset(self):
        return OrderType.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Post override
    def perform_create(self, serializer):
        serializer.save(
            created_by=self.get_request_user(),
            orgid=GetOrg(self.request)
        )


class OrderTypesMinList(generics.ListAPIView):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        return OrderTypesMinSerializer

    # Get the correct queryset
    def get_queryset(self):
        return OrderType.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )


class OrderTypesDetail(generics.RetrieveUpdateDestroyAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
    lookup_field = ('id')

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return OrderTypesSerializer
        else:
            return OrderTypesManageSerializer

    # Allow partial updates
    def get_serializer(self, *args, **kwargs):
        kwargs['partial'] = True
        return super(OrderTypesDetail, self).get_serializer(*args, **kwargs)

    # Get the correct queryset
    def get_queryset(self):
        return OrderType.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Put override
    def perform_update(self, serializer):
        serializer.save(updated_by=self.get_request_user())


class PackageTypesList(generics.ListCreateAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return PackageTypesSerializer
        else:
            return PackageTypesManageSerializer

    # Get the correct queryset
    def get_queryset(self):
        return PackageType.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Post override
    def perform_create(self, serializer):
        serializer.save(
            created_by=self.get_request_user(),
            orgid=GetOrg(self.request)
        )


class PackageTypesMinList(generics.ListAPIView):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        return PackageTypesMinSerializer

    # Get the correct queryset
    def get_queryset(self):
        return PackageType.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )


class PackageTypesDetail(generics.RetrieveUpdateDestroyAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
    lookup_field = ('id')

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return PackageTypesSerializer
        else:
            return PackageTypesManageSerializer

    # Allow partial updates
    def get_serializer(self, *args, **kwargs):
        kwargs['partial'] = True
        return super(PackageTypesDetail, self).get_serializer(*args, **kwargs)

    # Get the correct queryset
    def get_queryset(self):
        return PackageType.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Put override
    def perform_update(self, serializer):
        serializer.save(updated_by=self.get_request_user())


class EanTypesList(generics.ListCreateAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return EanTypesSerializer
        else:
            return EanTypesManageSerializer

    # Get the correct queryset
    def get_queryset(self):
        return EanType.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Post override
    def perform_create(self, serializer):
        serializer.save(
            created_by=self.get_request_user(),
            orgid=GetOrg(self.request)
        )


class EanTypesMinList(generics.ListAPIView):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        return EanTypesMinSerializer

    # Get the correct queryset
    def get_queryset(self):
        return EanType.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )


class EanTypesDetail(generics.RetrieveUpdateDestroyAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
    lookup_field = ('id')

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return EanTypesSerializer
        else:
            return EanTypesManageSerializer

    # Allow partial updates
    def get_serializer(self, *args, **kwargs):
        kwargs['partial'] = True
        return super(EanTypesDetail, self).get_serializer(*args, **kwargs)

    # Get the correct queryset
    def get_queryset(self):
        return EanType.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Put override
    def perform_update(self, serializer):
        serializer.save(updated_by=self.get_request_user())

#TODO mozno chybne umiestnenie
class PriceLevelsList(generics.ListCreateAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return PriceLevelsSerializer
        else:
            return PriceLevelsManageSerializer

    # Get the correct queryset
    def get_queryset(self):
        return PriceLevel.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Post override
    def perform_create(self, serializer):
        serializer.save(
            created_by=self.get_request_user(),
            orgid=GetOrg(self.request)
        )


class PriceLevelsMinList(generics.ListAPIView):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        return PriceLevelsMinSerializer

    # Get the correct queryset
    def get_queryset(self):
        return PriceLevel.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )


class PriceLevelsDetail(generics.RetrieveUpdateDestroyAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
    lookup_field = ('id')

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return PriceLevelsSerializer
        else:
            return PriceLevelsManageSerializer

    # Allow partial updates
    def get_serializer(self, *args, **kwargs):
        kwargs['partial'] = True
        return super(PriceLevelsDetail, self).get_serializer(*args, **kwargs)

    # Get the correct queryset
    def get_queryset(self):
        return PriceLevel.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Put override
    def perform_update(self, serializer):
        serializer.save(updated_by=self.get_request_user())


class ItemGroupsList(generics.ListCreateAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return ItemGroupsSerializer
        else:
            return ItemGroupsManageSerializer

    # Get the correct queryset
    def get_queryset(self):
        return ItemGroup.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Post override
    def perform_create(self, serializer):
        serializer.save(
            created_by=self.get_request_user(),
            orgid=GetOrg(self.request)
        )


class ItemGroupsMinList(generics.ListAPIView):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        return ItemGroupsMinSerializer

    # Get the correct queryset
    def get_queryset(self):
        return ItemGroup.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )


class ItemGroupsDetail(generics.RetrieveUpdateDestroyAPIView, ChosenUser):
    permission_classes = (AllowAny,)
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
    lookup_field = ('id')

    # Check the request type and select the corrent serializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return ItemGroupsSerializer
        else:
            return ItemGroupsManageSerializer

    # Allow partial updates
    def get_serializer(self, *args, **kwargs):
        kwargs['partial'] = True
        return super(ItemGroupsDetail, self).get_serializer(*args, **kwargs)

    # Get the correct queryset
    def get_queryset(self):
        return ItemGroup.objects.all()  # filter(

    # 	Q(orgid = GetOrgID(self.request))
    # )

    # Put override
    def perform_update(self, serializer):
        serializer.save(updated_by=self.get_request_user())
