import json

from django.db.models import Q
from django.http import HttpResponseServerError
from django.views import View
from django.views.decorators.csrf import csrf_exempt, ensure_csrf_cookie
from rest_framework import generics
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.authentication import BasicAuthentication

from config.permissions.ident import GetOrg, GetOrgID, GetUserID

from modules.lists.api.serializer.localization import *
from modules.lists.api.view.inherit import ChosenUser


class StatesList(generics.ListCreateAPIView, ChosenUser):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

	# Check the request type and select the corrent serializer
	def get_serializer_class(self):
		if self.request.method == 'GET':
			return StatesSerializer
		else:
			return StatesManageSerializer

	# Get the correct queryset
	def get_queryset(self):
		return State.objects.all().order_by('title')

	def create(self, request, *args, **kwargs):

		print("som v override create")
		#print(Currency.objects.get(pk=request.data['currency']))
		serializer = self.get_serializer(data=request.data)
		try:
			serializer.is_valid(raise_exception=True)
		except serializers.ValidationError as e:
			print(e)
		print("je to validne")
		self.perform_create(serializer)
		headers = self.get_success_headers(serializer.data)
		return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

	# Post override
	def perform_create(self, serializer):
		print("priprava pred ukladanim")
		serializer.save(
			created_by=self.get_request_user(),
			orgid=GetOrg(self.request)
		)


class StatesMinList(generics.ListAPIView):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

	# Check the request type and select the corrent serializer
	def get_serializer_class(self):
		return StatesMinSerializer

	# Get the correct queryset
	def get_queryset(self):
		return State.objects.all().order_by('title')


class StatesDetail(generics.RetrieveUpdateDestroyAPIView, ChosenUser):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
	lookup_field = ('id') #slug

	# Check the request type and select the corrent serializer
	def get_serializer_class(self):
		if self.request.method == 'GET':
			return StatesSerializer
		else:
			return StatesManageSerializer

	# Allow partial updates
	def get_serializer(self, *args, **kwargs):
		kwargs['partial'] = True
		return super(StatesDetail, self).get_serializer(*args, **kwargs)

	# Get the correct queryset
	def get_queryset(self):
		return State.objects.all().order_by('title')

	# Put override
	def perform_update(self, serializer):
		serializer.save(updated_by=self.get_request_user())


#FIXME: csrf token + permission
class StatesAdditing(APIView):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

	@staticmethod
	def _get_list_title(list):
		return [x.title for x in list]

	@csrf_exempt
	def post(self, request, *args, **kwargs):

		list = State.objects.filter(
			Q(orgid=GetOrgID(self.request))
		)

		list_r = []
		for da in request.data:
			list_r.append(State(id=da['id'],
								title=da['title'],
								created_by_id=GetUserID(self.request))
						  )

		for da in list_r:
			if not da.title in StatesAdditing._get_list_title(list):
				da.save()

		for li in list:
			if not li.title in StatesAdditing._get_list_title(list_r):
				cis = State.objects.get(pk=li.id)
				cis.delete()

		return Response(data=True, status=status.HTTP_201_CREATED)


class CitiesList(generics.ListCreateAPIView, ChosenUser):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

	# Check the request type and select the corrent serializer
	def get_serializer_class(self):
		if self.request.method == 'GET':
			return CitiesSerializer
		else:
			return CitiesManageSerializer

	# Get the correct queryset
	def get_queryset(self):
		return City.objects.all()  # filter(

	# 	Q(orgid = GetOrgID(self.request))
	# )

	# Post override
	def perform_create(self, serializer):
		serializer.save(
			created_by=self.get_request_user(),
			orgid=GetOrg(self.request)
		)


class CitiesMinList(generics.ListAPIView):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

	# Check the request type and select the corrent serializer
	def get_serializer_class(self):
		return CitiesMinSerializer

	# Get the correct queryset
	def get_queryset(self):
		return City.objects.all()  # filter(

	# 	Q(orgid = GetOrgID(self.request))
	# )


class CitiesDetail(generics.RetrieveUpdateDestroyAPIView, ChosenUser):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
	lookup_field = ('id')

	# Check the request type and select the corrent serializer
	def get_serializer_class(self):
		if self.request.method == 'GET':
			return CitiesSerializer
		else:
			return CitiesManageSerializer

	# Allow partial updates
	def get_serializer(self, *args, **kwargs):
		kwargs['partial'] = True
		return super(CitiesDetail, self).get_serializer(*args, **kwargs)

	# Get the correct queryset
	def get_queryset(self):
		return City.objects.all()  # filter(

	# 	Q(orgid = GetOrgID(self.request))
	# )

	# Put override
	def perform_update(self, serializer):
		serializer.save(updated_by=self.get_request_user())


class CountriesList(generics.ListCreateAPIView, ChosenUser):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

	# Check the request type and select the corrent serializer
	def get_serializer_class(self):
		if self.request.method == 'GET':
			return CountriesSerializer
		else:
			return CountriesManageSerializer

	# Get the correct queryset
	def get_queryset(self):
		return Country.objects.all()  # filter(

	# 	Q(orgid = GetOrgID(self.request))
	# )

	# Post override
	def perform_create(self, serializer):
		serializer.save(
			created_by=self.get_request_user(),
			orgid=GetOrg(self.request)
		)


class CountriesMinList(generics.ListAPIView):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

	# Check the request type and select the corrent serializer
	def get_serializer_class(self):
		return CountriesMinSerializer

	# Get the correct queryset
	def get_queryset(self):
		return Country.objects.all()  # filter(

	# 	Q(orgid = GetOrgID(self.request))
	# )


class CountriesDetail(generics.RetrieveUpdateDestroyAPIView, ChosenUser):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
	lookup_field = ('id')

	# Check the request type and select the corrent serializer
	def get_serializer_class(self):
		if self.request.method == 'GET':
			return CountriesSerializer
		else:
			return CountriesManageSerializer

	# Allow partial updates
	def get_serializer(self, *args, **kwargs):
		kwargs['partial'] = True
		return super(CountriesDetail, self).get_serializer(*args, **kwargs)

	# Get the correct queryset
	def get_queryset(self):
		return Country.objects.all()  # filter(

	# 	Q(orgid = GetOrgID(self.request))
	# )

	# Put override
	def perform_update(self, serializer):
		serializer.save(updated_by=self.get_request_user())


class LanguagesList(generics.ListCreateAPIView, ChosenUser):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

	# Check the request type and select the corrent serializer
	def get_serializer_class(self):
		if self.request.method == 'GET':
			return LanguagesSerializer
		else:
			return LanguagesManageSerializer

	# Get the correct queryset
	def get_queryset(self):
		return Language.objects.all()  # filter(

	# 	Q(orgid = GetOrgID(self.request))
	# )

	# Post override
	def perform_create(self, serializer):
		serializer.save(
			created_by=self.get_request_user(),
			orgid=GetOrg(self.request)
		)


class LanguagesMinList(generics.ListAPIView):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

	# Check the request type and select the corrent serializer
	def get_serializer_class(self):
		return LanguagesMinSerializer

	# Get the correct queryset
	def get_queryset(self):
		return Language.objects.all()  # filter(

	# 	Q(orgid = GetOrgID(self.request))
	# )


class LanguagesDetail(generics.RetrieveUpdateDestroyAPIView, ChosenUser):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
	lookup_field = ('id')

	# Check the request type and select the corrent serializer
	def get_serializer_class(self):
		if self.request.method == 'GET':
			return LanguagesSerializer
		else:
			return LanguagesManageSerializer

	# Allow partial updates
	def get_serializer(self, *args, **kwargs):
		kwargs['partial'] = True
		return super(LanguagesDetail, self).get_serializer(*args, **kwargs)

	# Get the correct queryset
	def get_queryset(self):
		return Language.objects.all()  # filter(

	# 	Q(orgid = GetOrgID(self.request))
	# )

	# Put override
	def perform_update(self, serializer):
		serializer.save(updated_by=self.get_request_user())

	def delete(self, request, *args, **kwargs):
		return self.destroy(request, *args, **kwargs)


class LanguagesDelete(generics.GenericAPIView):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

	def get_queryset(self):
		return Language.objects.filter(
			Q(orgid=GetOrgID(self.request))
		)

	def delete(self, request, *args, **kwargs):
		list = Language.objects.filter(
			Q(orgid=GetOrgID(self.request))
		)
		list.delete()
		return Response(status=status.HTTP_204_NO_CONTENT)


class LanguageAdditing(APIView):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

	@staticmethod
	def _get_list_title(list):
		return [x.title for x in list]

	@csrf_exempt
	def post(self, request, *args, **kwargs):

		list = Language.objects.filter(
			Q(orgid=GetOrgID(self.request))
		)

		list_r = []
		for da in request.data:
			list_r.append(Language(id=da['id'],
								   title=da['title'],
								   created_by_id=GetUserID(self.request))
						  )

		for da in list_r:
			if not da.title in LanguageAdditing._get_list_title(list):
				da.save()
		print(list)
		for li in list:
			if not li.title in LanguageAdditing._get_list_title(list_r):
				cis = Language.objects.get(pk=li.id)
				cis.delete()

		return Response(data=True, status=status.HTTP_201_CREATED)


class CurrenciesList(generics.ListCreateAPIView, ChosenUser):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

	# Check the request type and select the corrent serializer
	def get_serializer_class(self):
		if self.request.method == 'GET':
			return CurrenciesSerializer
		else:
			return CurrenciesManageSerializer

	# Get the correct queryset
	def get_queryset(self):
		return Currency.objects.all()  # filter(

	# 	Q(orgid = GetOrgID(self.request))
	# )

	# Post override
	def perform_create(self, serializer):
		serializer.save(
			created_by=self.get_request_user(),
			orgid=GetOrg(self.request)
		)


class CurrenciesMinList(generics.ListAPIView):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

	# Check the request type and select the corrent serializer
	def get_serializer_class(self):
		return CurrenciesMinSerializer

	# Get the correct queryset
	def get_queryset(self):
		return Currency.objects.all()  # filter(

	# 	Q(orgid = GetOrgID(self.request))
	# )


class CurrenciesDetail(generics.RetrieveUpdateDestroyAPIView, ChosenUser):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
	lookup_field = ('id')

	# Check the request type and select the corrent serializer
	def get_serializer_class(self):
		if self.request.method == 'GET':
			return CurrenciesSerializer
		else:
			return CurrenciesManageSerializer

	# Allow partial updates
	def get_serializer(self, *args, **kwargs):
		kwargs['partial'] = True
		return super(CurrenciesDetail, self).get_serializer(*args, **kwargs)

	# Get the correct queryset
	def get_queryset(self):
		return Currency.objects.all()  # filter(

	# 	Q(orgid = GetOrgID(self.request))
	# )

	# Put override
	def perform_update(self, serializer):
		serializer.save(updated_by=self.get_request_user())
