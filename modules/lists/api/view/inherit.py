from modules.lists.api.serializer.localization import User
from django.contrib.auth.models import AnonymousUser
from django.conf import settings

EMAIL = 'pokus@pokus.sk'


class ChosenUser:


    def get_request_user(self):
        """"""
        if settings.DISABLED_AUTHORIZATION:
            if isinstance(self.request.user, AnonymousUser):
                return User.objects.get(email=EMAIL)
        return self.request.user
