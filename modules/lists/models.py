from __future__ import unicode_literals
from django.db import models
from django.contrib.postgres.fields import JSONField
from config.orm.inherit import BaseTitleSlug, CRUDInfo, BaseTitleSlugOrgID
from django.db.models.signals import pre_save
from config.orm.signals import create_instance_slug


class MeasureUnit(BaseTitleSlugOrgID, CRUDInfo):
	abrv = models.CharField(max_length=10, null=False, blank=True, help_text="Unit's areviation")

	class Meta:
		verbose_name_plural = 'MeasureUnits'
		index_together = ['id'],['title']

	def __str__(self):
		return self.title


def pre_save_measureUnit_receiver(sender, instance, *args, **kwargs):
# create tenant data
	if not instance.slug:
		instance.slug = create_instance_slug(instance)

pre_save.connect(pre_save_measureUnit_receiver, sender=MeasureUnit)

class OutgoType(BaseTitleSlugOrgID, CRUDInfo):

	class Meta:
		verbose_name_plural = 'OutgoTypes'
		index_together = ['id'],['title']

	def __str__(self):
		return self.title


def pre_save_OutgoTypet_receiver(sender, instance, *args, **kwargs):
# create tenant data
	if not instance.slug:
		instance.slug = create_instance_slug(instance)

pre_save.connect(pre_save_OutgoTypet_receiver, sender=OutgoType)

class PayType(BaseTitleSlugOrgID, CRUDInfo):

	class Meta:
		verbose_name_plural = 'PayTypes'
		index_together = ['id'],['title']

	def __str__(self):
		return self.title

def pre_save_PayType_receiver(sender, instance, *args, **kwargs):
# create tenant data
	if not instance.slug:
		instance.slug = create_instance_slug(instance)

pre_save.connect(pre_save_PayType_receiver, sender=PayType)

class AlcRegType(BaseTitleSlugOrgID, CRUDInfo):

	class Meta:
		verbose_name_plural = 'AlcRegTypes'
		index_together = ['id'],['title']

	def __str__(self):
		return self.title

def pre_save_AlcRegType_receiver(sender, instance, *args, **kwargs):
# create tenant data
	if not instance.slug:
		instance.slug = create_instance_slug(instance)

pre_save.connect(pre_save_AlcRegType_receiver, sender=AlcRegType)


class State(BaseTitleSlugOrgID, CRUDInfo):
	title = models.CharField(max_length=100, null=False, blank=True, help_text="State's title")
	abrv = models.CharField(max_length=5, null=False, blank=True, help_text="State's abreviation")
	currency = models.ForeignKey('shared.Currency', related_name='state_currency', null=True, help_text="Currency Id")

	class Meta:
		verbose_name_plural = 'States'
		index_together = ['id'], ['title']

	def __str__(self):
		return self.title


def pre_save_State_receiver(sender, instance, *args, **kwargs):
# create tenant data
	if not instance.slug:
		instance.slug = create_instance_slug(instance)

pre_save.connect(pre_save_State_receiver, sender=State)

class Country(BaseTitleSlugOrgID, CRUDInfo):
	code = models.CharField(max_length=25, null=False, blank=True)
	iso = models.CharField(max_length=5, null=False, blank=True)
	iso3 = models.CharField(max_length=5, null=False, blank=True)
	phonecode = models.IntegerField(null=True)

	class Meta:
		verbose_name_plural = 'Countries'
		index_together = ['id'],['title']

	def __str__(self):
		return self.title


def pre_save_Country_receiver(sender, instance, *args, **kwargs):
# create tenant data
	if not instance.slug:
		instance.slug = create_instance_slug(instance)

pre_save.connect(pre_save_Country_receiver, sender=Country)


class PhoneType(BaseTitleSlugOrgID, CRUDInfo):
	title = models.CharField(max_length=100, null=False, blank=True, help_text="PhoneType's title")

	class Meta:
		verbose_name_plural = 'PhoneTypes'
		index_together = ['id'],['title']

	def __str__(self):
			return self.title


def pre_save_PhoneType_receiver(sender, instance, *args, **kwargs):
# create tenant data
	if not instance.slug:
		instance.slug = create_instance_slug(instance)

pre_save.connect(pre_save_PhoneType_receiver, sender=PhoneType)


class Language(BaseTitleSlugOrgID, CRUDInfo):

	class Meta:
		verbose_name_plural = 'Languages'
		index_together = ['id'],['title']

	def __str__(self):
		return self.title


def pre_save_Language_receiver(sender, instance, *args, **kwargs):
# create tenant data
	if not instance.slug:
		instance.slug = create_instance_slug(instance)

pre_save.connect(pre_save_Language_receiver, sender=Language)


class Currency(BaseTitleSlugOrgID, CRUDInfo):
	abrv = models.CharField(max_length=5, null=False, blank=True)
	sign = models.CharField(max_length=10, null=False, blank=True)
	market = models.DecimalField(decimal_places=2, max_digits=5, null=True, blank=True)

	class Meta:
		verbose_name_plural = 'Currencies'

	def __str__(self):
		return self.title


def pre_save_Currency_receiver(sender, instance, *args, **kwargs):
# create tenant data
	if not instance.slug:
		instance.slug = create_instance_slug(instance)

pre_save.connect(pre_save_Currency_receiver, sender=Currency)


class Priority(BaseTitleSlugOrgID, CRUDInfo):

	class Meta:
		verbose_name_plural = 'Priorities'

	def __str__(self):
		return self.title


def pre_save_Priority_receiver(sender, instance, *args, **kwargs):
# create tenant data
	if not instance.slug:
		instance.slug = create_instance_slug(instance)

pre_save.connect(pre_save_Priority_receiver, sender=Priority)


class AccountStatus(BaseTitleSlugOrgID, CRUDInfo):

	class Meta:
		verbose_name_plural = 'AccountStatuses'

	def __str__(self):
		return self.title


def pre_save_AccountStatus_receiver(sender, instance, *args, **kwargs):
# create tenant data
	if not instance.slug:
		instance.slug = create_instance_slug(instance)

pre_save.connect(pre_save_AccountStatus_receiver, sender=AccountStatus)


class City(BaseTitleSlugOrgID, CRUDInfo):
	okres = models.CharField(max_length=150, null=False, blank=True)
	kraj = models.CharField(max_length=10, null=False, blank=True)
	psc = JSONField(null=True)

	class Meta:
		verbose_name_plural = 'Cities'

	def __str__(self):
		return self.title


def pre_save_City_receiver(sender, instance, *args, **kwargs):
# create tenant data
	if not instance.slug:
		instance.slug = create_instance_slug(instance)

pre_save.connect(pre_save_City_receiver, sender=City)


class Territory(BaseTitleSlugOrgID, CRUDInfo):

	class Meta:
		verbose_name_plural = 'Territories'

	def __str__(self):
		return self.title


def pre_save_Territory_receiver(sender, instance, *args, **kwargs):
# create tenant data
	if not instance.slug:
		instance.slug = create_instance_slug(instance)

pre_save.connect(pre_save_Territory_receiver, sender=Territory)

class Movement(BaseTitleSlugOrgID, CRUDInfo):

	class Meta:
		verbose_name_plural = 'Movements'

	def __str__(self):
		return self.title


def pre_save_Movement_receiver(sender, instance, *args, **kwargs):
# create tenant data
	if not instance.slug:
		instance.slug = create_instance_slug(instance)

pre_save.connect(pre_save_Movement_receiver, sender=Movement)


class OrderType(BaseTitleSlugOrgID, CRUDInfo):

	class Meta:
		verbose_name_plural = 'OrderTypes'

	def __str__(self):
		return self.title


def pre_save_OrderType_receiver(sender, instance, *args, **kwargs):
# create tenant data
	if not instance.slug:
		instance.slug = create_instance_slug(instance)

pre_save.connect(pre_save_OrderType_receiver, sender=OrderType)

class PackageType(BaseTitleSlugOrgID, CRUDInfo):

	class Meta:
		verbose_name_plural = 'PackageTypes'

	def __str__(self):
		return self.title


def pre_save_PackageType_receiver(sender, instance, *args, **kwargs):
# create tenant data
	if not instance.slug:
		instance.slug = create_instance_slug(instance)

pre_save.connect(pre_save_PackageType_receiver, sender=PackageType)


class VatType(BaseTitleSlugOrgID, CRUDInfo):

	class Meta:
		verbose_name_plural = 'VatTypes'

	def __str__(self):
		return self.title


def pre_save_VatType_receiver(sender, instance, *args, **kwargs):
# create tenant data
	if not instance.slug:
		instance.slug = create_instance_slug(instance)

pre_save.connect(pre_save_VatType_receiver, sender=VatType)

class FinAccount(BaseTitleSlugOrgID, CRUDInfo):

	class Meta:
		verbose_name_plural = 'FinAccounts'

	def __str__(self):
		return self.title


def pre_save_FinAccount_receiver(sender, instance, *args, **kwargs):
# create tenant data
	if not instance.slug:
		instance.slug = create_instance_slug(instance)

pre_save.connect(pre_save_FinAccount_receiver, sender=FinAccount)


class Bank(BaseTitleSlugOrgID, CRUDInfo):
	code = models.CharField(max_length=10, null=False, blank=True, help_text="Bank's code")
	swift = models.CharField(max_length=50, null=False, blank=True, help_text="Bank's swift code")
	mark = models.CharField(max_length=50, null=False, blank=True, help_text="Bank's mark")

	class Meta:
		verbose_name_plural = 'Banks'

	def __str__(self):
		return self.title


def pre_save_Bank_receiver(sender, instance, *args, **kwargs):
# create tenant data
	if not instance.slug:
		instance.slug = create_instance_slug(instance)

pre_save.connect(pre_save_Bank_receiver, sender=Bank)


class SalesList(BaseTitleSlugOrgID, CRUDInfo):

	class Meta:
		verbose_name_plural = 'SalesLists'

	def __str__(self):
		return self.title


def pre_save_SalesList_receiver(sender, instance, *args, **kwargs):
# create tenant data
	if not instance.slug:
		instance.slug = create_instance_slug(instance)

pre_save.connect(pre_save_SalesList_receiver, sender=SalesList)


class DiscountType(BaseTitleSlugOrgID, CRUDInfo):

	class Meta:
		verbose_name_plural = 'DiscountTypes'

	def __str__(self):
		return self.title


def pre_save_DiscountType_receiver(sender, instance, *args, **kwargs):
# create tenant data
	if not instance.slug:
		instance.slug = create_instance_slug(instance)

pre_save.connect(pre_save_DiscountType_receiver, sender=DiscountType)


class PriceLevel(BaseTitleSlugOrgID, CRUDInfo):

	class Meta:
		verbose_name_plural = 'PriceLevels'

	def __str__(self):
		return self.title


def pre_save_PriceLevel_receiver(sender, instance, *args, **kwargs):
# create tenant data
	if not instance.slug:
		instance.slug = create_instance_slug(instance)

pre_save.connect(pre_save_PriceLevel_receiver, sender=PriceLevel)


class EanType(BaseTitleSlugOrgID, CRUDInfo):

	class Meta:
		verbose_name_plural = 'EanTypes'

	def __str__(self):
		return self.title


def pre_save_EanType_receiver(sender, instance, *args, **kwargs):
# create tenant data
	if not instance.slug:
		instance.slug = create_instance_slug(instance)

pre_save.connect(pre_save_EanType_receiver, sender=EanType)


class ItemGroup(BaseTitleSlugOrgID, CRUDInfo):

	class Meta:
		verbose_name_plural = 'ItemGroups'

	def __str__(self):
		return self.title


def pre_save_ItemGroup_receiver(sender, instance, *args, **kwargs):
# create tenant data
	if not instance.slug:
		instance.slug = create_instance_slug(instance)

pre_save.connect(pre_save_ItemGroup_receiver, sender=ItemGroup)