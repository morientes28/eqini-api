# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-10-07 17:43
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shared', '0003_remove_state_currency'),
        ('lists', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='state',
            name='currency',
            field=models.ForeignKey(help_text='Currency Id', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='state_currency', to='shared.Currency'),
        ),
    ]
