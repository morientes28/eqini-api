from django.test import TestCase, RequestFactory
from django.test import Client

class TestClients(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.client = Client()
        self.get_string = '/api/shared/directory/'

    def test_clients(self):
        response = self.client.get(self.get_string + 'clients/')
        self.assertEqual(response.status_code, 200)
