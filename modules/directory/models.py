from __future__ import unicode_literals
import uuid
from django.db import models
from config.orm.inherit import BaseTitleSlug, CRUDInfo, BaseTitleSlugOrgID, BaseLocation, PostLocation, ContactInfo, SocialLinks, FinInfo, IdOnly, BaseOrgID
from config.orm.signals import create_instance_slug, create_instance_slug_from_name
from django.core.mail import send_mail
from django.db.models.signals import pre_save
from datetime import datetime

def avatar_directory_path(instance, filename):
	# file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
	return 'org_{0}/{1}'.format(instance.orgid, filename)

# File directory
def file_directory_path(instance, filename):
	# file will be uploaded to MEDIA_ROOT/org_<id>/<filename>
	return 'org_{0}/{1}'.format(instance.orgid, filename)



class Client(BaseTitleSlugOrgID, CRUDInfo, SocialLinks, FinInfo):
	# Base extension
	code = models.CharField(max_length=10, null=False, blank=True, help_text="Instance code")
	long_title = models.CharField(max_length=150, null=False, blank=True, help_text="Instance long title")
	description = models.TextField(max_length=500, null=False, blank=True, help_text="Instance description")
	avatar = models.ImageField(upload_to=avatar_directory_path, default='default/company_avatar.jpg', max_length=100, null=True, blank=True, help_text="Instance avatar")
	is_fo = models.BooleanField(null=False, default=False, help_text="Is the client non-company?")
	is_blocked = models.BooleanField(null=False, default=False, help_text="Is the client blocked?")

	is_supplier = models.BooleanField(null=False, default=False)
	is_buyer = models.BooleanField(null=False, default=False)
	is_producer = models.BooleanField(null=False, default=False)

	# Categories (Part of base info)
	rating = models.PositiveSmallIntegerField(blank=True, null=True, default=0, help_text="Client rating")
	status = models.ForeignKey('lists.AccountStatus', related_name='client_status', null=True, help_text="Client status")
	source = models.ForeignKey('directory.Source', related_name='client_source', null=True, help_text="Client source")
	priority = models.ForeignKey('lists.Priority', related_name='client_priority', null=True, help_text="Client priority")
	business_territory = models.ForeignKey('lists.Territory', related_name='client_territory', null=True, help_text="Business territory")

	# Base Location
	base_street = models.CharField(max_length=200, null=False, blank=True, help_text="Base street with number")
	base_city = models.CharField(max_length=150, null=False, blank=True, help_text="Base city")
	base_zip = models.CharField(max_length=20, null=False, blank=True, help_text="Base zip code")
	base_state = models.ForeignKey('lists.State', related_name='%(app_label)s_%(class)s_state', null=True, help_text="Base state")
	base_country = models.ForeignKey('lists.Country', related_name='%(app_label)s_%(class)s_country', null=True, help_text="Base country")
	base_gps_coordinates = models.CharField(max_length=100, null=False, blank=True, help_text="Location gps coordinates")

	# Post Location
	post_street = models.CharField(max_length=200, null=False, blank=True, help_text="Post street with number")
	post_city = models.CharField(max_length=150, null=False, blank=True, help_text="Post city")
	post_zip = models.CharField(max_length=20, null=False, blank=True, help_text="Post zip code")
	post_state = models.ForeignKey('lists.State', related_name='%(class)s_state', null=True, help_text="Post state")
	post_country = models.ForeignKey('lists.Country', related_name='%(class)s_country', null=True, help_text="Post country")

	# Pricing & Trading Info
	price_level = models.ForeignKey('lists.PriceLevel', related_name='client_price_level', null=True, help_text="Price level of the subject")
	discount = models.DecimalField(blank=True, null=True, default=0, max_digits=20, decimal_places=4, help_text="Subject discount")
	discount_type = models.ForeignKey('lists.DiscountType', related_name='client_discount_type', null=True, help_text="Price discount type")
	discount_prices = models.BooleanField(null=False, default=False, help_text="Are discount prices enabled?")
	outgo_type = models.ForeignKey('lists.OutgoType', related_name='client_outgo_type', null=True, help_text="Subject's outgo type")
	pay_type = models.ForeignKey('lists.PayType', related_name='client_pay_type', null=True, help_text="Price discount type")
	bank_account = models.CharField(max_length=50, null=False, blank=True, help_text="Bank account (IBAN)")
	bank_swift = models.CharField(max_length=100, null=False, blank=True, help_text="Bank code (SWIFT)")
	payment_due = models.PositiveSmallIntegerField(blank=True, null=True, help_text="Days for payment")
	credit = models.DecimalField(blank=True, null=True, max_digits=20, decimal_places=4, help_text="Credit amount")
	amount_to_block = models.DecimalField(blank=True, null=True, max_digits=20, decimal_places=4, help_text="How much debt will auto block the account")
	days_to_block = models.IntegerField(blank=True, null=True, help_text="How many days after payment due will auto block the account")
	enable_blocking = models.BooleanField(null=False, default=False, help_text="Enable auto-blocking?")
	transport_coefficient = models.DecimalField(blank=True, null=True, max_digits=20, decimal_places=4, help_text="Transport Coeficient")

	# Special Info
	id_edi = models.CharField(max_length=100, null=False, blank=True, help_text="EDI ID")
	ean_edi = models.CharField(max_length=100, null=False, blank=True, help_text="EDI EAN")
	ean_delivery = models.CharField(max_length=100, null=False, blank=True, help_text="Delivery EAN")
	ean_invoice = models.CharField(max_length=100, null=False, blank=True, help_text="Invoice EAN")
	alc_reg_number = models.CharField(max_length=100, null=False, blank=True, help_text="Alcohol registration number")
	alc_reg_start = models.DateTimeField(auto_now=False, auto_now_add=False, null=True, help_text="Start of the alcohol registration")
	alc_reg_end = models.DateTimeField(auto_now=False, auto_now_add=False, null=True, help_text="End of the alcohol registration")
	alc_reg_type = models.ForeignKey('lists.AlcRegType', related_name='client_alc_reg_type', null=True, help_text="Alcohol registration type")

	# Other Info
	anniversary = models.DateTimeField(auto_now=False, auto_now_add=False, null=True, help_text="Anniversary date")
	turnover = models.ForeignKey('shared.Turnover', related_name='client_turnover', null=True, help_text="Turnover of the client")
	number_of_employee = models.ForeignKey('shared.EmployeeCount', related_name='client_num_emp', null=True, help_text="Number of employee")

	# Contact Info
	phone_1 = models.CharField(max_length=30, null=False, blank=True, help_text="Phone number 1")
	phone_1_type = models.ForeignKey('lists.PhoneType', related_name='%(app_label)s_%(class)s_phone_type_1', null=True, help_text="Phone number type 1")
	phone_2 = models.CharField(max_length=30, null=False, blank=True, help_text="Phone number 2")
	phone_2_type = models.ForeignKey('lists.PhoneType', related_name='%(app_label)s_%(class)s_phone_type_2', null=True, help_text="Phone number type 2")
	phone_3 = models.CharField(max_length=30, null=False, blank=True, help_text="Phone number 3")
	phone_3_type = models.ForeignKey('lists.PhoneType', related_name='%(app_label)s_%(class)s_phone_type_3', null=True, help_text="Phone number type 3")
	email_1 = models.EmailField(max_length=150, null=False, blank=True, help_text="Email address 1")
	email_2 = models.EmailField(max_length=150, null=False, blank=True, help_text="Email address 2")
	contact_person = models.ForeignKey('account.User', related_name='%(app_label)s_%(class)s_contact_person', null=True, help_text="Contact person")
	web = models.CharField(max_length=100, null=False, blank=True, help_text="Web address")

	class Meta:
		verbose_name_plural = 'Clients'
		index_together = ['id'],['orgid'],['title'],['slug'],['orgid', 'slug']

	def __str__(self):
		return self.title

def pre_save_client_receiver(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = create_instance_slug(instance)

pre_save.connect(pre_save_client_receiver, sender=Client)


class Contact(BaseOrgID, CRUDInfo, SocialLinks):
	# Base info
	slug = models.SlugField(max_length=125, unique=True, null=False, blank=True, help_text="Slug of the instance")
	name_prefix = models.CharField(max_length=25, null=False, blank=True, help_text="Name prefix")
	first_name = models.CharField(max_length=100, null=False, blank=True, help_text="First Name")
	last_name = models.CharField(max_length=100, null=False, blank=True, help_text="Last Name")
	name_suffix = models.CharField(max_length=25, null=False, blank=True, help_text="Name suffix")
	full_name = models.CharField(max_length=150, null=False, blank=False, help_text="Full name to display")
	is_primary = models.BooleanField(null=False, default=False, help_text="Is this contact primary one?")
	description = models.TextField(max_length=500, null=False, blank=True, help_text="Instance description")
	avatar = models.ImageField(upload_to=avatar_directory_path, default='default/avatar.jpg', max_length=100, null=True, blank=True, help_text="Instance avatar")
	client = models.ForeignKey('directory.Client', related_name='contact_client', null=True, help_text="Client of the contact (parent)")
	language = models.ForeignKey('lists.Language', related_name='contactlanguage_language', null=True, help_text="Client of the contact (parent)")

	# Categories (Part of base info)
	position = models.CharField(max_length=150, null=False, blank=True, help_text="Person's work position")

	# Other Info
	anniversary = models.DateTimeField(auto_now=False, auto_now_add=False, null=True, help_text="Anniversary date")
	family_status = models.ForeignKey('shared.FamilyStatus', related_name='contact_family_status', null=True, help_text="Person's family status")
	gender = models.ForeignKey('shared.Gender', related_name='contact_gender', null=True, help_text="Person's gender")

	# Contact Info
	phone_1 = models.CharField(max_length=30, null=False, blank=True, help_text="Phone number 1")
	phone_1_type = models.ForeignKey('lists.PhoneType', related_name='%(app_label)s_%(class)s_phone_type_1', null=True, help_text="Phone number type 1")
	phone_2 = models.CharField(max_length=30, null=False, blank=True, help_text="Phone number 2")
	phone_2_type = models.ForeignKey('lists.PhoneType', related_name='%(app_label)s_%(class)s_phone_type_2', null=True, help_text="Phone number type 2")
	phone_3 = models.CharField(max_length=30, null=False, blank=True, help_text="Phone number 3")
	phone_3_type = models.ForeignKey('lists.PhoneType', related_name='%(app_label)s_%(class)s_phone_type_3', null=True, help_text="Phone number type 3")
	email_1 = models.EmailField(max_length=150, null=False, blank=True, help_text="Email address 1")
	email_2 = models.EmailField(max_length=150, null=False, blank=True, help_text="Email address 2")
	contact_person = models.ForeignKey('account.User', related_name='%(app_label)s_%(class)s_contact_person', null=True, help_text="Contact person")
	web = models.CharField(max_length=100, null=False, blank=True, help_text="Web address")

	class Meta:
		verbose_name_plural = 'Clients'
		index_together = ['id'],['orgid'],['full_name'],['slug'],['client'],['orgid', 'slug'],['orgid','client', 'slug']

	def __str__(self):
		return self.full_name

def pre_save_contact_receiver(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = create_instance_slug_from_name(instance)

pre_save.connect(pre_save_contact_receiver, sender=Contact)

class Source(IdOnly):
	title = models.CharField(max_length=150, null=False, blank=True)

	class Meta:
		verbose_name_plural = 'Sources'

	def __str__(self):
		return self.title

class BusinessTerritory(IdOnly):
	title = models.CharField(max_length=150, null=False, blank=True)

	class Meta:
		verbose_name_plural = 'BusinessTerritories'

	def __str__(self):
		return self.title

class PriceLevel(IdOnly):
	title = models.CharField(max_length=150, null=False, blank=True)

	class Meta:
		verbose_name_plural = 'PriceLevels'

	def __str__(self):
		return self.title

class PriceDiscountType(IdOnly):
	title = models.CharField(max_length=150, null=False, blank=True)

	class Meta:
		verbose_name_plural = 'PriceDiscountTypes'

	def __str__(self):
		return self.title