from django.conf.urls import url, include
from modules.directory.api.view.client import *

urlpatterns = [
    url(r'^clients/$', ListClients.as_view(), name='client-all'),
    url(r'^clients/(?P<slug>[^/]+)/$', ClientDetail.as_view(), name='client-detail')
]