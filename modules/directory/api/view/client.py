from django.http import Http404
import datetime

from django.db.models import Q

from rest_framework import status
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from rest_framework.response import Response
from rest_framework.permissions import AllowAny

from rest_framework.renderers import JSONRenderer
from rest_framework import filters
from config.api.pagination import StandardResultsSet

from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.authentication import BasicAuthentication

from config.permissions.ident import GetOrgID, GetUserID, GetOrg

from modules.directory.models import Client
from modules.directory.api.serializer.client import *

class ListClients(generics.ListCreateAPIView):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
	pagination_class = StandardResultsSet
	filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend, filters.OrderingFilter)

	filter_fields = ('title', 'long_title', 'is_fo', 'is_blocked', 'rating', 'status', 'source', 'priority', 'business_territory', 'price_level', 'discount', 'discount_type', 'discount_prices', 'outgo_type', 'pay_type', 'bank_account', 'bank_swift', 'payment_due', 'credit', 'amount_to_block', 'days_to_block', 'enable_blocking', 'transport_coefficient', 'id_edi', 'ean_edi', 'ean_delivery', 'ean_invoice', 'alc_reg_number', 'alc_reg_start', 'alc_reg_end', 'alc_reg_type', 'anniversary', 'turnover', 'number_of_employee', 'base_street', 'base_city', 'base_zip', 'base_state', 'base_country', 'phone_1', 'email_1', 'ico', 'dic', 'icdph', 'is_dph', 'created_at', 'updated_at', 'deleted_at', 'restored_at', 'created_by', 'updated_by', 'deleted_by', 'restored_by')
	ordering_fields = ('title', 'long_title', 'is_fo', 'is_blocked', 'rating', 'status', 'source', 'priority', 'business_territory', 'price_level', 'discount', 'discount_type', 'discount_prices', 'outgo_type', 'pay_type', 'bank_account', 'bank_swift', 'payment_due', 'credit', 'amount_to_block', 'days_to_block', 'enable_blocking', 'transport_coefficient', 'id_edi', 'ean_edi', 'ean_delivery', 'ean_invoice', 'alc_reg_number', 'alc_reg_start', 'alc_reg_end', 'alc_reg_type', 'anniversary', 'turnover', 'number_of_employee', 'base_street', 'base_city', 'base_zip', 'base_state', 'base_country', 'phone_1', 'email_1', 'ico', 'dic', 'icdph', 'is_dph', 'created_at', 'updated_at', 'deleted_at', 'restored_at', 'created_by', 'updated_by', 'deleted_by', 'restored_by')
	search_fields = ('^title', '^long_title')
	ordering = ('-created_at')

	# Check the request type and select the corrent serializer
	def get_serializer_class(self):
		if self.request.method == 'GET':
			return ClientSerializer
		else:
			return ClientManageSerializer

	# Get the correct queryset
	def get_queryset(self):
		return Client.objects.filter(
			Q(orgid = GetOrgID(self.request)) & Q(deleted = False)
		)

	# Post override
	def perform_create(self, serializer):
		serializer.save(
			created_by = self.request.user,
			orgid = GetOrg(self.request)
		)

class ClientDetail(generics.RetrieveUpdateDestroyAPIView):
	permission_classes 		= (AllowAny,)
	authentication_classes 	= (BasicAuthentication, JSONWebTokenAuthentication)
	lookup_field 			= ('slug')

	# Check the request type and select the corrent serializer
	def get_serializer_class(self):
		if self.request.method == 'GET':
			return ClientDetailSerializer
		else:
			return ClientManageSerializer

	# Allow partial updates
	def get_serializer(self, *args, **kwargs):
		kwargs['partial'] = True
		return super(ClientDetail, self).get_serializer(*args, **kwargs)

	# Get the correct queryset
	def get_queryset(self):
		return Client.objects.filter(
			Q(orgid = GetOrgID(self.request)) & Q(deleted = False)
		)

	# Put override
	def perform_update(self, serializer):
		serializer.save(updated_by = self.request.user)

	# Delete override
	def destroy(self, request, *args, **kwargs):
		try:
			instance = self.get_object()
			serializer = self.get_serializer(instance, data=request.data)
			serializer.is_valid(raise_exception=True)
			serializer.save(deleted = True, deleted_by = self.request.user, deleted_at = datetime.datetime.now())
		except Http404:
			pass
		return Response(status=status.HTTP_204_NO_CONTENT)





