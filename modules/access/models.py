from __future__ import unicode_literals
import uuid
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.db import models
from tenant_schemas.models import TenantMixin
from config.orm.inherit import CRUDInfo, BaseTitleOrgID, IdOnly
from config.orm.signals import create_instance_slug
from django.conf import settings
from django.db.models.signals import pre_save
from django.core.mail import send_mail
from datetime import datetime
from modules.shared.models import Permission

#class AppSetting
#class NotificationSetting

## User Roles Model
class UserRole(BaseTitleOrgID, CRUDInfo):
	description = models.TextField(null=False, blank=True, help_text="User role descriprion")
	users = models.ManyToManyField('account.User', through="UsersInRole", related_name="users_in_role", help_text="Users in the role")

	class Meta:
		verbose_name_plural = 'UserRoles'
		get_latest_by = 'created_at'
		index_together = ['id'],['title']

	def __str__(self):
		return self.title

## User - Roles intermediate table
class UsersInRole(IdOnly):
	user = models.ForeignKey('account.User', related_name='usersinrole_user', null=True, help_text="User Id")
	role = models.ForeignKey('access.UserRole', related_name='usersinrole_role', null=True, help_text="Role Id")

	class Meta:
		verbose_name_plural = 'UsersInRoles'
		get_latest_by = 'created_at'
		index_together = ['id'],['user'],['role'],['role','user']

	def __str__(self):
		return self.id

## Permissions - Scheme intermediate table
class PermissionsInScheme(IdOnly, CRUDInfo):
	permission = models.ForeignKey('shared.Permission', related_name='permissionsinscheme_permssion', null=True, help_text="Permission Id")
	scheme = models.ForeignKey('access.PermissionScheme', related_name='permissionsinscheme_scheme', null=True, help_text="Scheme Id")

	class Meta:
		verbose_name_plural = 'PermissionsInSchemes'
		get_latest_by = 'created_at'
		index_together = ['id'],['permission'],['scheme'],['permission','scheme']

	def __str__(self):
		return self.id

## Permissions Schemes Model
class PermissionScheme(BaseTitleOrgID, CRUDInfo):
	description = models.TextField(null=False, blank=True, help_text="Pemission scheme descriprion")
	permissions = models.ManyToManyField('shared.Permission', through="PermissionsInScheme", related_name='permissions_in_scheme', help_text="Granted permissions")

	class Meta:
		verbose_name_plural = 'PermissionSchemes'
		get_latest_by = 'created_at'
		index_together = ['id'],['title']

	def __str__(self):
		return self.title
