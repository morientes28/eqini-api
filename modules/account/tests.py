from django.test import TestCase, RequestFactory
from django.test import Client
from rest_framework.test import APIRequestFactory

from modules.account.api.view.account import UserDetail, ListUsers
from modules.account.models import User, Company


class TestRegister(TestCase):
	def setUp(self):

		self.factory = RequestFactory()
		self.client = Client()
		self.get_string = '/api/account/register/'

	def test_company(self):
		response = self.client.get(self.get_string + 'company/')
		self.assertEqual(response.status_code, 200)

	def test_users(self):
		response = self.client.get(self.get_string + 'users/')
		self.assertEqual(response.status_code, 200)

		# TODO chceck/domain/X, check/email/X


class TestUsers(TestCase):

	user = None
	company = None
	factory = APIRequestFactory()

	def setUp(self):
		self.company = Company.objects.create(title="test", slug="test", domain="127.0.0.1", domain_url="127.0.0.1")
		self.company = self.company.save()

		self.user = User.objects.create()
		self.user.id = "ea67eb88-22a8-b34b-5ff7-d472f2b25cbc"
		self.user.email = "test@test.sk"
		self.user.full_name = "testovac"
		self.user.password = "xxx"
		self.user.company = self.company
		self.user.save()


		self.factory = RequestFactory()
		self.client = Client()
		self.get_string = '/api/account/users/'

	def test_users(self):
		request = self.factory.post(self.get_string)
		#response = self.client.post(self.get_string)
		view = ListUsers.as_view()
		response = view(request)
		self.assertEqual(response.status_code, 200)

	def test_users_email(self):
		request = self.factory.get(self.get_string)
		view = UserDetail.as_view()
		response = view(request, email=self.user.email)
		#response = self.client.get(self.get_string + self.user.email)
		self.assertEqual(response.status_code, 200)


class TestCompanies(TestCase):
	def setUp(self):
		self.factory = RequestFactory()
		self.client = Client()
		self.get_string = '/api/account/companies/'

	def test_companies(self):
		response = self.client.get(self.get_string)
		self.assertEqual(response.status_code, 200)

	# TODO companies/X

	def test_company_settings(self):
		response = self.client.get("/api/account/company/settings")
		self.assertEqual(response.status_code, 200)


class TestBranches(TestCase):
	def setUp(self):
		self.factory = RequestFactory()
		self.client = Client()
		self.get_string = '/api/account/branches/'

	def test_branches(self):
		response = self.client.get(self.get_string)
		self.assertEqual(response.status_code, 200)

		# TODO branches/X
