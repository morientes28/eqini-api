from __future__ import unicode_literals
import random, string
import uuid
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.db import models
from tenant_schemas.models import TenantMixin
from config.orm.inherit import BaseTitleSlug, CRUDInfo, BaseTitleSlugOrgID, BaseLocation, PostLocation, ContactInfo, SocialLinks, FinInfo, BaseOrgID
from config.orm.signals import create_instance_slug
from django.conf import settings
from django.db.models.signals import pre_save, post_save
from datetime import datetime


## User Manager
class UserManager(BaseUserManager):
	def create_user(self, email, password=None):
		if not email:
			raise ValueError('Users must have an email address')
		user = self.model(email=self.normalize_email(email))
		user.set_password(password)
		user.save(using=self._db)
		return user

	def create_superuser(self, email, password):
		user = self.create_user(email, password=password)
		user.is_admin = True
		user.save(using=self._db)
		return user

## User Model
class User(AbstractBaseUser, PermissionsMixin, CRUDInfo):
	id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
	email = models.EmailField(max_length=254, unique=True, blank=True, null=False)
	full_name = models.CharField(max_length=150, null=False, blank=True)
	is_staff = models.BooleanField(default=True)
	is_admin = models.BooleanField(default=False)
	is_active = models.BooleanField(default=True, null=False)
	activation_token = models.CharField(max_length=150, null=False, blank=True)
	company = models.IntegerField(default=0)
	avatar = models.ImageField()

	objects = UserManager()
	USERNAME_FIELD = 'email'

	class Meta:
		verbose_name = "user"
		verbose_name_plural = "users"
		get_latest_by = 'created_at'
		index_together = ['id'],['email']

	def get_full_name(self):
		return self.full_name

	def get_short_name(self):
		return self.full_name

	def has_perm(self, perm, obj=None):
		"Does the user have a specific permission?"
		return True

	def has_module_perms(self, users):
		"Does the user have permissions to view the users?"
		return True

	def __str__(self):
		return self.full_name


#def pre_save_user_receiver(sender, instance, *args, **kwargs):
#	instance.activation_token = ''.join(random.choice(string.lowercase) for i in range(25))

# def post_save_user_receiver(sender, instance, *args, **kwargs):

#pre_save.connect(pre_save_user_receiver, sender=User)	
# post_save.connect(post_save_user_receiver, sender=User)









## Company (tenant) Model
class Company(TenantMixin, CRUDInfo, BaseLocation, PostLocation, ContactInfo, SocialLinks, FinInfo):
	# Tenant Data
	title = models.CharField(max_length=120, unique=True, help_text="Company's title")
	slug = models.SlugField(max_length=125, unique=True, null=False, blank=False, help_text="Slug of the instance")
	domain_url = models.CharField(max_length=120, unique=True, help_text="Company's tenant url")
	schema_name = models.CharField(max_length=50, unique=True, help_text="Company's tenant name")

	# Extended Base Data
	long_title = models.CharField(max_length=150, null=False, blank=True, help_text="Long title")
	domain = models.CharField(max_length=120, unique=True, blank=False, help_text="Company's subdomain")
	users = models.ManyToManyField('User', related_name='company_users', help_text="Account users")

	# Extended Meta Data
	is_trial = models.BooleanField(default=True, null=False, help_text="Is on trial?")
	is_blocked = models.BooleanField(default=False, null=False, help_text="Is blocked?")

	class Meta:
		verbose_name_plural = 'companies'
		get_latest_by = 'created_at'
		index_together = ['id'],['title'],['domain']

	def __str__(self):
		return self.title

def pre_save_company_receiver(sender, instance, *args, **kwargs):
	# create tenant data
	if not instance.slug:
		instance.slug = create_instance_slug(instance)

	edited_name = instance.domain.strip('-')
	instance.schema_name = 'org_' + edited_name + '_' + str(datetime.now().strftime("%d%m%y"))
	instance.domain_url = instance.domain + settings.FRONT_END_DOMAIN

def post_save_company_receiver(sender, instance, created, *args, **kwargs):
	if created:
		settings = CompanySetting(orgid=instance)
		settings.save()
		
		# try:
		# 	CompanySetting.objects.get(orgid=instance.id)
		# except CompanySetting.DoesNotExist:
		# 	settings = CompanySetting(orgid=instance)
		# 	settings.save()

pre_save.connect(pre_save_company_receiver, sender=Company)
post_save.connect(post_save_company_receiver, sender=Company)























## Company Branch Model
class CompanyBranch(BaseTitleSlugOrgID, CRUDInfo, BaseLocation, PostLocation, ContactInfo):
	class Meta:
		verbose_name_plural = 'CompanyBranches'
		get_latest_by = 'created_at'
		index_together = ['id'],['orgid'],['slug'],['orgid','slug','id']
	def __str__(self):
		return self.title

def pre_save_branch_receiver(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = create_instance_slug(instance)

pre_save.connect(pre_save_branch_receiver, sender=CompanyBranch)







class CompanySetting(BaseOrgID):
	general_enable_vat = models.BooleanField(default=False, null=False)
	change_vat_on_acceptance = models.BooleanField(default=False, null=False)
	add_note_on_pl = models.BooleanField(default=False, null=False)
	print_note_on_pl = models.BooleanField(default=True, null=False)
	first_byuing_price_with_vat = models.BooleanField(default=False, null=False)
	enter_discount_byuing_price_acceptance = models.BooleanField(default=False, null=False)
	remember_last_discount_on_pl = models.BooleanField(default=True, null=False)
	priority_on_change_selling_price_acceptance_with_vat = models.BooleanField(default=False, null=False)
	give_discounted_selling_price_on_calculation = models.BooleanField(default=False, null=False)
	print_weight_on_pl = models.BooleanField(default=False, null=False)
	print_username_on_pl = models.BooleanField(default=True, null=False)
	print_date_consumence_on_pl = models.BooleanField(default=False, null=False)
	print_individual_number_of_supplier_on_pl = models.BooleanField(default=False, null=False)
	system_static_prices = models.BooleanField(default=False, null=False)

	counter_order_supplier = models.CharField(max_length=50, blank=False, default="SO<YYYY><NNNN>")
	counter_order_buyer = models.CharField(max_length=50, blank=False, default="BO<YYYY><NNNN>")
	counter_acceptance = models.CharField(max_length=50, blank=False, default="AC<YYYY><NNNN>")
	counter_outgo = models.CharField(max_length=50, blank=False, default="OU<YYYY><NNNN>")

	class Meta:
		verbose_name_plural = 'CompanySettings'
		index_together = ['id'],['orgid']

class UserSetting(BaseOrgID):
	user = models.ForeignKey('account.User', related_name='userSetting_user', null=True)

	class Meta:
		verbose_name_plural = 'UserSettings'
		index_together = ['id'],['orgid'],['user']