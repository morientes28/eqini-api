from django.conf.urls import url, include
from modules.account.api.view.account import *
from modules.account.api.view.settings import *

urlpatterns = [
	url(r'^register/company/$', RegisterCompanies.as_view(), name='register-company'),
	url(r'^register/users/$', RegisterAdminUser.as_view(), name='register-user'),
	url(r'^register/check/domain/(?P<domain>[^/]+)/$', RegisterCheck.as_view(), name='register-check'),
	url(r'^register/check/email/(?P<email>[^/]+)/$', EmailCheck.as_view(), name='email-check'),

    url(r'^users/$', ListUsers.as_view(), name='users-all'),
    url(r'^users/(?P<email>[^/]+)/$', UserDetail.as_view(), name='users-detail'),

    url(r'^companies/$', ListCompanies.as_view(), name='companies-all'),
    url(r'^companies/(?P<domain>[^/]+)/$', CompanyDetails.as_view(), name='companies-detail'),
    url(r'^company/settings/$', CompanySettings.as_view(), name='companysettings-detail'),

    url(r'^branches/$', ListBranches.as_view(), name='companybranch-all'),
    url(r'^branches/(?P<slug>[^/]+)/$', BranchDetails.as_view(), name='companybranch-detail'),
]