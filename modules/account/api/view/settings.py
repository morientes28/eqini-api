from django.http import Http404
import datetime

from django.db.models import Q

from rest_framework import status
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from django.http import Http404

from rest_framework.renderers import JSONRenderer

from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.authentication import BasicAuthentication

from config.permissions.ident import GetOrgID, GetUserID

from modules.account.models import Company, User, CompanyBranch, CompanySetting, UserSetting
from modules.account.api.serializer.settings import *

class CompanySettings(APIView):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

	def get_object(self, orgid):
		try:
			return CompanySetting.objects.get(orgid=orgid)
		except CompanySetting.DoesNotExist:
			Response(status=status.HTTP_404_NOT_FOUND)

	def get(self, request, format=None):
		orgid = GetOrgID(self.request)
		settings = self.get_object(orgid)
		serializer = SettingsSerializer(settings)
		return Response(serializer.data)

	def put(self, request, format=None):
		orgid = GetOrgID(self.request)
		settings = self.get_object(orgid)
		serializer = SettingsSerializer(settings, data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)