from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny

from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.authentication import BasicAuthentication

from modules.account.api.view.services import *


class ListUsers(generics.ListCreateAPIView):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

	def __init__(self, accountService=None, **kwargs):
		super(ListUsers, self).__init__(**kwargs)
		self.service = accountService if accountService else AccountService()

	# Check the request type and select the corrent serializer
	def get_serializer_class(self):
		return self.service.get_user_serializer(self.request.method)

	# Get the correct queryset
	def get_queryset(self):
		return self.service.get_user_by_id(self.request)

	def get_serializer_context(self):
		return self.service.get_company_context(self.request)

	# Post override
	def perform_create(self, serializer):
		self.service.create_user(serializer, self.request)


class RegisterAdminUser(generics.CreateAPIView):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

	def __init__(self, accountService=None, **kwargs):
		super(RegisterAdminUser, self).__init__(**kwargs)
		self.service = accountService if accountService else AccountService()

	# Check the request type and select the corrent serializer
	def get_serializer_class(self):
		return self.service.get_admin_serializer()

	def get_serializer_context(self):
		print(self.request)
		company = None
		#return {'company': GetOrgID(self.request), 'request': self.request}
		return {'company': company, 'request': self.request}

	# Post override
	def perform_create(self, serializer):
		serializer.save()


class UserDetail(generics.RetrieveUpdateDestroyAPIView):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
	lookup_field = ('email')

	def __init__(self, accountService=None, **kwargs):
		super(UserDetail, self).__init__(**kwargs)
		self.service = accountService if accountService else AccountService()

	# Check the request type and select the corrent serializer
	def get_serializer_class(self):
		return self.service.get_user_serializer(self.request.method)

	# Allow partial updates
	def get_serializer(self, *args, **kwargs):
		kwargs['partial'] = True
		return super(UserDetail, self).get_serializer(*args, **kwargs)

	# Get the correct queryset
	def get_queryset(self):
		return self.service.get_user_by_id(self.request)

	# Put override
	def perform_update(self, serializer):
		self.service.update_user(serializer, self.request)

	# Delete override
	def destroy(self, request, *args, **kwargs):
		return self.service.delete_user(UserDetail=self, request=request, *args, **kwargs)
		# TODO neviem ci to takto bude fungovat preto nechavam povodne (rado)
		# try:
		#     instance = self.get_object()
		#     serializer = self.get_serializer(instance, data=request.data)
		#     serializer.is_valid(raise_exception=True)
		#     serializer.save(deleted=True, deleted_by=self.request.user, deleted_at=datetime.datetime.now())
		# except Http404:
		#     pass
		# return Response(status=status.HTTP_204_NO_CONTENT)


class RegisterCheck(APIView):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
	lookup_field = ('domain')

	def __init__(self, accountService=None, **kwargs):
		super(RegisterCheck, self).__init__(**kwargs)
		self.service = accountService if accountService else AccountService()

	def get(self, request, domain, format=None):
		return self.service.register(request, domain, format)


class EmailCheck(APIView):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
	lookup_field = ('email')

	def __init__(self, accountService=None, **kwargs):
		super(EmailCheck, self).__init__(**kwargs)
		self.service = accountService if accountService else AccountService()

	def get(self, request, email, format=None):
		return self.service.email_check(request, email, format)

		# def get_serializer_class(self):
		# 	return CheckDomainSerializer

		# # Get the correct queryset
		# def get_queryset(self):
		# 	try:
		# 		return Company.objects.all()
		# 	except Company.DoesNotExist:
		# 		return Response(status=status.HTTP_204_NO_CONTENT)


class RegisterCompanies(generics.CreateAPIView):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

	def __init__(self, accountService=None, **kwargs):
		super(RegisterCompanies, self).__init__(**kwargs)
		self.service = accountService if accountService else AccountService()

	# Check the request type and select the corrent serializer
	def get_serializer_class(self):
		return self.service.get_register_serializer()

	# Get the correct queryset
	def get_queryset(self):
		return self.service.get_all_companies()


class ListCompanies(generics.ListCreateAPIView):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

	def __init__(self, accountService=None, **kwargs):
		super(ListCompanies, self).__init__(**kwargs)
		self.service = accountService if accountService else AccountService()

	# Check the request type and select the corrent serializer
	def get_serializer_class(self):
		return self.service.get_company_serializer(self.request.method)

	# Get the correct queryset
	def get_queryset(self):
		return self.service.get_company_by_userid(self.request)


class CompanyDetails(generics.RetrieveUpdateDestroyAPIView):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
	lookup_field = ('domain')

	def __init__(self, accountService=None, **kwargs):
		super(CompanyDetails, self).__init__(**kwargs)
		self.service = accountService if accountService else AccountService()

	# Check the request type and select the corrent serializer
	def get_serializer_class(self):
		return self.service.get_company_serializer(self.request.method)

	# Allow partial updates
	def get_serializer(self, *args, **kwargs):
		kwargs['partial'] = True
		return super(CompanyDetails, self).get_serializer(*args, **kwargs)

	# Get the correct queryset
	def get_queryset(self):
		return self.service.get_company_by_orgid(self.request)

	# Put override
	def perform_update(self, serializer):
		self.service.update_company(serializer, self.request)

	# Delete override
	def destroy(self, request, *args, **kwargs):
		return self.service.delete_company(CompanyDetail=self, request=request, *args, **kwargs)
		# TODO neviem ci to takto bude fungovat preto nechavam povodne (rado)
		# try:
		#     instance = self.get_object()
		#     serializer = self.get_serializer(instance, data=request.data)
		#     serializer.is_valid(raise_exception=True)
		#     serializer.save(deleted=True, deleted_by=self.request.user, deleted_at=datetime.datetime.now())
		# except Http404:
		#     pass
		# return Response(status=status.HTTP_204_NO_CONTENT)


class ListBranches(generics.ListCreateAPIView):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)

	def __init__(self, accountService=None, **kwargs):
		super(ListBranches, self).__init__(**kwargs)
		self.service = accountService if accountService else AccountService()

	# Check the request type and select the corrent serializer
	def get_serializer_class(self):
		return self.service.get_branch_serializer(self.request.method)

	# Get the correct queryset
	def get_queryset(self):
		return self.service.get_company_branch_by_id(self.request)

	# Post override
	def perform_create(self, serializer):
		self.service.create_branch(serializer, self.request)


class BranchDetails(generics.RetrieveUpdateDestroyAPIView):
	permission_classes = (AllowAny,)
	authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
	lookup_field = ('slug')

	def __init__(self, accountService=None, **kwargs):
		super(BranchDetails, self).__init__(**kwargs)
		self.service = accountService if accountService else AccountService()

	# Check the request type and select the corrent serializer
	def get_serializer_class(self):
		return self.service.get_branch_serializer(self.request.method)

	# Allow partial updates
	def get_serializer(self, *args, **kwargs):
		kwargs['partial'] = True
		return super(BranchDetails, self).get_serializer(*args, **kwargs)

	# Get the correct queryset
	def get_queryset(self):
		return self.service.get_company_branch_by_id(self.request)

	# Put override
	def perform_update(self, serializer):
		self.service.update_branch(serializer, self.request)

	# Delete override
	def destroy(self, request, *args, **kwargs):
		return self.service.delete_branch(BranchDetails=self, request=request, *args, **kwargs)
		# TODO neviem ci to takto bude fungovat preto nechavam povodne (rado)
		# try:
		#     instance = self.get_object()
		#     serializer = self.get_serializer(instance, data=request.data)
		#     serializer.is_valid(raise_exception=True)
		#     serializer.save(deleted=True, deleted_by=self.request.user, deleted_at=datetime.datetime.now())
		# except Http404:
		#     pass
		# return Response(status=status.HTTP_204_NO_CONTENT)
