from django.db.models import Q
from config.permissions.ident import GetOrgID, GetUserID
from modules.account.api.serializer.account import *
from rest_framework.response import Response
from rest_framework import status
import datetime
from django.http import Http404


class AccountService:

    def get_user_serializer(self, request_method):
        """Vrati serializer podla typu requestu pre GET UsersSerializer 
                pre ostatne UsersManageSerializer"""
        if request_method == 'GET':
            return UsersSerializer
        else:
            return UsersManageSerializer

    def get_admin_serializer(self):
        return UsersAdminManageSerializer

    def get_register_serializer(self):
        return RegisterCompanySerializer

    def get_company_serializer(self, request_method):
        """Vrati serializer podla typu requestu pre GET CompaniesSerializer
                pre ostatne CompaniesManageSerializer"""
        if request_method == 'GET':
            return CompaniesSerializer
        else:
            return CompaniesManageSerializer

    def get_branch_serializer(self, request_method):
        """Vrati serializer podla typu requestu pre GET BranchesSerializer
                pre ostatne BranchManageSerializer"""
        if request_method == 'GET':
            return BranchesSerializer
        else:
            return BranchManageSerializer

    def get_user_by_id(self, request):
        """Vrati usera podla ID"""
        return User.objects.filter(Q(company_users=GetOrgID(request)) & Q(deleted=False))

    def get_company_context(self, request):
        """Vrati dictionary s id firmy a s requestom"""
        return {'company': GetOrgID(request), 'request': request}

    def update_user(self, serializer, request):
        """Update usera..."""
        serializer.save(updated_by=request.user)

    def delete_user(self, UserDetail, request, *args, **kwargs):
        """Zmaze usera..."""
        try:
            instance = UserDetail.get_object()
            serializer = UserDetail.get_serializer(instance, data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save(deleted=True, deleted_by=UserDetail.request.user, deleted_at=datetime.datetime.now())
        except Http404:
            pass
        return Response(status=status.HTTP_204_NO_CONTENT)

    def create_user(self, serializer, request):
        """Vytvori usera..."""
        serializer.save(created_by=request.user)

    def register(self, request, domain, format):
        """Vytvori firmu..."""
        try:
            domain = Company.objects.get(domain=domain)
            serializer = CheckDomainSerializer
            return Response(status=status.HTTP_206_PARTIAL_CONTENT)
        except Company.DoesNotExist:
            return Response(status=status.HTTP_200_OK)

    def email_check(self, request, email, format):
        """Skontroluje ci sa email uz nenachadza v databaze ak ano vrati 206 ak nie 200"""
        try:
            email = User.objects.get(email=email)
            serializer = CheckEmailSerializer;
            return Response(status=status.HTTP_206_PARTIAL_CONTENT)
        except User.DoesNotExist:
            return Response(status=status.HTTP_200_OK)

    def get_all_companies(self):
        """Vrati vsetky firmy v """
        return Company.objects.all()

    def get_company_by_userid(self, request):
        """Najde firmu podla id usera"""
        return Company.objects.filter(users=GetUserID(request))

    def get_company_by_orgid(self, request):
        """Vrati firmu podla id firmy"""
        return Company.objects.filter(id=GetOrgID(request))

    def update_company(self, serializer, request):
        serializer.save(updated_by=request.user)

    def delete_company(self, CompanyDetail, request, *args, **kwargs):
        try:
            instance = CompanyDetail.get_object()
            serializer = CompanyDetail.get_serializer(instance, data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save(deleted=True, deleted_by=CompanyDetail.request.user, deleted_at=datetime.datetime.now())
        except Http404:
            pass
        return Response(status=status.HTTP_204_NO_CONTENT)

    def get_company_branch_by_id(self, request):
        """Vrati branch podla id firmy"""
        return CompanyBranch.objects.filter(Q(orgid=GetOrgID(request)) & Q(deleted=False))

    def create_branch(self, serializer, request):
        """Vytvori branch..."""
        org = Company.objects.get(id=GetOrgID(request))

        serializer.save(
            created_by=request.user,
            orgid=org
        )

    def update_branch(self, serializer, request):
        serializer.save(updated_by=request.user)

    def delete_branch(self, BranchDetails, request, *args, **kwargs):
        try:
            instance = BranchDetails.get_object()
            serializer = BranchDetails.get_serializer(instance, data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save(deleted=True, deleted_by=BranchDetails.request.user, deleted_at=datetime.datetime.now())
        except Http404:
            pass
        return Response(status=status.HTTP_204_NO_CONTENT)
