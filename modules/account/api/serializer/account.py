from django.db import transaction, connection

from rest_framework import serializers

from modules.shared.models import Language, Currency, PhoneType, State, Country
from modules.account.models import User, Company, CompanyBranch
from modules.shared.api.serializer.localization import CountriesSerializer
from modules.mailer.regMail import *


class UsersMinSerializer(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = ('email', 'full_name', 'is_active')


class CompaniesMinSerializer(serializers.ModelSerializer):
	class Meta:
		model = Company
		fields = ('title', 'domain_url', 'is_trial', 'is_blocked')


class BranchesMinSerializer(serializers.ModelSerializer):
	class Meta:
		model = CompanyBranch
		fields = ('title',)


class UsersSerializer(serializers.HyperlinkedModelSerializer):
	url = serializers.HyperlinkedIdentityField(view_name='users-detail', lookup_field='email', lookup_url_kwarg='email')
	created_by = UsersMinSerializer(many=False)
	updated_by = UsersMinSerializer(many=False)
	deleted_by = UsersMinSerializer(many=False)
	restored_by = UsersMinSerializer(many=False)

	class Meta:
		model = User
		fields = ('url', 'email', 'is_active', 'last_login', 'full_name', 'is_admin', 'created_by', 'updated_by', 'deleted_by', 'restored_by', 'created_at', 'updated_at', 'deleted_at', 'restored_at', 'deleted')


class UsersManageSerializer(serializers.ModelSerializer):
	email = serializers.EmailField(max_length=150, allow_blank=True, required=False)
	full_name = serializers.CharField(max_length=100, allow_blank=True, required=False)
	password = serializers.CharField(max_length=150, allow_blank=True, required=False, write_only=True)
	is_admin = serializers.BooleanField(required=False)
	is_active = serializers.BooleanField(required=False)
	created_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False)
	updated_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False)
	deleted_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False)
	restored_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False)
	deleted 	= serializers.BooleanField(required=False)
	deleted_at 	= serializers.DateTimeField(required=False)
	avatar = serializers.ImageField(required=False, max_length=None, allow_empty_file=False)

	class Meta:
		model = User
		fields = ('email', 'avatar', 'full_name', 'is_admin', 'is_active','password', 'created_by', 'updated_by', 'deleted_by', 'restored_by', 'created_at', 'updated_at', 'deleted_at', 'restored_at', 'deleted')

	def create(self, validated_data):
		user = User.objects.create(
			full_name = validated_data['full_name'],
			email = validated_data['email'],
			created_by = validated_data['created_by'],
			is_admin = validated_data['is_admin'],

			# fixme: zatial nekomplikujem s avatarom
			#avatar = validated_data['avatar']
		)
		user.set_password(validated_data['password'])
		user.save()

		# assign the user to a company
		if 'company' in self.context:
			try:
				org = Company.objects.get(id = self.context['company'])
			except: 
				msg = 'Company was not found.'
				raise serializers.ValidationError(msg)

			new_user = user.id
			org.users.add(new_user)

		return user

	def update(self, instance, validated_data):
		instance.full_name = validated_data.get('full_name', instance.full_name)
		# fixme: zatial nekomplikujem s avatarom
		#instance.avatar = validated_data.get('avatar', instance.avatar)
		instance.email = validated_data.get('email', instance.email)
		instance.updated_by = validated_data.get('updated_by', instance.updated_by)
		instance.deleted = validated_data.get('deleted', instance.deleted)
		instance.deleted_at = validated_data.get('deleted_at', instance.deleted_at)
		instance.is_admin = validated_data.get('is_admin', instance.is_admin)
		instance.is_active = validated_data.get('is_active', instance.is_active)

		if 'password' in validated_data:
			if validated_data['password'] != '':
				instance.set_password(validated_data['password'])

		instance.save()
		return instance


class UsersAdminManageSerializer(serializers.ModelSerializer):
	email = serializers.EmailField(max_length=150, allow_blank=True, required=False)
	full_name = serializers.CharField(max_length=100, allow_blank=True, required=False)
	password = serializers.CharField(max_length=150, allow_blank=True, required=False, write_only=True)
	is_admin = serializers.BooleanField(required=False)

	title = serializers.CharField(max_length=150, allow_blank=True, required=False)
	domain = serializers.CharField(max_length=150, allow_blank=True, required=False)

	class Meta:
		model = User
		fields = ('email', 'full_name', 'is_admin', 'password', 'title', 'domain')

	@transaction.atomic
	def create(self, validated_data):
		user = User.objects.create(
			full_name = validated_data['full_name'],
			email = validated_data['email'],
			is_admin = True
		)
		user.set_password(validated_data['password'])
		user.save()
		print('idem')
		# assign the user to a company
		if 'domain' in validated_data['domain']:
			try:
				org = Company.objects.get(domain = validated_data['domain'])
				print(org.domain)
			except:
				msg = 'Company was not found.'
				raise serializers.ValidationError(msg)

			new_user = user.id
			org.users.add(new_user)
			org.save()

		#try:
		#	if connection.get_schema() != 'public':
		#		connection.set_schema_to_public()
		#	org = Company.objects.create(
		#		title=validated_data['title'],
		#		domain=validated_data['domain'])
		#	new_user = user.id
		#	org.users.add(new_user)
		#	org.save()

			# Assign user to the company
			#org = Company.objects.get(id=validated_data['company'])


			# send reg mail	
			# emailAddress = user.email
			# fullName = user.full_name
			# companyTitle = org.title
			# companyDomain = org.domain_url

			# fixme: docasne som vypol posielani emailov
			#sendUserRegMail(emailAddress, fullName, companyTitle, companyDomain)
		#except Company.DoesNotExist:
		#	msg = 'Company was not found.'
		#	raise serializers.ValidationError(msg)
		return user


class CompaniesSerializer(serializers.HyperlinkedModelSerializer):
	url = serializers.HyperlinkedIdentityField(view_name='companies-detail', lookup_field='domain', lookup_url_kwarg='domain')
	users = UsersMinSerializer(many=True)
	created_by = UsersMinSerializer(many=False)
	updated_by = UsersMinSerializer(many=False)
	deleted_by = UsersMinSerializer(many=False)
	restored_by = UsersMinSerializer(many=False)
	phone_1_type = serializers.StringRelatedField(read_only=True, many=False)
	phone_2_type = serializers.StringRelatedField(read_only=True, many=False)
	phone_3_type = serializers.StringRelatedField(read_only=True, many=False)
	post_state = serializers.StringRelatedField(read_only=True, many=False)
	post_country = CountriesSerializer()
	base_state = serializers.StringRelatedField(read_only=True, many=False)
	base_country = CountriesSerializer()

	class Meta:
		model = Company
		fields = ('url', 'web', 'ico', 'dic', 'icdph', 'is_dph', 'domain_url', 'long_title', 'domain', 'users', 'is_trial', 'is_blocked', 'title', 'created_at', 'updated_at', 'deleted_at', 'restored_at', 'deleted', 'created_by', 'updated_by', 'deleted_by', 'restored_by', 'base_street', 'base_city', 'base_zip', 'base_state', 'base_country', 'post_street', 'post_city', 'post_zip', 'post_state', 'post_country', 'phone_1', 'phone_1_type', 'phone_2', 'phone_2_type', 'phone_3', 'phone_3_type', 'email_1', 'email_2')


class RegisterCompanySerializer(serializers.ModelSerializer):
	title = serializers.CharField(max_length=100, allow_blank=True, required=False)
	domain = serializers.CharField(max_length=100, allow_blank=True, required=False)

	class Meta:
		model = Company
		fields = ('id', 'title', 'domain')

	def create(self, validated_data):
		connection.set_schema_to_public()
		company = Company.objects.create(**validated_data)
		company.save()
		return company


class CompaniesManageSerializer(serializers.ModelSerializer):
	title = serializers.CharField(max_length=100, allow_blank=True, required=False)
	long_title = serializers.CharField(max_length=100, allow_blank=True, required=False)
	domain = serializers.CharField(max_length=100, allow_blank=True, required=False)
	is_trial = serializers.BooleanField(required=False)
	is_blocked = serializers.BooleanField(required=False)
	phone_1 = serializers.CharField(max_length=100, allow_blank=True, required=False)
	phone_1_type = serializers.PrimaryKeyRelatedField(queryset=PhoneType.objects.all(), required=False, allow_null=True)
	phone_2 = serializers.CharField(max_length=100, allow_blank=True, required=False)
	phone_2_type = serializers.PrimaryKeyRelatedField(queryset=PhoneType.objects.all(), required=False, allow_null=True)
	phone_3 = serializers.CharField(max_length=100, allow_blank=True, required=False)
	phone_3_type = serializers.PrimaryKeyRelatedField(queryset=PhoneType.objects.all(), required=False, allow_null=True)
	email_1 = serializers.EmailField(max_length=100, allow_blank=True, required=False)
	email_2 = serializers.EmailField(max_length=100, allow_blank=True, required=False)
	post_street = serializers.CharField(max_length=100, allow_blank=True, required=False)
	post_city = serializers.CharField(max_length=100, allow_blank=True, required=False)
	post_zip = serializers.CharField(max_length=100, allow_blank=True, required=False)
	post_state = serializers.PrimaryKeyRelatedField(queryset=State.objects.all(), required=False, allow_null=True)
	post_country = serializers.PrimaryKeyRelatedField(queryset=Country.objects.all(), required=False, allow_null=True)
	base_street = serializers.CharField(max_length=100, allow_blank=True, required=False)
	base_city = serializers.CharField(max_length=100, allow_blank=True, required=False)
	base_zip = serializers.CharField(max_length=100, allow_blank=True, required=False)
	base_state = serializers.PrimaryKeyRelatedField(queryset=State.objects.all(), required=False, allow_null=True)
	base_country = serializers.PrimaryKeyRelatedField(queryset=Country.objects.all(), required=False, allow_null=True)
	created_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	updated_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	restored_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted = serializers.BooleanField(required=False)
	deleted_at 	= serializers.DateTimeField(required=False, allow_null=True)
	ico = serializers.CharField(max_length=20, allow_blank=True, required=False)
	dic = serializers.CharField(max_length=20, allow_blank=True, required=False)
	icdph = serializers.CharField(max_length=20, allow_blank=True, required=False)
	web = serializers.CharField(max_length=100, allow_blank=True, required=False)
	is_dph = serializers.BooleanField(required=False)

	class Meta:
		model = Company
		fields = ('long_title', 'web', 'ico', 'dic', 'icdph', 'is_dph', 'domain', 'is_trial', 'is_blocked', 'title', 'created_at', 'updated_at', 'deleted_at', 'restored_at', 'deleted', 'created_by', 'updated_by', 'deleted_by', 'restored_by', 'base_street', 'base_city', 'base_zip', 'base_state', 'base_country', 'post_street', 'post_city', 'post_zip', 'post_state', 'post_country', 'phone_1', 'phone_1_type', 'phone_2', 'phone_2_type', 'phone_3', 'phone_3_type', 'email_1', 'email_2')

	def create(self, validated_data):
		company = Company.objects.create(**validated_data)
		company.save()
		return company

	def update(self, instance, validated_data):
		instance.title = validated_data.get('title', instance.title)
		instance.long_title = validated_data.get('long_title', instance.long_title)
		instance.domain = validated_data.get('domain', instance.domain)
		instance.is_trial = validated_data.get('is_trial', instance.is_trial)
		instance.is_blocked = validated_data.get('is_blocked', instance.is_blocked)
		instance.phone_1 = validated_data.get('phone_1', instance.phone_1)
		instance.phone_1_type = validated_data.get('phone_1_type', instance.phone_1_type)
		instance.phone_2 = validated_data.get('phone_2', instance.phone_2)
		instance.phone_2_type = validated_data.get('phone_2_type', instance.phone_2_type)
		instance.phone_3 = validated_data.get('phone_3', instance.phone_3)
		instance.phone_3_type = validated_data.get('phone_3_type', instance.phone_3_type)
		instance.email_1 = validated_data.get('email_1', instance.email_1)
		instance.email_2 = validated_data.get('email_2', instance.email_2)
		instance.post_street = validated_data.get('post_street', instance.post_street)
		instance.post_city = validated_data.get('post_city', instance.post_city)
		instance.post_zip = validated_data.get('post_zip', instance.post_zip)
		instance.post_state = validated_data.get('post_state', instance.post_state)
		instance.post_country = validated_data.get('post_country', instance.post_country)
		instance.base_street = validated_data.get('base_street', instance.base_street)
		instance.base_city = validated_data.get('base_city', instance.base_city)
		instance.base_zip = validated_data.get('base_zip', instance.base_zip)
		instance.base_state = validated_data.get('base_state', instance.base_state)
		instance.base_country = validated_data.get('base_country', instance.base_country)
		instance.updated_by = validated_data.get('updated_by', instance.updated_by)
		instance.deleted_by = validated_data.get('deleted_by', instance.deleted_by)
		instance.restored_by = validated_data.get('restored_by', instance.restored_by)
		instance.deleted = validated_data.get('deleted', instance.deleted)
		instance.deleted_at = validated_data.get('deleted_at', instance.deleted_at)
		instance.ico = validated_data.get('ico', instance.ico)
		instance.dic = validated_data.get('dic', instance.dic)
		instance.icdph = validated_data.get('icdph', instance.icdph)
		instance.is_dph = validated_data.get('is_dph', instance.is_dph)
		instance.web = validated_data.get('web', instance.web)

		instance.save()
		return instance


class CheckDomainSerializer(serializers.ModelSerializer):
	class Meta:
		model = Company
		fields = ('domain',)


class CheckEmailSerializer(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = ('email',)


class BranchesSerializer(serializers.HyperlinkedModelSerializer):
	url = serializers.HyperlinkedIdentityField(view_name='companybranch-detail', lookup_field='slug', lookup_url_kwarg='slug')
	created_by = UsersMinSerializer(many=False)
	updated_by = UsersMinSerializer(many=False)
	deleted_by = UsersMinSerializer(many=False)
	restored_by = UsersMinSerializer(many=False)
	phone_1_type = serializers.StringRelatedField(read_only=True, many=False)
	phone_2_type = serializers.StringRelatedField(read_only=True, many=False)
	phone_3_type = serializers.StringRelatedField(read_only=True, many=False)
	post_state = serializers.StringRelatedField(read_only=True, many=False)
	post_country = CountriesSerializer()
	base_state = serializers.StringRelatedField(read_only=True, many=False)
	base_country = CountriesSerializer()

	class Meta:
		model = CompanyBranch
		fields = ('url', 'web', 'slug', 'title', 'created_at', 'updated_at', 'deleted_at', 'restored_at', 'deleted', 'created_by', 'updated_by', 'deleted_by', 'restored_by', 'base_street', 'base_city', 'base_zip', 'base_state', 'base_country', 'post_street', 'post_city', 'post_zip', 'post_state', 'post_country', 'phone_1', 'phone_1_type', 'phone_2', 'phone_2_type', 'phone_3', 'phone_3_type', 'email_1', 'email_2')


class BranchManageSerializer(serializers.ModelSerializer):
	title = serializers.CharField(max_length=100, allow_blank=True, required=False)
	slug = serializers.CharField(max_length=100, allow_blank=True, required=False)
	orgid = serializers.PrimaryKeyRelatedField(queryset=Company.objects.all(), required=False, allow_null=True)
	phone_1 = serializers.CharField(max_length=100, allow_blank=True, required=False)
	phone_1_type = serializers.PrimaryKeyRelatedField(queryset=PhoneType.objects.all(), required=False, allow_null=True)
	phone_2 = serializers.CharField(max_length=100, allow_blank=True, required=False)
	phone_2_type = serializers.PrimaryKeyRelatedField(queryset=PhoneType.objects.all(), required=False, allow_null=True)
	phone_3 = serializers.CharField(max_length=100, allow_blank=True, required=False)
	phone_3_type = serializers.PrimaryKeyRelatedField(queryset=PhoneType.objects.all(), required=False, allow_null=True)
	email_1 = serializers.EmailField(max_length=100, allow_blank=True, required=False)
	email_2 = serializers.EmailField(max_length=100, allow_blank=True, required=False)
	post_street = serializers.CharField(max_length=100, allow_blank=True, required=False)
	post_city = serializers.CharField(max_length=100, allow_blank=True, required=False)
	post_zip = serializers.CharField(max_length=100, allow_blank=True, required=False)
	post_state = serializers.PrimaryKeyRelatedField(queryset=State.objects.all(), required=False, allow_null=True)
	post_country = serializers.PrimaryKeyRelatedField(queryset=Country.objects.all(), required=False, allow_null=True)
	base_street = serializers.CharField(max_length=100, allow_blank=True, required=False)
	base_city = serializers.CharField(max_length=100, allow_blank=True, required=False)
	base_zip = serializers.CharField(max_length=100, allow_blank=True, required=False)
	base_state = serializers.PrimaryKeyRelatedField(queryset=State.objects.all(), required=False, allow_null=True)
	base_country = serializers.PrimaryKeyRelatedField(queryset=Country.objects.all(), required=False, allow_null=True)
	created_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	updated_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	restored_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, allow_null=True)
	deleted = serializers.BooleanField(required=False)
	deleted_at 	= serializers.DateTimeField(required=False, allow_null=True)

	class Meta:
		model = CompanyBranch
		fields = ('orgid', 'web', 'slug', 'title', 'created_at', 'updated_at', 'deleted_at', 'restored_at', 'deleted', 'created_by', 'updated_by', 'deleted_by', 'restored_by', 'base_street', 'base_city', 'base_zip', 'base_state', 'base_country', 'post_street', 'post_city', 'post_zip', 'post_state', 'post_country', 'phone_1', 'phone_1_type', 'phone_2', 'phone_2_type', 'phone_3', 'phone_3_type', 'email_1', 'email_2')

	def create(self, validated_data):
		branch = CompanyBranch.objects.create(**validated_data)
		branch.save()
		return branch

	def update(self, instance, validated_data):
		instance.title = validated_data.get('title', instance.title)
		instance.slug = validated_data.get('slug', instance.slug)
		instance.phone_1 = validated_data.get('phone_1', instance.phone_1)
		instance.phone_1_type = validated_data.get('phone_1_type', instance.phone_1_type)
		instance.phone_2 = validated_data.get('phone_2', instance.phone_2)
		instance.phone_2_type = validated_data.get('phone_2_type', instance.phone_2_type)
		instance.phone_3 = validated_data.get('phone_3', instance.phone_3)
		instance.phone_3_type = validated_data.get('phone_3_type', instance.phone_3_type)
		instance.email_1 = validated_data.get('email_1', instance.email_1)
		instance.email_2 = validated_data.get('email_2', instance.email_2)
		instance.post_street = validated_data.get('post_street', instance.post_street)
		instance.post_city = validated_data.get('post_city', instance.post_city)
		instance.post_zip = validated_data.get('post_zip', instance.post_zip)
		instance.post_state = validated_data.get('post_state', instance.post_state)
		instance.post_country = validated_data.get('post_country', instance.post_country)
		instance.base_street = validated_data.get('base_street', instance.base_street)
		instance.base_city = validated_data.get('base_city', instance.base_city)
		instance.base_zip = validated_data.get('base_zip', instance.base_zip)
		instance.base_state = validated_data.get('base_state', instance.base_state)
		instance.base_country = validated_data.get('base_country', instance.base_country)
		instance.updated_by = validated_data.get('updated_by', instance.updated_by)
		instance.deleted_by = validated_data.get('deleted_by', instance.deleted_by)
		instance.restored_by = validated_data.get('restored_by', instance.restored_by)
		instance.deleted = validated_data.get('deleted', instance.deleted)
		instance.deleted_at = validated_data.get('deleted_at', instance.deleted_at)

		instance.save()
		return instance
