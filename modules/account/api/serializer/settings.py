from rest_framework.validators import UniqueTogetherValidator
from rest_framework import serializers

from modules.account.models import User, Company, CompanySetting, UserSetting

class SettingsSerializer(serializers.ModelSerializer):
	general_enable_vat = serializers.BooleanField(required=False)
	change_vat_on_acceptance = serializers.BooleanField(required=False)
	add_note_on_pl = serializers.BooleanField(required=False)
	print_note_on_pl = serializers.BooleanField(required=False)
	first_byuing_price_with_vat = serializers.BooleanField(required=False)
	enter_discount_byuing_price_acceptance = serializers.BooleanField(required=False)
	remember_last_discount_on_pl = serializers.BooleanField(required=False)
	priority_on_change_selling_price_acceptance_with_vat = serializers.BooleanField(required=False)
	give_discounted_selling_price_on_calculation = serializers.BooleanField(required=False)
	print_weight_on_pl = serializers.BooleanField(required=False)
	print_username_on_pl = serializers.BooleanField(required=False)
	print_date_consumence_on_pl = serializers.BooleanField(required=False)
	print_individual_number_of_supplier_on_pl = serializers.BooleanField(required=False)
	system_static_prices = serializers.BooleanField(required=False)
	counter_order_supplier = serializers.CharField(max_length=100, allow_blank=True, required=False)
	counter_order_buyer = serializers.CharField(max_length=100, allow_blank=True, required=False)
	counter_acceptance = serializers.CharField(max_length=100, allow_blank=True, required=False)
	counter_outgo = serializers.CharField(max_length=100, allow_blank=True, required=False)

	class Meta:
		model = CompanySetting
		fields = ('general_enable_vat', 'change_vat_on_acceptance', 'add_note_on_pl', 'print_note_on_pl', 'first_byuing_price_with_vat', 'enter_discount_byuing_price_acceptance', 'remember_last_discount_on_pl', 'priority_on_change_selling_price_acceptance_with_vat', 'give_discounted_selling_price_on_calculation', 'print_weight_on_pl', 'print_username_on_pl', 'print_date_consumence_on_pl', 'print_individual_number_of_supplier_on_pl', 'system_static_prices', 'counter_order_supplier', 'counter_order_buyer', 'counter_acceptance', 'counter_outgo')
	def update(self, instance, validated_data):
		instance.general_enable_vat = validated_data.get('general_enable_vat', instance.general_enable_vat)
		instance.change_vat_on_acceptance = validated_data.get('change_vat_on_acceptance', instance.change_vat_on_acceptance)
		instance.add_note_on_pl = validated_data.get('add_note_on_pl', instance.add_note_on_pl)
		instance.print_note_on_pl = validated_data.get('print_note_on_pl', instance.print_note_on_pl)
		instance.first_byuing_price_with_vat = validated_data.get('first_byuing_price_with_vat', instance.first_byuing_price_with_vat)
		instance.enter_discount_byuing_price_acceptance = validated_data.get('enter_discount_byuing_price_acceptance', instance.enter_discount_byuing_price_acceptance)
		instance.remember_last_discount_on_pl = validated_data.get('remember_last_discount_on_pl', instance.remember_last_discount_on_pl)
		instance.priority_on_change_selling_price_acceptance_with_vat = validated_data.get('priority_on_change_selling_price_acceptance_with_vat', instance.priority_on_change_selling_price_acceptance_with_vat)
		instance.give_discounted_selling_price_on_calculation = validated_data.get('give_discounted_selling_price_on_calculation', instance.give_discounted_selling_price_on_calculation)
		instance.print_weight_on_pl = validated_data.get('print_weight_on_pl', instance.print_weight_on_pl)
		instance.print_username_on_pl = validated_data.get('print_username_on_pl', instance.print_username_on_pl)
		instance.print_date_consumence_on_pl = validated_data.get('print_date_consumence_on_pl', instance.print_date_consumence_on_pl)
		instance.print_individual_number_of_supplier_on_pl = validated_data.get('print_individual_number_of_supplier_on_pl', instance.print_individual_number_of_supplier_on_pl)
		instance.system_static_prices = validated_data.get('system_static_prices', instance.system_static_prices)
		instance.counter_order_supplier = validated_data.get('counter_order_supplier', instance.counter_order_supplier)
		instance.counter_order_buyer = validated_data.get('counter_order_buyer', instance.counter_order_buyer)
		instance.counter_acceptance = validated_data.get('counter_acceptance', instance.counter_acceptance)
		instance.counter_outgo = validated_data.get('counter_outgo', instance.counter_outgo)

		instance.save()
		return instance